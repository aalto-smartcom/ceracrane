# Notice
**This project is no longer under active development and there might be problems with the individual components.**

<br>
<br>
<br>
<br>

<div align="center"><img width="500px" src="https://gitlab.com/kurkimiehet/ceracrane/-/raw/master/ui/res/logo2.png" /></div>


  <p align="center">
    Container measurement signature-system.
    <br />
    <a href="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/Project-motivation"><strong>Find more information about the project and it's motivation by reading the Wikis! »</strong></a>
  </p>
<p align="center">
    <!--<img src="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/53994088b712327ad246812eeb96c161/image.png" alt="Logo" width="350" height="330">
    <img src="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/9dac545968c20b748314b4753cbc1a8c/image.png" alt="Logo" width="350" height="330">-->
    <a style="color: #FFFFFF; text-decoration: none;" href="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/babbed6836484996d88a9d1150a8d66b/example-measurement.mp4" target="_blank" >
         <br />
         <img src="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/4b9f381a3c5e29e40219832554fd67d7/example-measurement.gif">
    </a>
</p>


<!-- ABOUT THE PROJECT -->
## About The Project

Ship container verification and signature system for Konecranes and Aalto University. The backend-environment has been built with Docker-compose consisting of many different services. The main developing principle for the project has been portability, scalability and error isolation. We have achieved this by seggregating and abstracting said endpoint/services into REST APIs.

For information about why, how and for who, please read the [Project motivation](https://gitlab.com/kurkimiehet/ceracrane/-/wikis/Project-motivation) in the wikis.

#### Technologies used

* [IOTA & Blockhain](https://www.iota.org/)
* [Docker-compose](https://www.docker.com/)
* [React Native](https://reactnative.dev/)
* [(Flutter)](https://flutter.dev/)
* [OPC-UA](https://opcfoundation.org/about/opc-technologies/opc-ua/) Crane API

<!-- GETTING STARTED -->
## Running the project

Since the whole environment is very large we decided to make some of the heavier Docker images available through CONTIOT Gitlab registries.

If you want to further develop or take a closer look at the microservices you can find the additional relevant install instructions inside of the corresponding to the service.

### How to run 


1. Create a free gitlab account: https://gitlab.com/users/sign_up
2. Open powershell, navigate to the directory where you want to download the project, and clone the project with command:
```
git clone http://gitlab.com/aalto-smartcom/ceracrane.git
``` 
 * Alternatively download and extract manually without git from: 
https://gitlab.com/aalto-smartcom/ceracrane/-/archive/master/ceracrane-master.zip

3. For docker desktop you need WSL2 on your computer. Microsoft has a good guide on this. Follow the Manual installation steps 1-5. Step 6 is optional, if you want to use ubuntu terminal instead of windows PowerShell.
https://docs.microsoft.com/en-us/windows/wsl/install-win10

4.  Download and open Docker Desktop (the installer is straightforward) https://www.docker.com/products/docker-desktop

5. First make sure you are logged in to the GitLab registry. To login, type this command to powershell and use the gitlab account you created:
```
docker login registry.gitlab.com
```
6. With powershell, navigate to the root of the project and bring the stack up (first time takes a while) with command:
```
docker-compose up
```
7. Now with your browser visit
http://localhost:80. Firefox may have some problems with the project, so if that happens try using chrome instead.

### To stop the container

From the same powershell window you started the project press:
```
ctrl + c
```
Then from powershell in the project root directory use command:
```
docker-compose down
```


### When problems arrive
Docker can use large ammount of memory so if weird things start to happen, it might be that your memory is maxing out.

Also to free space and remove unused resources try this command from powershell:
```
docker system prune --volumes
```




<!-- USAGE EXAMPLES -->
### Front end (Flutter)

Front end made using Flutter. See [README.md](./ui) at ui-directory for development instructions and  more in-depth thoughts about UI.

Even though the Flutter application is the main and easiest way to interact with the whole system you could still use for example the eIDAS-Service independently from this project. More information can be found in the eIDAS [README.md](./eidas-dss).

### Front end (React)
The Flutter frontend was a curiosity to try new technologies. For further development a React frontend was developed, and we suggest to use the React front end to see the latest features.


<!-- ROADMAP -->
## Roadmap

Currently the project has no future roadmap, but if you have any questions of enhancements to suggest please create an [issue](https://gitlab.com/kurkimiehet/ceracrane/-/issues).


<!-- CONTACT -->
## Contact
For questions related to EMPIR SmartCom or used data formats (DCC, D-SI etc.) contact [Tuukka Mustapää](https://gitlab.com/TuukkaMustapaa)


[Jaan Taponen](https://gitlab.com/janetsqy)

Henri Tunkkari

[Santeri Kääriäinen](https://gitlab.com/napuu)

...


<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

* [EIDAS](https://github.com/esig/dss)
