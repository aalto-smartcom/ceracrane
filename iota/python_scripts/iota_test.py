from iota import Iota
from iota import Transaction
from iota import TryteString
import iota_interface
import iotaconf
import unittest
import time

class TestWrite(unittest.TestCase):

    def setUp(self):
        self.api = Iota(iotaconf.DEVNET)
        self.address = iotaconf.ADDRESS
        self.tag = 'CERAPROJECT'
        self.data = 'unittest@' + str(int(time.time()))

    def test_write(self):
        self.data = 'unittest@' + str(int(time.time()))
        tx_hash = iota_interface.write_to_tangle(self.api, self.address, self.tag, self.data)
        self.assertTrue(isinstance(tx_hash, str))

    def test_confirmed_write(self):
        self.data = 'unittest@' + str(int(time.time()))
        tx_hash = iota_interface.confirmed_write(self.tag, self.data, self.api, self.address)
        self.assertTrue(isinstance(tx_hash, str))

class TestRead(unittest.TestCase):
    
    def setUp(self):
        self.api = Iota(iotaconf.DEVNET)
        self.address = iotaconf.ADDRESS
        self.tag = str(int(time.time()))
        self.data = 'unittest@' + str(int(time.time()))

    '''
    def test_read_existing_data(self):
        tx_hash = iota_interface.confirmed_write(self.tag, self.data, self.api, self.address)
        transactions = iota_interface.read_from_tangle(self.api, self.address,
                tx_data=self.data)
        self.assertTrue(list(filter(lambda x:
            TryteString.decode(x.signature_message_fragment) == self.data, transactions)))

    def test_read_nonexisting_data(self):
        wrong_data = 'nonexistent'
        transactions = iota_interface.read_from_tangle(self.api, self.address, tx_data=wrong_data)
        self.assertFalse(transactions)
    '''

    def test_read_existing_tag(self):
        tx_hash = iota_interface.confirmed_write(self.tag, self.data, self.api, self.address)
        transactions = iota_interface.read_from_tangle(self.api, self.address, tx_tags = [self.tag])
        self.assertTrue(len(transactions), 1)
        tx = transactions[0]
        self.assertTrue(tx.signature_message_fragment.decode() == self.data)

        #self.assertTrue(list(filter(lambda x:
                #TryteString.decode(x.signature_message_fragment) == self.data, transactions)))

    def test_read_nonexisting_tag(self):
        nonexisting_tag = self.tag[1:]
        transactions = iota_interface.read_from_tangle(self.api, self.address, tx_tags=[nonexisting_tag])
        self.assertFalse(transactions)
 

if __name__ == "__main__":
    unittest.main(verbosity=2)
