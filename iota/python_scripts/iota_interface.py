from iota import Iota
from iota import Transaction
from iota import ProposedTransaction
from iota import Fragment
from iota import Address
from iota import Tag
from iota import TryteString
from iota import TransactionHash
import hashlib
import iotaconf
import argparse
import time
import json
from time import sleep


def hash_tag(cid):
    """Calculate 6-byte BLAKE2B hash of a string."""
    b = hashlib.blake2b(cid.encode('utf-8'), digest_size=6)
    return b.hexdigest()


def read_from_tangle(api=Iota(iotaconf.DEVNET), 
                     address=iotaconf.ADDRESS,
                     tx_tags=None, 
                     tx_data=None):
    '''Returns a list of transactions from the Tangle matching from the given address,
    optionally matching given transaction tags and/or data.

    Args:
        api: Iota object (Pyota) for accessing the tangle
        address: Iota address
        tx_tags: Transaction tags
        tx_data: Transaction data

    Returns:
        List of transactions fulfilling the search criteria
    '''
    # Convert unicode tags to trytestrings.
    if tx_tags is not None:
        hashed_tags = list(map(hash_tag, tx_tags))
        tx_tags_trytes = list(map(TryteString.from_unicode, hashed_tags))
        tag_objects = list( Tag(t) for t in tx_tags_trytes )
    else:
        tx_tags_trytes = None

    # Get transaction hashes with the tx_tags from the given address.
    transactions = api.find_transactions(addresses=[address, ],
                                         tags=tag_objects)
    # Get unique hashes.
    hashes = list(set(transactions['hashes']))

    transactions = []  # List for storing raw transactions

    # If there are matching transactions, get raw transactions
    if hashes:
        trytes = api.get_trytes(hashes)['trytes']  
         
        for trytestring in trytes:
            tx = Transaction.from_tryte_string(trytestring)
            if tx.tag in tag_objects:
                transactions.append(tx)

        if tx_data:
            transactions = list(filter(lambda x:
                TryteString.decode(x.signature_message_fragment) == tx_data, transactions))

        transactions.sort(key=lambda x: -x.timestamp)

    return transactions


# TODO: check where this can fail
def write_to_tangle(api, address, tag, data):
    '''Add transaction to the tangle with given tag and data.

    Args:
        api: Iota object (Pyota) for accessing the tangle
        address: Iota address
        tag: Tag for the new transaction
        data: Data for the new transaction
    Returns:
        Transaction hash of the added transaction
    '''
    try:
        data_trytes = TryteString.from_unicode(data)
        tx_tag = Tag(TryteString.from_unicode(hash_tag(tag)))

        tx = ProposedTransaction(
                address=Address(address),
                message=data_trytes,
                tag=tx_tag,
                value=0
                )
        result = api.send_transfer([tx, ], depth=3, min_weight_magnitude=10)

        # Find the broadcasted transaction in the bundle and its hash.
        tx_sent = next(t for t in result['bundle'].transactions if (
            t.address == tx.address and
            t.tag == tx.tag and
            t.signature_message_fragment == Fragment(tx.message)
            ))
        tx_hash = tx_sent.hash

        return str(tx_hash)
    except Exception as e:
        raise e


# Note: get_bundles may occasionally return multiple bundles for a single 
# bundle hash but only one of them can be confirmed. It also suffices to find
# the one confirmed tail for the bundle to be confirmed. For details, see Iota
# documentation for rebroadcasting a bundle. 

def check_bundle_confirmation(api, tx_hash_str: str):
    '''Check if the tail transaction of a bundle is confirmed implying that
    the bundle is confirmed (and hence the all the transactions in the bundle).

    Args:
        api: Iota object (Pyota) for accessing the tangle
        tx_hash_str: Transaction hash as string

    Returns:
        Boolean indicating if the tail transaction (and entire bundle)
        has been confirmed. 
    '''
    tx_hash = TransactionHash(tx_hash_str)
    bundles = api.get_bundles(tx_hash)['bundles']
    tail_confirmed = False
    for b in bundles:
        tail_hash = b.tail_transaction.hash
        states = api.get_latest_inclusion([tail_hash, ])['states']
        tail_confirmed = states[tail_hash]
        if tail_confirmed:
            return tail_confirmed
    return tail_confirmed

# TODO: Add a function to rebroadcast a bundle.

def confirmed_write(tag, data, api=Iota(iotaconf.DEVNET), 
        address=iotaconf.ADDRESS, verbose=True, confirmation=True):
    """Adds a transaction to the tangle, and waits for the transaction to be confirmed.

    Args:
        tag: Tag for the new transaction
        data: Data for the new transaction
        api: Iota object (Pyota) for accessing the tangle
        address: Iota address
        verbose (boolean): If True, print situation updates

    Returns:
        The transaction hash of the new transaction as String
    """
    write_success = False
    n = 600
    while not write_success:
        try:
            tx_hash = write_to_tangle(api, address, tag, data)
            write_success = True
        except Exception as e:
            n -= 1
            if n <= 0:
                raise e
            sleep(0.1)
            pass

    if verbose:
        print('Transaction hash:\n' + tx_hash, flush=True)
        print('Transaction sent to the tangle!', flush=True)
        print('https://comnet.thetangle.org/address/%s' % address, flush=True)

    if tx_hash is not None:
        if confirmation:
            if verbose:
                print('Waiting for confirmation.', flush=True)

            tx_confirmed = False
            while not tx_confirmed:
                tx_confirmed = check_bundle_confirmation(api, tx_hash)
                time.sleep(1)

            if verbose:
                print('Transaction confirmed.', flush=True)
    else:
        if verbose:
            print('Sending transaction failed.', flush=True)

    return tx_hash

def txs_as_dict(txs):
    """Convert list of iota transactions to dict with key-value pairs 
    (transaction hash (string), message fragment (string))."""
    tx_dict = {str(tx.hash): TryteString.decode(tx.signature_message_fragment) for tx in txs}
    return tx_dict

# Helper to get data from transaction in human readable form.
def get_decoded_tx_data(txs):
    txd = list(TryteString.decode(tx.signature_message_fragment) for tx in txs)
    return txd

