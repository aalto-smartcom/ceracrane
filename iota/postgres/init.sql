DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO public;
COMMENT ON SCHEMA public IS 'The standard schema for ceracrane db';

CREATE TABLE measurements (
    crane_name text,                    --name of the operating crane (i.e. 'Ilmatar')
    load_gross_t float(32) NOT NULL,    --the gross weight of container
    load_tared_t float(32),             --tared weight of container
    trolley_pos float(32),
    hoist_pos float(32),
    bridge_pos float(32),
    date_time varchar(32),              --type varchar avoids conversion issues
    container_id text NOT NULL,         --container id
    fingerprint text NOT NULL,          --the fingerprint calculated for the container
    xml_string text,                    --value read from an xml file
    iota_tx_hash text NOT NULL,
    dcc text                            --digital calibration certificate for the crane
);

INSERT INTO measurements (crane_name, load_gross_t, load_tared_t, trolley_pos, hoist_pos, bridge_pos, date_time,
    container_id, fingerprint, xml_string, iota_tx_hash, dcc)
VALUES (
    'Ilmatar', 
    3.478, 
    0.5, 
    4.5, 
    2.5, 
    5.5, 
    '1997-09-14 23:04:34', 
    'CERA7428ILM899271-37281', 
    'VlGQpd59g3mssf1Bc47tHdnpC3ebuhxg83zFMDLRMc1k0',
    'xn0GHEmW1jEN2pxDSIICa7a4bZSpFnUcPS1klkAInV79fGaGKvdAwSPGxgjgx8Fa3FUD44jeq2IfrrhZdQZHeg8BJPQkadLYUFyZC2wrnz69D1vS3vh0QGipCNTXhTjw1aUlqcQ5g27NxbrseSmnbUjkMoisijp4eiu9sAJCC9kQgW11rqxh6EZfZi1NrBtliT7HOklyaHlMrbxD6EUgxxUAICATNtpCvUVUeLHudgpNZdTpVbKVHeacatAB8T9g',
    'YC9TAVPFYFIYXLMCJT9ZINYJNYPSUTXLXEMJMZDHTXTAAATTSMKFDOBZCNOPRKEIMHDBBTMSW9OJWR999',
    'EXAMPLE-DCC-STRING'
),
(
    'Ilmatar', 
    2.11, 
    1.10, 
    4.5, 
    2.5, 
    5.5, 
    '1999-09-14 23:05:45', 
    'C99999999', 
    'Nf4JvNydhfbKmqAgeX2IsFe1WeOyqBYVZge4sxfOoxeEO',
    'L7QLJ90ZHiNKCFevlTpmXqP3G2PixM9wxqRec23pDjQVfttcV7Kpsd1WZz4N2AFRCK1NzdquNLVikS1lHAd9NSAhcrfb9vHKLWb6pOapYSKCW22azo9144hbDeDkguehp9Uf6NYAFMxPfvaA1HW60fiurOH8MI9EIOulOCpyKgPiPxRcguUKBo7EyKCsjFW3c0eKqDGSo1yov4IzYg1GoooWUgA8pY4dqFZrkDG2SQZko32vOLZpTiNtvJjeg1aF',
    'GTIBIVX9YTC99SQCSNAVXEPKFSQVNOJRNYKSPMSCXSVYNMLZUWBYCKFSMNDIKRUZOORCTJZJHUYEWAGXF',
    'EXAMPLE-DCC-STRING'
),
(
    'Ilmatar', 
    2.110, 
    1.111, 
    4.5, 
    2.5, 
    5.5, 
    '2021-05-01 23:05:45', 
    'democontainer', 
    'Nf4JvNydhfbKmqAgeX2IsFe1WeOyqBYVZge4sxfOoxeEO',
    'L7QLJ90ZHiNKCFevlTpmXqP3G2PixM9wxqRec23pDjQVfttcV7Kpsd1WZz4N2AFRCK1NzdquNLVikS1lHAd9NSAhcrfb9vHKLWb6pOapYSKCW22azo9144hbDeDkguehp9Uf6NYAFMxPfvaA1HW60fiurOH8MI9EIOulOCpyKgPiPxRcguUKBo7EyKCsjFW3c0eKqDGSo1yov4IzYg1GoooWUgA8pY4dqFZrkDG2SQZko32vOLZpTiNtvJjeg1aF',
    'GTIBIVX9YTC99SQCSNAVXEPKFSQVNOJRNYKSPMSCXSVYNMLZUWBYCKFSMNDIKRUZOORCTJZJHUYEWAGXF',
    'EXAMPLE-DCC-STRING'
),
(
    'Ilmatar', 
    2.111, 
    1.110, 
    7.1, 
    3.5, 
    3.5, 
    '2021-05-10 18:02:22', 
    'democontainer', 
    'Nf4JvNydhfbKmqAgeX2IsFe1WeOyqBYVZge4sxfOoxeEO',
    'L7QLJ90ZHiNKCFevlTpmXqP3G2PixM9wxqRec23pDjQVfttcV7Kpsd1WZz4N2AFRCK1NzdquNLVikS1lHAd9NSAhcrfb9vHKLWb6pOapYSKCW22azo9144hbDeDkguehp9Uf6NYAFMxPfvaA1HW60fiurOH8MI9EIOulOCpyKgPiPxRcguUKBo7EyKCsjFW3c0eKqDGSo1yov4IzYg1GoooWUgA8pY4dqFZrkDG2SQZko32vOLZpTiNtvJjeg1aF',
    'GTIBIVX9YTC99SQCSNAVXEPKFSQVNOJRNYKSPMSCXSVYNMLZUWBYCKFSMNDIKRUZOORCTJZJHUYEWAGXF',
    'EXAMPLE-DCC-STRING'
),
(
    'Ilmatar', 
    2.112, 
    1.111, 
    5.2, 
    3.8, 
    4.1, 
    '2021-06-20 10:05:11', 
    'democontainer', 
    'Nf4JvNydhfbKmqAgeX2IsFe1WeOyqBYVZge4sxfOoxeEO',
    'L7QLJ90ZHiNKCFevlTpmXqP3G2PixM9wxqRec23pDjQVfttcV7Kpsd1WZz4N2AFRCK1NzdquNLVikS1lHAd9NSAhcrfb9vHKLWb6pOapYSKCW22azo9144hbDeDkguehp9Uf6NYAFMxPfvaA1HW60fiurOH8MI9EIOulOCpyKgPiPxRcguUKBo7EyKCsjFW3c0eKqDGSo1yov4IzYg1GoooWUgA8pY4dqFZrkDG2SQZko32vOLZpTiNtvJjeg1aF',
    'GTIBIVX9YTC99SQCSNAVXEPKFSQVNOJRNYKSPMSCXSVYNMLZUWBYCKFSMNDIKRUZOORCTJZJHUYEWAGXF',
    'EXAMPLE-DCC-STRING'
);

