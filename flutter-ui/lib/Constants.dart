library constants;

var JOB_STATUS = {
  -2: "No measurement progress",
  -1: "Something went wrong while making the measurement",
  0: "Job created succesfully",
  1: "Data fetched from crane",
  2: "XML created and signed",
  3: "XML validated",
  4: "Writing data to backend and IOTA",
  5: "Ready!"
};

double maximumAcceptedWeightDelta = 0.1;

String dbApiAddress = "http://localhost:9010";

String opcRestAddress = "http://localhost:9000";

String craneName = "Ilmatar"; // this is ok to be hardcoded as every crane needs their private keys set manually anyway
/*
0 job created
1 data fetched from crane
2 xml created and signed
3 xml validated
4 waiting for iota
5 ready
*/