import 'dart:core';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

TextStyle headline(context) {
  return GoogleFonts.roboto(
    textStyle: Theme.of(context).textTheme.headline4,
    fontSize: 26,
  );
}

TextStyle body(context) {
  return GoogleFonts.roboto(
    textStyle: Theme.of(context).textTheme.bodyText1,
    fontSize: 20,
    fontWeight: FontWeight.w300,
  );
}

TextStyle bodyBold(context) {
  return GoogleFonts.roboto(
    textStyle: Theme.of(context).textTheme.bodyText1,
    fontSize: 20,
    fontWeight: FontWeight.w400,
  );
}

TextStyle redBodyBold(context) {
  return GoogleFonts.roboto(
    textStyle: Theme.of(context).textTheme.bodyText1,
    fontSize: 20,
    fontWeight: FontWeight.w400,
    color: Color.fromRGBO(255, 0, 0, 1)
  );
}