import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'SearchUI.dart';

class LandingPage extends StatefulWidget {
  LandingPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(80.0),

            ),
            Container(
              padding: const EdgeInsets.only(top: 300.0),
              width: 600,
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage("res/logo3.png")),
              )
            ),
            
            Container(
              width: 600.0,
              height: 750.0,
              child: SearchWrapper()
            ),
          ],
        )
      ),
    );
  }
}