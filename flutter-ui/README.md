# User Interface

In this page
- [Instructions for developing UI](#instructions-for-developing-ui-locally)
- [Why flutter?](#why-flutter)
- [Behind the scenes](#behind-the-scenes)
- [Screenshots of UI](#screeshots)
    - [Search UI](#search-ui)
    - [Measurement UI](#measurement-ui)


## Instructions for developing ui locally

If you have succesfully brought the environment up via `docker-compose up -d` at project root, next you need to is installing Flutter and more precisely its beta channel.
See [setting up flutter web](https://flutter.dev/docs/get-started/web), in a nutshell:

```
 flutter channel beta
 flutter upgrade
 flutter config --enable-web
```
After flutter is ready:
`flutter run -d chrome` at this directory

#### Behind the scenes

After writing container id to search field, GET-request is sent to [db-backend](../iota#iotadb-rest) which returns JSON-object, a list with matching containers in database. Information about weighing integrity is also returned. All necessary information is then displayed at the screen, including links to raw measurements XML-files and [nicely formatted DCC](../dcc-view) of the crane used.

At measurement ui, current weight is fetched from [/cranestatus](../opcua#endpoints) endpoint every second. If connection to crane is not working for some reason, that is indicated by text 'NOT OK'.

Once our crane is carrying container we want to measure, we fill in the containers id and press 'Create a measurement'. POST-request with containers id is sent to [/createjob](../opcua#endpoints) endpoint and job-id is returned. UI now starts polling [/status](../opcua#endpoints) with job-id returned earlier. Backend responds with jobs current status which is displayed accordingly. Once measurement is done it is possible to make a new measurement.

---
---

## Why flutter?

We wanted to  try some new framework of which none of us had previous experience. Flutter allows deployment of same codebase to iOS, Android and Desktop platforms with little to no effort, which makes it really appealing as no other framework can offer that feature right now.

## Screenshots

#### Search UI

##### Main view used for searching measurements
<img src="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/66b5e7f82090fedecb049de844601e36/search2.png" width=700>

##### Digital Calibration Certificate (mockup) for crane used
<img src="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/b8472adbd090104156eb7d8e20304bfd/cert.png" width=700>

---
---

#### Measurement UI

##### Making measurement (container id filled in)
<img src="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/9adea5e1968cecff1e85ea3b53d26206/meas1.png" width=700>

##### Measurement in progress
<img src="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/22d165de23aaeccab7758f6beae6f252/meas2.png" width=700>

##### Measurement done
<img src="https://gitlab.com/kurkimiehet/ceracrane/-/wikis/uploads/6fbe659ed2a0bcdb6cc5f63647949c12/meas3.png" width=700>
