/* eslint-disable */

const AIIC_K16052_str = `<?xml version="1.0" encoding="UTF-8" standalone="no"?><dcc:digitalCalibrationCertificate xmlns:dcc="https://ptb.de/dcc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" schemaVersion="3.0.0-rc.2" xsi:schemaLocation="https://ptb.de/dcc https://ptb.de/dcc/v3.0.0-rc.2/dcc.xsd">

<!--

  Copyright (c) 2021 Aalto-korkeakoulusäätiö

This project (17IND02) has received funding from the EMPIR programme co-financed by the Participating States
  and from the European Union's Horizon 2020 research and innovation programme.

  The development of minimum requirements for a digital calibration certificate (DCC) is partially funded and
  supported by the joint research project EMPIR 17IND02 (title: SmartCom).

  -->

<dcc:administrativeData>
  <dcc:dccSoftware>
    <dcc:software>
      <dcc:name>
        <dcc:content>Notepad++ (32-bit)</dcc:content>
      </dcc:name>
      <dcc:release>v7.6.6</dcc:release>
    </dcc:software>
  </dcc:dccSoftware>
  <dcc:coreData>
    <dcc:countryCodeISO3166_1>FI</dcc:countryCodeISO3166_1>
    <dcc:usedLangCodeISO639_1>en</dcc:usedLangCodeISO639_1>
    <dcc:mandatoryLangCodeISO639_1>en</dcc:mandatoryLangCodeISO639_1>
    <dcc:uniqueIdentifier>AIIC-K16052</dcc:uniqueIdentifier>
    <dcc:beginPerformanceDate>2021-06-09</dcc:beginPerformanceDate>
    <dcc:endPerformanceDate>2021-06-09</dcc:endPerformanceDate>
  </dcc:coreData>
  <dcc:items>
    <dcc:item>
      <dcc:name>
        <dcc:content lang="en">Overhead crane load sensor</dcc:content>
      </dcc:name>
      <dcc:description>
        <dcc:content lang="en">Aalto Industrial Internet Campus smart overhead crane - Ilmatar</dcc:content>
      </dcc:description>
      <dcc:manufacturer>
        <dcc:name>
          <dcc:content>Konecranes</dcc:content>
        </dcc:name>
      </dcc:manufacturer>
      <dcc:model>CXTD3.2t</dcc:model>
      <dcc:identifications>
        <dcc:identification>
          <dcc:issuer>manufacturer</dcc:issuer>
          <dcc:value>K16052</dcc:value>
          <dcc:description>
            <dcc:content lang="en">Serial Number</dcc:content>
          </dcc:description>
        </dcc:identification>
      </dcc:identifications>
    </dcc:item>
  </dcc:items>
  <dcc:calibrationLaboratory>
    <dcc:contact>
      <dcc:name>
        <dcc:content>AROTOR Laboratory</dcc:content>
      </dcc:name>
      <dcc:eMail>tuukka.mustapaa@aalto.fi</dcc:eMail>
      <dcc:location>
        <dcc:countryCode>FI</dcc:countryCode>
        <dcc:city>Espoo</dcc:city>
        <dcc:postCode>02150</dcc:postCode>
        <dcc:street>Sähkömiehentie</dcc:street>
        <dcc:streetNo>4</dcc:streetNo>
      </dcc:location>
    </dcc:contact>
  </dcc:calibrationLaboratory>
  <dcc:respPersons>
    <dcc:respPerson>
      <dcc:person>
        <dcc:name>
          <dcc:content>Tuukka Mustapää</dcc:content>
        </dcc:name>
      </dcc:person>
    </dcc:respPerson>
  </dcc:respPersons>
  <dcc:customer>
    <dcc:name>
      <dcc:content>Aalto Industrial Internet Campus</dcc:content>
    </dcc:name>
    <dcc:eMail>tuukka.mustapaa@aalto.fi</dcc:eMail>
    <dcc:location>
      <dcc:countryCode>FI</dcc:countryCode>
      <dcc:city>Espoo</dcc:city>
      <dcc:postCode>02150</dcc:postCode>
      <dcc:street>Puumiehenkuja</dcc:street>
      <dcc:streetNo>5</dcc:streetNo>
    </dcc:location>
  </dcc:customer>
  <dcc:statements>
    <dcc:statement>
      <dcc:declaration>
        <dcc:content>This Certificate is not an official calibration certificate and is only meant to demonstrate the use of DCCs in an industrial application.</dcc:content>
      </dcc:declaration>
    </dcc:statement>
  </dcc:statements>
</dcc:administrativeData>
<dcc:measurementResults>
  <dcc:measurementResult>
    <dcc:name>
      <dcc:content lang="en">Mass measurement result</dcc:content>
    </dcc:name>
    <dcc:usedMethods>
      <dcc:usedMethod>
        <dcc:name>
          <dcc:content lang="en">Calibration Procedure</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content lang="en">Comparison to a reference mass</dcc:content>
        </dcc:description>
      </dcc:usedMethod>
    </dcc:usedMethods>
    <dcc:measuringEquipments>
      <dcc:measuringEquipment>
        <dcc:name>
          <dcc:content lang="en">Reference mass set</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content lang="en">A set of 100 kg weights</dcc:content>
        </dcc:description>
        <dcc:manufacturer>
          <dcc:name>
            <dcc:content>Aalto University</dcc:content>
          </dcc:name>
        </dcc:manufacturer>
        <dcc:identifications>
          <dcc:identification>
            <dcc:issuer>owner</dcc:issuer>
            <dcc:value>12345678</dcc:value>
            <dcc:description>
              <dcc:content lang="en">Serial Number</dcc:content>
            </dcc:description>
          </dcc:identification>
        </dcc:identifications>
      </dcc:measuringEquipment>
    </dcc:measuringEquipments>
    <dcc:influenceConditions>
      <dcc:influenceCondition>
        <dcc:name>
          <dcc:content>Ambient conditions</dcc:content>
        </dcc:name>
        <dcc:status>beforeAdjustment</dcc:status>
        <dcc:data>
          <dcc:list refType="/temperature">
            <dcc:quantity refType="/maxValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>25</si:value>
                <si:unit>\degreecelsius</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/minValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>19</si:value>
                <si:unit>\degreecelsius</si:unit>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/humidity">
            <dcc:quantity refType="/maxValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.40</si:value>
                <si:unit>\kilogram\tothe{1}\metre\tothe{-3}\kilogram\tothe{-1}\metre\tothe{3}</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/minValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.20</si:value>
                <si:unit>\kilogram\tothe{1}\metre\tothe{-3}\kilogram\tothe{-1}\metre\tothe{3}</si:unit>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/pressure">
            <dcc:quantity refType="/maxValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>1050</si:value>
                <si:unit>\hecto\pascal</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/minValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>1000</si:value>
                <si:unit>\hecto\pascal</si:unit>
              </si:real>
            </dcc:quantity>
          </dcc:list>
        </dcc:data>
      </dcc:influenceCondition>
    </dcc:influenceConditions>
    <dcc:results>
      <dcc:result>
        <dcc:name>
          <dcc:content lang="fi">Mass measurements</dcc:content>
        </dcc:name>
        <dcc:data>
          <dcc:list>
            <dcc:name>
              <dcc:content>Mass of 100 kg</dcc:content>
            </dcc:name>		
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>100.00</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/indicatedValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>100.2</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.20</si:value>
                <si:unit>\kilogram</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.06</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list>
            <dcc:name>
              <dcc:content>Mass of 200 kg</dcc:content>
            </dcc:name>		
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>200.00</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/indicatedValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>200.2</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.20</si:value>
                <si:unit>\kilogram</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.06</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list>
            <dcc:name>
              <dcc:content>Mass of 300 kg</dcc:content>
            </dcc:name>		
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>300.00</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/indicatedValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>300.2</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.20</si:value>
                <si:unit>\kilogram</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.06</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list>
            <dcc:name>
              <dcc:content>Mass of 400 kg</dcc:content>
            </dcc:name>		
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>400.00</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/indicatedValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>400.2</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.20</si:value>
                <si:unit>\kilogram</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.06</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list>
            <dcc:name>
              <dcc:content>Mass of 500 kg</dcc:content>
            </dcc:name>		
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>500.00</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/indicatedValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>500.2</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.20</si:value>
                <si:unit>\kilogram</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.06</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list>
            <dcc:name>
              <dcc:content>Mass of 600 kg</dcc:content>
            </dcc:name>		
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>600.00</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/indicatedValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>600.2</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.20</si:value>
                <si:unit>\kilogram</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.06</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list>
            <dcc:name>
              <dcc:content>Mass of 700 kg</dcc:content>
            </dcc:name>		
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>700.00</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/indicatedValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>100.2</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.20</si:value>
                <si:unit>\kilogram</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.06</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list>
            <dcc:name>
              <dcc:content>Mass of 800 kg</dcc:content>
            </dcc:name>		
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>800.00</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/indicatedValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>800.2</si:value>
                <si:unit>\kilogram</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.20</si:value>
                <si:unit>\kilogram</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.06</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
        </dcc:data>
      </dcc:result>
    </dcc:results>
  </dcc:measurementResult>
</dcc:measurementResults>
<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="id-7e161ebc1887f86fc9f86ecda58e10db"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256"/><ds:Reference Id="r-id-7e161ebc1887f86fc9f86ecda58e10db-1" URI=""><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/TR/1999/REC-xpath-19991116"><ds:XPath>not(ancestor-or-self::ds:Signature)</ds:XPath></ds:Transform><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>XYcJP2oF9TmVD4PxPNsD+HYTkkRs1UgnSQI9JeYpImY=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903#SignedProperties" URI="#xades-id-7e161ebc1887f86fc9f86ecda58e10db"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>14gsf19sVS+RyLSg2mXLPYzxUk6S/wR7TGQmpJMckkM=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="value-id-7e161ebc1887f86fc9f86ecda58e10db">RlTsIC6oDyzvk1VWpMqjFpgIEDt69OzmGuXim+Sbp25OQA+Yckl36USbz8ncb97eED0nvMqk3qYxYZAjbzjO9g==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICYTCCAgegAwIBAgIUC7P5iuL3F3OwTA4gsT38SQOQaRcwCgYIKoZIzj0EAwIwdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxHTAbBgNVBAMTFEludGVybWVkaWFyeSBUZXN0IENBMB4XDTIxMDQyMzA2NDUwMFoXDTI1MDQyMjA2NDUwMFowczELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxGTAXBgNVBAMTEFRlc3QgQ2VydGlmaWNhdGUwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAASKHHf6RHFTVkNiUphF4N4/yfmUJMZ203pi3hDysht5GAK/hljXMVzksAtnGW4vhNcheNTcl9HOSX1KdlbQK47mo3UwczAOBgNVHQ8BAf8EBAMCBaAwEwYDVR0lBAwwCgYIKwYBBQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQURaEgNaGTM4ByFCz6Sb6uEDarKkswHwYDVR0jBBgwFoAUTYFZ14ynSoaA8S8Ls6+5hqBVXUowCgYIKoZIzj0EAwIDSAAwRQIgIGETvYTeKizBqip9LMEjjMzInYumUeZiIgsuyZypKI4CIQDyj2j9fqRpbET/+MiuP4RTMDp+60YhlHT+7QrmDrXPOg==</ds:X509Certificate><ds:X509Certificate>MIICUzCCAfmgAwIBAgIUYFAqkIvPBDd0KXzOZmAOaHh1GhgwCgYIKoZIzj0EAwIwdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB1V1c2ltYWExETAPBgNVBAcTCEhlbHNpbmtpMQ4wDAYDVQQKEwVBYWx0bzEOMAwGA1UECxMFQWFsdG8xIzAhBgNVBAMTGkFhbHRvIEludGVybWVkaWFyeSBUZXN0IENBMB4XDTIxMDQyMzA2NDUwMFoXDTI1MDQyMjA2NDUwMFowdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxHTAbBgNVBAMTFEludGVybWVkaWFyeSBUZXN0IENBMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEWud6EjKXIUchRzhOlVIKtXHIdvWlbzZ31JoEVb5r6RLiRIG05/T8/tVTamj9ApqFCMSwj3ahuDWBudqBbmWXEqNjMGEwDgYDVR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFE2BWdeMp0qGgPEvC7OvuYagVV1KMB8GA1UdIwQYMBaAFHfzhicsmfD2EWWSaf3ZPRgs79+UMAoGCCqGSM49BAMCA0gAMEUCIASEKNQtJwb5Ht0C5iO1I44OgE/zRuyhnQrPaDbIZ8wjAiEA3N4bj03HX/FgC/bnHrkK16hUT79Aluzy+IteJIGtNHQ=</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Object><xades:QualifyingProperties xmlns:xades="http://uri.etsi.org/01903/v1.3.2#" Target="#id-7e161ebc1887f86fc9f86ecda58e10db"><xades:SignedProperties Id="xades-id-7e161ebc1887f86fc9f86ecda58e10db"><xades:SignedSignatureProperties><xades:SigningTime>2021-06-15T09:50:40Z</xades:SigningTime><xades:SigningCertificateV2><xades:Cert><xades:CertDigest><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha512"/><ds:DigestValue>5w8jXxhfZB4DdB+OzdMp4VEpjbOWEdiWSegTQrlmgY+juZ2HynKY2mg4Arg1JITlk5zv0TJGA+uBcUg1jF2sqw==</ds:DigestValue></xades:CertDigest><xades:IssuerSerialV2>MIGTMHukeTB3MQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxETAPBgNVBAoTCEN1c3RvbWVyMREwDwYDVQQLEwhDdXN0b21lcjEdMBsGA1UEAxMUSW50ZXJtZWRpYXJ5IFRlc3QgQ0ECFAuz+Yri9xdzsEwOILE9/EkDkGkX</xades:IssuerSerialV2></xades:Cert></xades:SigningCertificateV2><xades:SignaturePolicyIdentifier><xades:SignaturePolicyImplied/></xades:SignaturePolicyIdentifier></xades:SignedSignatureProperties><xades:SignedDataObjectProperties><xades:DataObjectFormat ObjectReference="#r-id-7e161ebc1887f86fc9f86ecda58e10db-1"><xades:MimeType>text/xml</xades:MimeType></xades:DataObjectFormat></xades:SignedDataObjectProperties></xades:SignedProperties></xades:QualifyingProperties></ds:Object></ds:Signature>
<dcc:cryptographicIdentifier>dcc://952000000.230/UZ8yXh3_rwDHrWOb/kItPXKwo_z886v95XL9uL-bovOk</dcc:cryptographicIdentifier>
</dcc:digitalCalibrationCertificate>`

const AIIC_M91300_str = `<?xml version="1.0" encoding="UTF-8" standalone="no"?><dcc:digitalCalibrationCertificate xmlns:dcc="https://ptb.de/dcc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" schemaVersion="3.0.0-rc.2" xsi:schemaLocation="https://ptb.de/dcc https://ptb.de/dcc/v3.0.0-rc.2/dcc.xsd">

<!--

Copyright (c) 2021 Aalto-korkeakoulusäätiö

This project (17IND02) has received funding from the EMPIR programme co-financed by the Participating States
and from the European Union's Horizon 2020 research and innovation programme.

The development of minimum requirements for a digital calibration certificate (DCC) is partially funded and
supported by the joint research project EMPIR 17IND02 (title: SmartCom).

-->

<dcc:administrativeData>
    <dcc:dccSoftware>
        <dcc:software>
            <dcc:name>
                <dcc:content>Notepad++ (32-bit)</dcc:content>
            </dcc:name>
            <dcc:release>v7.6.6</dcc:release>
        </dcc:software>
    </dcc:dccSoftware>
    <dcc:coreData>
        <dcc:countryCodeISO3166_1>FI</dcc:countryCodeISO3166_1>
        <dcc:usedLangCodeISO639_1>en</dcc:usedLangCodeISO639_1>
        <dcc:mandatoryLangCodeISO639_1>en</dcc:mandatoryLangCodeISO639_1>
        <dcc:uniqueIdentifier>AIIC-91300</dcc:uniqueIdentifier>
        <dcc:beginPerformanceDate>2021-06-09</dcc:beginPerformanceDate>
        <dcc:endPerformanceDate>2021-06-09</dcc:endPerformanceDate>
    </dcc:coreData>
    <dcc:items>
        <dcc:name>
            <dcc:content lang="en">A set of weights</dcc:content>
        </dcc:name>
        <dcc:description>
            <dcc:content lang="en">A set of weights</dcc:content>
        </dcc:description>
        <dcc:item>
            <dcc:name>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:name>
            <dcc:description>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:description>
            <dcc:manufacturer>
                <dcc:name>
                    <dcc:content>Aalto University</dcc:content>
                </dcc:name>
            </dcc:manufacturer>
            <dcc:identifications>
                <dcc:identification>
                    <dcc:issuer>manufacturer</dcc:issuer>
                    <dcc:value>K1001</dcc:value>
                    <dcc:description>
                        <dcc:content lang="en">Serial Number</dcc:content>
                    </dcc:description>
                </dcc:identification>
            </dcc:identifications>
        </dcc:item>
        <dcc:item>
            <dcc:name>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:name>
            <dcc:description>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:description>
            <dcc:manufacturer>
                <dcc:name>
                    <dcc:content>Aalto University</dcc:content>
                </dcc:name>
            </dcc:manufacturer>
            <dcc:identifications>
                <dcc:identification>
                    <dcc:issuer>manufacturer</dcc:issuer>
                    <dcc:value>K1002</dcc:value>
                    <dcc:description>
                        <dcc:content lang="en">Serial Number</dcc:content>
                    </dcc:description>
                </dcc:identification>
            </dcc:identifications>
        </dcc:item>
        <dcc:item>
            <dcc:name>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:name>
            <dcc:description>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:description>
            <dcc:manufacturer>
                <dcc:name>
                    <dcc:content>Aalto University</dcc:content>
                </dcc:name>
            </dcc:manufacturer>
            <dcc:identifications>
                <dcc:identification>
                    <dcc:issuer>manufacturer</dcc:issuer>
                    <dcc:value>K1003</dcc:value>
                    <dcc:description>
                        <dcc:content lang="en">Serial Number</dcc:content>
                    </dcc:description>
                </dcc:identification>
            </dcc:identifications>
        </dcc:item>
        <dcc:item>
            <dcc:name>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:name>
            <dcc:description>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:description>
            <dcc:manufacturer>
                <dcc:name>
                    <dcc:content>Aalto University</dcc:content>
                </dcc:name>
            </dcc:manufacturer>
            <dcc:identifications>
                <dcc:identification>
                    <dcc:issuer>manufacturer</dcc:issuer>
                    <dcc:value>K1004</dcc:value>
                    <dcc:description>
                        <dcc:content lang="en">Serial Number</dcc:content>
                    </dcc:description>
                </dcc:identification>
            </dcc:identifications>
        </dcc:item>
        <dcc:item>
            <dcc:name>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:name>
            <dcc:description>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:description>
            <dcc:manufacturer>
                <dcc:name>
                    <dcc:content>Aalto University</dcc:content>
                </dcc:name>
            </dcc:manufacturer>
            <dcc:identifications>
                <dcc:identification>
                    <dcc:issuer>manufacturer</dcc:issuer>
                    <dcc:value>K1005</dcc:value>
                    <dcc:description>
                        <dcc:content lang="en">Serial Number</dcc:content>
                    </dcc:description>
                </dcc:identification>
            </dcc:identifications>
        </dcc:item>
        <dcc:item>
            <dcc:name>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:name>
            <dcc:description>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:description>
            <dcc:manufacturer>
                <dcc:name>
                    <dcc:content>Aalto University</dcc:content>
                </dcc:name>
            </dcc:manufacturer>
            <dcc:identifications>
                <dcc:identification>
                    <dcc:issuer>manufacturer</dcc:issuer>
                    <dcc:value>K1006</dcc:value>
                    <dcc:description>
                        <dcc:content lang="en">Serial Number</dcc:content>
                    </dcc:description>
                </dcc:identification>
            </dcc:identifications>
        </dcc:item>
        <dcc:item>
            <dcc:name>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:name>
            <dcc:description>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:description>
            <dcc:manufacturer>
                <dcc:name>
                    <dcc:content>Aalto University</dcc:content>
                </dcc:name>
            </dcc:manufacturer>
            <dcc:identifications>
                <dcc:identification>
                    <dcc:issuer>manufacturer</dcc:issuer>
                    <dcc:value>K1007</dcc:value>
                    <dcc:description>
                        <dcc:content lang="en">Serial Number</dcc:content>
                    </dcc:description>
                </dcc:identification>
            </dcc:identifications>
        </dcc:item>
        <dcc:item>
            <dcc:name>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:name>
            <dcc:description>
                <dcc:content lang="en">A weight of 100kg</dcc:content>
            </dcc:description>
            <dcc:manufacturer>
                <dcc:name>
                    <dcc:content>Aalto University</dcc:content>
                </dcc:name>
            </dcc:manufacturer>
            <dcc:identifications>
                <dcc:identification>
                    <dcc:issuer>manufacturer</dcc:issuer>
                    <dcc:value>K1008</dcc:value>
                    <dcc:description>
                        <dcc:content lang="en">Serial Number</dcc:content>
                    </dcc:description>
                </dcc:identification>
            </dcc:identifications>
        </dcc:item>
    </dcc:items>
    <dcc:calibrationLaboratory>
        <dcc:contact>
            <dcc:name>
                <dcc:content>AROTOR Laboratory</dcc:content>
            </dcc:name>
            <dcc:eMail>tuukka.mustapaa@aalto.fi</dcc:eMail>
            <dcc:location>
                <dcc:countryCode>FI</dcc:countryCode>
                <dcc:city>Espoo</dcc:city>
                <dcc:postCode>02150</dcc:postCode>
                <dcc:street>Sähkömiehentie</dcc:street>
                <dcc:streetNo>4</dcc:streetNo>
            </dcc:location>
        </dcc:contact>
    </dcc:calibrationLaboratory>
    <dcc:respPersons>
        <dcc:respPerson>
            <dcc:person>
                <dcc:name>
                    <dcc:content>Tuukka Mustapää</dcc:content>
                </dcc:name>
            </dcc:person>
        </dcc:respPerson>
    </dcc:respPersons>
    <dcc:customer>
            <dcc:name>
                <dcc:content>AROTOR Laboratory</dcc:content>
            </dcc:name>
        <dcc:eMail>tuukka.mustapaa@aalto.fi</dcc:eMail>
        <dcc:location>
            <dcc:countryCode>FI</dcc:countryCode>
            <dcc:city>Espoo</dcc:city>
            <dcc:postCode>02150</dcc:postCode>
            <dcc:street>Sähkömiehentie</dcc:street>
            <dcc:streetNo>4</dcc:streetNo>
        </dcc:location>
    </dcc:customer>
    <dcc:statements>
        <dcc:statement>
            <dcc:declaration>
                <dcc:content lang="en">This certificate is not an official calibration certificate and is only meant to demonstrate the use of DCCs in an industrial application.</dcc:content>
            </dcc:declaration>
        </dcc:statement>
    </dcc:statements>
</dcc:administrativeData>
<dcc:measurementResults>
    <dcc:measurementResult>
        <dcc:name>
            <dcc:content lang="en">Mass measuremnt results</dcc:content>
        </dcc:name>
        <dcc:usedMethods>
            <dcc:usedMethod>
                <dcc:name>
                    <dcc:content lang="en">Calibration Procedure</dcc:content>
                </dcc:name>
                <dcc:description>
                    <dcc:content lang="en">Comparison to a reference</dcc:content>
                </dcc:description>
            </dcc:usedMethod>
        </dcc:usedMethods>
        <dcc:measuringEquipments>
            <dcc:measuringEquipment>
                <dcc:name>
                    <dcc:content lang="en">Reference instrument</dcc:content>
                </dcc:name>
                <dcc:description>
                    <dcc:content lang="en">Reference instrument</dcc:content>
                </dcc:description>
                <dcc:manufacturer>
                    <dcc:name>
                        <dcc:content>Aalto University</dcc:content>
                    </dcc:name>
                </dcc:manufacturer>
                <dcc:identifications>
                    <dcc:identification>
                        <dcc:issuer>owner</dcc:issuer>
                        <dcc:value>11223344</dcc:value>
                        <dcc:description>
                            <dcc:content lang="en">Serial Number</dcc:content>
                        </dcc:description>
                    </dcc:identification>
                </dcc:identifications>
            </dcc:measuringEquipment>
        </dcc:measuringEquipments>
        <dcc:influenceConditions>
            <dcc:influenceCondition>
                <dcc:name>
                    <dcc:content>Ambient conditions</dcc:content>
                </dcc:name>
                <dcc:status>beforeAdjustment</dcc:status>
                <dcc:data>
                    <dcc:list refType="/temperature">
                        <dcc:quantity refType="/maxValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>25</si:value>
                                <si:unit>\degreecelsius</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/minValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>19</si:value>
                                <si:unit>\degreecelsius</si:unit>
                            </si:real>
                        </dcc:quantity>
                    </dcc:list>
                    <dcc:list refType="/humidity">
                        <dcc:quantity refType="/maxValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>0.40</si:value>
                                <si:unit>\kilogram\tothe{1}\metre\tothe{-3}\kilogram\tothe{-1}\metre\tothe{3}</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/minValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>0.20</si:value>
                                <si:unit>\kilogram\tothe{1}\metre\tothe{-3}\kilogram\tothe{-1}\metre\tothe{3}</si:unit>
                            </si:real>
                        </dcc:quantity>
                    </dcc:list>
                </dcc:data>
            </dcc:influenceCondition>
        </dcc:influenceConditions>
        <dcc:results>
            <dcc:result>
                <dcc:name>
                    <dcc:content lang="fi">Weight K1001</dcc:content>
                </dcc:name>
                <dcc:data>
                    <dcc:list>
                        <dcc:name>
                            <dcc:content>Mass of 100 kg</dcc:content>
                        </dcc:name>		
                        <dcc:quantity refType="/referenceValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.00</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/indicatedValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.2</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/deviation">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>0.20</si:value>
                                <si:unit>\kilogram</si:unit>
                                <si:expandedUnc>
                                    <si:uncertainty>0.06</si:uncertainty>
                                    <si:coverageFactor>2</si:coverageFactor>
                                    <si:coverageProbability>0.95</si:coverageProbability>
                                    <si:distribution>normal</si:distribution>
                                </si:expandedUnc>
                            </si:real>
                        </dcc:quantity>
                    </dcc:list>
                </dcc:data>
            </dcc:result>
            <dcc:result>
                <dcc:name>
                    <dcc:content lang="fi">Weight K1002</dcc:content>
                </dcc:name>
                <dcc:data>
                    <dcc:list>
                        <dcc:name>
                            <dcc:content>Mass of 100 kg</dcc:content>
                        </dcc:name>		
                        <dcc:quantity refType="/referenceValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.00</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/indicatedValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.2</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/deviation">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>0.20</si:value>
                                <si:unit>\kilogram</si:unit>
                                <si:expandedUnc>
                                    <si:uncertainty>0.06</si:uncertainty>
                                    <si:coverageFactor>2</si:coverageFactor>
                                    <si:coverageProbability>0.95</si:coverageProbability>
                                    <si:distribution>normal</si:distribution>
                                </si:expandedUnc>
                            </si:real>
                        </dcc:quantity>
                    </dcc:list>
                </dcc:data>
            </dcc:result>
            <dcc:result>
                <dcc:name>
                    <dcc:content lang="fi">Weight K1003</dcc:content>
                </dcc:name>
                <dcc:data>
                    <dcc:list>
                        <dcc:name>
                            <dcc:content>Mass of 100 kg</dcc:content>
                        </dcc:name>		
                        <dcc:quantity refType="/referenceValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.00</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/indicatedValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.2</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/deviation">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>0.20</si:value>
                                <si:unit>\kilogram</si:unit>
                                <si:expandedUnc>
                                    <si:uncertainty>0.06</si:uncertainty>
                                    <si:coverageFactor>2</si:coverageFactor>
                                    <si:coverageProbability>0.95</si:coverageProbability>
                                    <si:distribution>normal</si:distribution>
                                </si:expandedUnc>
                            </si:real>
                        </dcc:quantity>
                    </dcc:list>
                </dcc:data>
            </dcc:result>
            <dcc:result>
                <dcc:name>
                    <dcc:content lang="fi">Weight K1004</dcc:content>
                </dcc:name>
                <dcc:data>
                    <dcc:list>
                        <dcc:name>
                            <dcc:content>Mass of 100 kg</dcc:content>
                        </dcc:name>		
                        <dcc:quantity refType="/referenceValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.00</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/indicatedValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.2</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/deviation">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>0.20</si:value>
                                <si:unit>\kilogram</si:unit>
                                <si:expandedUnc>
                                    <si:uncertainty>0.06</si:uncertainty>
                                    <si:coverageFactor>2</si:coverageFactor>
                                    <si:coverageProbability>0.95</si:coverageProbability>
                                    <si:distribution>normal</si:distribution>
                                </si:expandedUnc>
                            </si:real>
                        </dcc:quantity>
                    </dcc:list>
                </dcc:data>
            </dcc:result>
            <dcc:result>
                <dcc:name>
                    <dcc:content lang="fi">Weight K1005</dcc:content>
                </dcc:name>
                <dcc:data>
                    <dcc:list>
                        <dcc:name>
                            <dcc:content>Mass of 100 kg</dcc:content>
                        </dcc:name>		
                        <dcc:quantity refType="/referenceValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.00</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/indicatedValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.2</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/deviation">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>0.20</si:value>
                                <si:unit>\kilogram</si:unit>
                                <si:expandedUnc>
                                    <si:uncertainty>0.06</si:uncertainty>
                                    <si:coverageFactor>2</si:coverageFactor>
                                    <si:coverageProbability>0.95</si:coverageProbability>
                                    <si:distribution>normal</si:distribution>
                                </si:expandedUnc>
                            </si:real>
                        </dcc:quantity>
                    </dcc:list>
                </dcc:data>
            </dcc:result>
            <dcc:result>
                <dcc:name>
                    <dcc:content lang="fi">Weight K1006</dcc:content>
                </dcc:name>
                <dcc:data>
                    <dcc:list>
                        <dcc:name>
                            <dcc:content>Mass of 100 kg</dcc:content>
                        </dcc:name>		
                        <dcc:quantity refType="/referenceValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.00</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/indicatedValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.2</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/deviation">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>0.20</si:value>
                                <si:unit>\kilogram</si:unit>
                                <si:expandedUnc>
                                    <si:uncertainty>0.06</si:uncertainty>
                                    <si:coverageFactor>2</si:coverageFactor>
                                    <si:coverageProbability>0.95</si:coverageProbability>
                                    <si:distribution>normal</si:distribution>
                                </si:expandedUnc>
                            </si:real>
                        </dcc:quantity>
                    </dcc:list>
                </dcc:data>
            </dcc:result>
            <dcc:result>
                <dcc:name>
                    <dcc:content lang="fi">Weight K1007</dcc:content>
                </dcc:name>
                <dcc:data>
                    <dcc:list>
                        <dcc:name>
                            <dcc:content>Mass of 100 kg</dcc:content>
                        </dcc:name>		
                        <dcc:quantity refType="/referenceValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.00</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/indicatedValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.2</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/deviation">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>0.20</si:value>
                                <si:unit>\kilogram</si:unit>
                                <si:expandedUnc>
                                    <si:uncertainty>0.06</si:uncertainty>
                                    <si:coverageFactor>2</si:coverageFactor>
                                    <si:coverageProbability>0.95</si:coverageProbability>
                                    <si:distribution>normal</si:distribution>
                                </si:expandedUnc>
                            </si:real>
                        </dcc:quantity>
                    </dcc:list>
                </dcc:data>
            </dcc:result>
            <dcc:result>
                <dcc:name>
                    <dcc:content lang="fi">Weight K1008</dcc:content>
                </dcc:name>
                <dcc:data>
                    <dcc:list>
                        <dcc:name>
                            <dcc:content>Mass of 100 kg</dcc:content>
                        </dcc:name>		
                        <dcc:quantity refType="/referenceValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.00</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/indicatedValue">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>100.2</si:value>
                                <si:unit>\kilogram</si:unit>
                            </si:real>
                        </dcc:quantity>
                        <dcc:quantity refType="/deviation">
                            <si:real xmlns:si="https://ptb.de/si">
                                <si:value>0.20</si:value>
                                <si:unit>\kilogram</si:unit>
                                <si:expandedUnc>
                                    <si:uncertainty>0.06</si:uncertainty>
                                    <si:coverageFactor>2</si:coverageFactor>
                                    <si:coverageProbability>0.95</si:coverageProbability>
                                    <si:distribution>normal</si:distribution>
                                </si:expandedUnc>
                            </si:real>
                        </dcc:quantity>
                    </dcc:list>
                </dcc:data>
            </dcc:result>
        </dcc:results>
    </dcc:measurementResult>
</dcc:measurementResults>
<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="id-0adc5eb4c0473be5245c7bd12248eec3"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256"/><ds:Reference Id="r-id-0adc5eb4c0473be5245c7bd12248eec3-1" URI=""><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/TR/1999/REC-xpath-19991116"><ds:XPath>not(ancestor-or-self::ds:Signature)</ds:XPath></ds:Transform><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>53nBdyEiUkedhWvmAacc9PwidcjpkeSE2EXxim3SdxY=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903#SignedProperties" URI="#xades-id-0adc5eb4c0473be5245c7bd12248eec3"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>IAt8ebAosWg7a761xLHLm86w1EkjCzy816fmAsXLyEI=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="value-id-0adc5eb4c0473be5245c7bd12248eec3">kBODu1X1BvobHRHL4InO1YkW/tk5QXfKiDQef8b6kfuDQTEtIw2L86nLFMBqPayNA6jEQih9D50X0AMLIdUUSA==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICYTCCAgegAwIBAgIUC7P5iuL3F3OwTA4gsT38SQOQaRcwCgYIKoZIzj0EAwIwdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxHTAbBgNVBAMTFEludGVybWVkaWFyeSBUZXN0IENBMB4XDTIxMDQyMzA2NDUwMFoXDTI1MDQyMjA2NDUwMFowczELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxGTAXBgNVBAMTEFRlc3QgQ2VydGlmaWNhdGUwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAASKHHf6RHFTVkNiUphF4N4/yfmUJMZ203pi3hDysht5GAK/hljXMVzksAtnGW4vhNcheNTcl9HOSX1KdlbQK47mo3UwczAOBgNVHQ8BAf8EBAMCBaAwEwYDVR0lBAwwCgYIKwYBBQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQURaEgNaGTM4ByFCz6Sb6uEDarKkswHwYDVR0jBBgwFoAUTYFZ14ynSoaA8S8Ls6+5hqBVXUowCgYIKoZIzj0EAwIDSAAwRQIgIGETvYTeKizBqip9LMEjjMzInYumUeZiIgsuyZypKI4CIQDyj2j9fqRpbET/+MiuP4RTMDp+60YhlHT+7QrmDrXPOg==</ds:X509Certificate><ds:X509Certificate>MIICUzCCAfmgAwIBAgIUYFAqkIvPBDd0KXzOZmAOaHh1GhgwCgYIKoZIzj0EAwIwdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB1V1c2ltYWExETAPBgNVBAcTCEhlbHNpbmtpMQ4wDAYDVQQKEwVBYWx0bzEOMAwGA1UECxMFQWFsdG8xIzAhBgNVBAMTGkFhbHRvIEludGVybWVkaWFyeSBUZXN0IENBMB4XDTIxMDQyMzA2NDUwMFoXDTI1MDQyMjA2NDUwMFowdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxHTAbBgNVBAMTFEludGVybWVkaWFyeSBUZXN0IENBMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEWud6EjKXIUchRzhOlVIKtXHIdvWlbzZ31JoEVb5r6RLiRIG05/T8/tVTamj9ApqFCMSwj3ahuDWBudqBbmWXEqNjMGEwDgYDVR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFE2BWdeMp0qGgPEvC7OvuYagVV1KMB8GA1UdIwQYMBaAFHfzhicsmfD2EWWSaf3ZPRgs79+UMAoGCCqGSM49BAMCA0gAMEUCIASEKNQtJwb5Ht0C5iO1I44OgE/zRuyhnQrPaDbIZ8wjAiEA3N4bj03HX/FgC/bnHrkK16hUT79Aluzy+IteJIGtNHQ=</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Object><xades:QualifyingProperties xmlns:xades="http://uri.etsi.org/01903/v1.3.2#" Target="#id-0adc5eb4c0473be5245c7bd12248eec3"><xades:SignedProperties Id="xades-id-0adc5eb4c0473be5245c7bd12248eec3"><xades:SignedSignatureProperties><xades:SigningTime>2021-06-15T09:50:54Z</xades:SigningTime><xades:SigningCertificateV2><xades:Cert><xades:CertDigest><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha512"/><ds:DigestValue>5w8jXxhfZB4DdB+OzdMp4VEpjbOWEdiWSegTQrlmgY+juZ2HynKY2mg4Arg1JITlk5zv0TJGA+uBcUg1jF2sqw==</ds:DigestValue></xades:CertDigest><xades:IssuerSerialV2>MIGTMHukeTB3MQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxETAPBgNVBAoTCEN1c3RvbWVyMREwDwYDVQQLEwhDdXN0b21lcjEdMBsGA1UEAxMUSW50ZXJtZWRpYXJ5IFRlc3QgQ0ECFAuz+Yri9xdzsEwOILE9/EkDkGkX</xades:IssuerSerialV2></xades:Cert></xades:SigningCertificateV2><xades:SignaturePolicyIdentifier><xades:SignaturePolicyImplied/></xades:SignaturePolicyIdentifier></xades:SignedSignatureProperties><xades:SignedDataObjectProperties><xades:DataObjectFormat ObjectReference="#r-id-0adc5eb4c0473be5245c7bd12248eec3-1"><xades:MimeType>text/xml</xades:MimeType></xades:DataObjectFormat></xades:SignedDataObjectProperties></xades:SignedProperties></xades:QualifyingProperties></ds:Object></ds:Signature>
<dcc:cryptographicIdentifier>dcc://952000000.230/z5lPMF3iveXUIwGe/Abv0UkRacmJU5PNZNmhy8pMyBeI</dcc:cryptographicIdentifier>
</dcc:digitalCalibrationCertificate>`


const M_17L288_str = `<?xml version="1.0" encoding="UTF-8" standalone="no"?><dcc:digitalCalibrationCertificate xmlns:dcc="https://ptb.de/dcc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" schemaVersion="3.0.0-rc.2" xsi:schemaLocation="https://ptb.de/dcc https://ptb.de/dcc/v3.0.0-rc.2/dcc.xsd">

<!--
    Copyright (c) 2020 - Aalto-korkeakoulusäätiö sr., all rights reserved

    This software was developed at the Aalto University. Aalto University assumes no responsibility whatsoever for its use by other parties, and makes no guarantees, expressed or implied, about its quality, reliability, safety, suitability or any other characteristic.
    As far as legally permitted Aalto University refuses any liability for any direct, indirect or consequential damage arising in connection with the use of this software.
    
    This project (17IND02) has received funding from the EMPIR programme co-financed by the Participating States and from the European Union's Horizon 2020 research and innovation programme.
-->

<dcc:administrativeData>
  <dcc:dccSoftware>
    <dcc:software>
      <dcc:name>
        <dcc:content>Notepad++ (32-bit)</dcc:content>
      </dcc:name>
      <dcc:release>v7.9.1</dcc:release>
    </dcc:software>
  </dcc:dccSoftware>
  <dcc:coreData>
    <dcc:countryCodeISO3166_1>FI</dcc:countryCodeISO3166_1>
    <dcc:usedLangCodeISO639_1>en</dcc:usedLangCodeISO639_1>
    <dcc:mandatoryLangCodeISO639_1>en</dcc:mandatoryLangCodeISO639_1>
    <dcc:uniqueIdentifier>M-17L288</dcc:uniqueIdentifier>
    <dcc:beginPerformanceDate>2017-09-27</dcc:beginPerformanceDate>
    <dcc:endPerformanceDate>2017-09-27</dcc:endPerformanceDate>
  </dcc:coreData>
  <dcc:items>
    <dcc:item>
      <dcc:name>
        <dcc:content lang="en">Laser interferometer</dcc:content>
      </dcc:name>
      <dcc:description>
        <dcc:content lang="en">Laser interferometer</dcc:content>
      </dcc:description>
      <dcc:manufacturer>
        <dcc:name>
          <dcc:content>Hewlett Packard</dcc:content>
        </dcc:name>
      </dcc:manufacturer>
      <dcc:model>HP 5519</dcc:model>
      <dcc:identifications>
        <dcc:identification>
          <dcc:issuer>manufacturer</dcc:issuer>
          <dcc:value>3627A00909</dcc:value>
          <dcc:description>
            <dcc:content lang="en">Serial number</dcc:content>
          </dcc:description>
        </dcc:identification>
      </dcc:identifications>
    </dcc:item>
  </dcc:items>
  <dcc:calibrationLaboratory>
    <dcc:contact>
      <dcc:name>
        <dcc:content>National Metrology Institute VTT MIKES</dcc:content>
      </dcc:name>
      <dcc:eMail>firstname.lastname@vtt.fi</dcc:eMail>
      <dcc:location>
        <dcc:city>Espoo</dcc:city>
        <dcc:countryCode>FI</dcc:countryCode>
        <dcc:postCode>02150</dcc:postCode>
        <dcc:street>Tekniikantie</dcc:street>
        <dcc:streetNo>1</dcc:streetNo>
      </dcc:location>
    </dcc:contact>
  </dcc:calibrationLaboratory>
  <dcc:respPersons>
    <dcc:respPerson>
      <dcc:person>
        <dcc:name>
          <dcc:content>Antti Lassila</dcc:content>
        </dcc:name>
        <dcc:eMail>firstname.lastname@vtt.fi</dcc:eMail>
      </dcc:person>
      <dcc:role>Research team leader</dcc:role>
    </dcc:respPerson>
    <dcc:respPerson>
      <dcc:person>
        <dcc:name>
          <dcc:content>Jeremias Seppä</dcc:content>
        </dcc:name>
        <dcc:eMail>firstname.lastname@vtt.fi</dcc:eMail>
      </dcc:person>
      <dcc:role>Research scientist</dcc:role>
    </dcc:respPerson>
  </dcc:respPersons>
  <dcc:customer>
    <dcc:name>
      <dcc:content>National Metrology Institute VTT MIKES</dcc:content>
    </dcc:name>
    <dcc:eMail>firstname.lastname@vtt.fi</dcc:eMail>
    <dcc:location>
      <dcc:city>Espoo</dcc:city>
      <dcc:countryCode>FI</dcc:countryCode>
      <dcc:postCode>02150</dcc:postCode>
      <dcc:street>Sähkömiehentie</dcc:street>
      <dcc:streetNo>4</dcc:streetNo>
    </dcc:location>
  </dcc:customer>
  <dcc:statements>
    <dcc:statement>
      <dcc:convention>CIPM-MRA</dcc:convention>
      <dcc:valid>true</dcc:valid>
    </dcc:statement>
    <dcc:statement>
      <dcc:convention>Tracebility</dcc:convention>
      <dcc:declaration>
        <dcc:content lang="en">The calibration results presented in this certificate are traceable to the International System of Units (SI) via Finnish national measurement standards.</dcc:content>
      </dcc:declaration>
    </dcc:statement>
    <dcc:statement>
      <dcc:convention>JCGM 100:2008</dcc:convention>
      <dcc:declaration>
        <dcc:content lang="en">
          The frequency of the iodine stabilized laser (wavelength in a vacuum) is known with an uncertainty that is
          smaller than 4,2·10-11 (k = 2). The most dominant component of the measurement uncertainty in the calibration
          is the uncertainty of the calibrated laser; the uncertainties from other sources are small. The uncertainty
          given in addition to the measurement results represents the measurement uncertainty in the calibration
          conditions at the time of the calibration instance and the short interval repeatability of the calibrated laser.
          Measurement uncertainty for the results was obtained by multiplying it with the coverage factor k = 2. 
          Measurement uncertainty is estimated and stated according to GUM (Evaluation of measurement data – Guide
          to the expression of uncertainty in measurement, JCGM 100:2008).
        </dcc:content>
      </dcc:declaration>
    </dcc:statement>
    <dcc:statement>
      <dcc:declaration>
        <dcc:content lang="en">This certificate may not be reproduced partially, except with a written approval of the issuing National Standards Laboratory.</dcc:content>
      </dcc:declaration>
    </dcc:statement>
    <dcc:statement>
      <dcc:declaration>
        <dcc:content lang="en">VTT MIKES is the National Metrology Institute of Finland which designates the National Standards Laboratories and supervises their activities. The National Standards Laboratories are responsible for maintaining of national standards and their traceability to SI units. The Finnish national standards system is based on the Act No. 1156/1993 and the Decree No. 972/1994, including amendments. VTT MIKES is a part of VTT Technical Research Centre of Finland Ltd.</dcc:content>
      </dcc:declaration>
    </dcc:statement>
    <dcc:statement>
      <dcc:declaration>
        <dcc:content lang="en">This certificate is consistent with Calibration and Measurement Capabilities (CMCs) that are included in Appendix C of the Mutual Recognition Arrangement (CIPM MRA) drawn up by the International Committee for Weights and Measures (CIPM). Under the CIPM MRA, all participating institutes recognise the validity of each other’s calibration and measurement certificates for the quantities, ranges and measurement uncertainties specified in Appendix C (for details see http://www.bipm.org). VTT MIKES meets the requirements for calibration laboratories as defined in standard ISO/IEC 17025.</dcc:content>
      </dcc:declaration>
    </dcc:statement>
  </dcc:statements>
</dcc:administrativeData>
<dcc:measurementResults>
  <dcc:measurementResult>
    <dcc:name>
      <dcc:content lang="en">Length calibration results</dcc:content>
    </dcc:name>
    <dcc:usedMethods>
      <dcc:usedMethod>
        <dcc:name>
          <dcc:content lang="en">Calibration method</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content lang="en">
            The frequency of the laser was determined according to the operation instruction KMP 1.1.2. The laser was
            calibrated using frequency comparison by measuring the frequency difference between the horizontally polarized mode
            of the calibrated laser and an iodine stabilized HeNe laser. The beams of the examined laser and the iodine stailezed
            laser were combined using a beamsplitter and were directed to a photosensitive diode. The frequency of the diode
            output signal (the difference between the frequencies of the lasers) was measured with a frequenct counter that was
            controlled with a computer. The fequency difference between the vertically and horizontally polarized modes of the laser
            was measured directly with the frequency counter by placing a polarizer, the transmittance axis direction of which
            maximized the signal between the modes, on the path of the laser beam.
            Before the measurements began the laser was off for at least three hours. The laser was switched on from
            the power switch and the collection of the frequency difference data was started after the READY light of the laser lit
            up. The frequency difference data were collected over several hours. The stable frequency of the horizontally polarized
            mode of the laser was determined using the mean of the measured frequency differences and known frequency of the iodine
            stabilized laser. The determination was done using the measurement points in which the calibrated laser had been on at
            least 20000 seconds from the start of the frequency difference data collection. The Allan deviation between
            the calibrated laser and the iodine stabilized laser was also calculated from the results. The stable frequency of the
            vertically polarized mode of the laser was calculated using the measured frequency difference between the modes and
            the stable frequency of the horizontally polarized mode. The wavelengths in a vacuum are obtained from the determined
            frequencies using the speed of light in a vacuum. All the measurements except the under 10 s Allan deviations
            were done using a gate time of 10 s. The luminosity of the laser was measured using a calibrated power meter.
            Resolution, repeatability, angular errors and performance of the reference were factors in uncertainty estimate calculation.
          </dcc:content>
        </dcc:description>
      </dcc:usedMethod>
    </dcc:usedMethods>
    <dcc:measuringEquipments>
      <dcc:measuringEquipment>
        <dcc:name>
          <dcc:content lang="en">Iodine stabilized HeNe laser</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content lang="en">Iodine stabilized HeNe laser</dcc:content>
        </dcc:description>
        <dcc:identifications>
          <dcc:identification>
            <dcc:issuer>calibrationLaboratory</dcc:issuer>
            <dcc:value>MIKES003980</dcc:value>
          </dcc:identification>
        </dcc:identifications>
      </dcc:measuringEquipment>
      <dcc:measuringEquipment>
        <dcc:name>
          <dcc:content lang="en">Optical power meter</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content lang="en">Optical power meter</dcc:content>
        </dcc:description>
        <dcc:identifications>
          <dcc:identification>
            <dcc:issuer>calibrationLaboratory</dcc:issuer>
            <dcc:value>MIKES009877</dcc:value>
          </dcc:identification>
        </dcc:identifications>
      </dcc:measuringEquipment>
      <dcc:measuringEquipment>
        <dcc:name>
          <dcc:content lang="en">Frequency counter</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content lang="en">Frequency counter</dcc:content>
        </dcc:description>
        <dcc:identifications>
          <dcc:identification>
            <dcc:issuer>calibrationLaboratory</dcc:issuer>
            <dcc:value>MIKES003985</dcc:value>
          </dcc:identification>
        </dcc:identifications>
      </dcc:measuringEquipment>
    </dcc:measuringEquipments>
    <dcc:influenceConditions>
      <dcc:influenceCondition>
        <dcc:name>
          <dcc:content lang="en">Ambient conditions</dcc:content>
        </dcc:name>
        <dcc:status>beforeAdjustment</dcc:status>
        <dcc:data>
          <dcc:list refType="/duration">
            <dcc:quantity>
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>17</si:value>
                <si:unit>\hour</si:unit>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/temperature">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>20.1</si:value>
                <si:unit>\degreecelsius</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.1</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/pressure">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>1042</si:value>
                <si:unit>\hecto\pascal</si:unit>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/humidity">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.47</si:value>
                <si:unit>\kilogram\tothe{1}\metre\tothe{-3}\kilogram\tothe{-1}\metre\tothe{3}</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.02</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
        </dcc:data>
      </dcc:influenceCondition>
    </dcc:influenceConditions>
    <dcc:results>
      <dcc:result>
        <dcc:name>
          <dcc:content lang="en">Stable wavelength</dcc:content>
        </dcc:name>
        <dcc:data>
          <dcc:list refType="/horizontalPolarization">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>632.9913809</si:value>
                <si:unit>\nano\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.0000064</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/verticalPolarization">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>632.9913771</si:value>
                <si:unit>\nano\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.0000064</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
        </dcc:data>
      </dcc:result>
      <dcc:result>
        <dcc:name>
          <dcc:content lang="en">Stable frequency</dcc:content>
        </dcc:name>
        <dcc:data>
          <dcc:list refType="/horizontalPolarization">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>473612227.6</si:value>
                <si:unit>\mega\hertz</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>4.7</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/verticalPolarization">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>473612230.5</si:value>
                <si:unit>\mega\hertz</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>4.7</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
        </dcc:data>
      </dcc:result>
      <dcc:result>
        <dcc:name>
          <dcc:content lang="en">Range of variation</dcc:content>
        </dcc:name>
        <dcc:data>
          <dcc:list refType="/stableWavelenghtRange">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.0000063</si:value>
                <si:unit>\nano\metre</si:unit>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/totalWavelengthRange">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.00000033</si:value>
                <si:unit>\nano\metre</si:unit>
              </si:real>
            </dcc:quantity>
          </dcc:list>
        </dcc:data>
      </dcc:result>
      <dcc:result>
        <dcc:name>
          <dcc:content lang="en">Allan deviation (100 s, N=400)</dcc:content>
        </dcc:name>
        <dcc:data>
          <dcc:list refType="/allanDeviation">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.3</si:value>
                <si:unit>\kilo\hertz</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:measurementMetaData>
              <dcc:metaData refType="/supportQuantity">
                <dcc:data>
                  <dcc:list refType="/averagingTime">
                    <dcc:quantity refType="/measuredValue">
                      <si:real xmlns:si="https://ptb.de/si">
                        <si:value>100</si:value>
                        <si:unit>\second</si:unit>
                      </si:real>
                    </dcc:quantity>
                  </dcc:list>
                </dcc:data>
              </dcc:metaData>
              <dcc:metaData refType="/supportQuantity">
                <dcc:data>
                  <dcc:list refType="/numberOfSamples">
                    <dcc:quantity refType="/samples">
                      <si:real xmlns:si="https://ptb.de/si">
                        <si:value>400</si:value>
                        <si:unit>\one</si:unit>
                      </si:real>
                    </dcc:quantity>
                  </dcc:list>
                </dcc:data>
              </dcc:metaData>
            </dcc:measurementMetaData>
          </dcc:list>
        </dcc:data>
      </dcc:result>
      <dcc:result>
        <dcc:name>
          <dcc:content lang="en">Laser output power</dcc:content>
        </dcc:name>
        <dcc:data>
          <dcc:list refType="/power">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.38</si:value>
                <si:unit>\milli\watt</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.05</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
        </dcc:data>
      </dcc:result>
    </dcc:results>
  </dcc:measurementResult>
</dcc:measurementResults>
<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="id-6ed3ea267fa8a9f17b22813b23a2124a"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256"/><ds:Reference Id="r-id-6ed3ea267fa8a9f17b22813b23a2124a-1" URI=""><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/TR/1999/REC-xpath-19991116"><ds:XPath>not(ancestor-or-self::ds:Signature)</ds:XPath></ds:Transform><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>OIfenYcWzsdm2DWHH2BNrGp4j8NdiPRAgorYo1+gZbs=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903#SignedProperties" URI="#xades-id-6ed3ea267fa8a9f17b22813b23a2124a"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>qHFYfRdK475KD/fgwUlbmi83vhExypAT/k0zKaXe/tc=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="value-id-6ed3ea267fa8a9f17b22813b23a2124a">c4Zs8xrCspKnyDDrOIAN0AvcxMCzEcYX1AHAeV7MKYCQvxGL23MlbUJI9JLbd7Cfs7QrOd8NzNVdNWVFfbLDHg==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICYTCCAgegAwIBAgIUC7P5iuL3F3OwTA4gsT38SQOQaRcwCgYIKoZIzj0EAwIwdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxHTAbBgNVBAMTFEludGVybWVkaWFyeSBUZXN0IENBMB4XDTIxMDQyMzA2NDUwMFoXDTI1MDQyMjA2NDUwMFowczELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxGTAXBgNVBAMTEFRlc3QgQ2VydGlmaWNhdGUwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAASKHHf6RHFTVkNiUphF4N4/yfmUJMZ203pi3hDysht5GAK/hljXMVzksAtnGW4vhNcheNTcl9HOSX1KdlbQK47mo3UwczAOBgNVHQ8BAf8EBAMCBaAwEwYDVR0lBAwwCgYIKwYBBQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQURaEgNaGTM4ByFCz6Sb6uEDarKkswHwYDVR0jBBgwFoAUTYFZ14ynSoaA8S8Ls6+5hqBVXUowCgYIKoZIzj0EAwIDSAAwRQIgIGETvYTeKizBqip9LMEjjMzInYumUeZiIgsuyZypKI4CIQDyj2j9fqRpbET/+MiuP4RTMDp+60YhlHT+7QrmDrXPOg==</ds:X509Certificate><ds:X509Certificate>MIICUzCCAfmgAwIBAgIUYFAqkIvPBDd0KXzOZmAOaHh1GhgwCgYIKoZIzj0EAwIwdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB1V1c2ltYWExETAPBgNVBAcTCEhlbHNpbmtpMQ4wDAYDVQQKEwVBYWx0bzEOMAwGA1UECxMFQWFsdG8xIzAhBgNVBAMTGkFhbHRvIEludGVybWVkaWFyeSBUZXN0IENBMB4XDTIxMDQyMzA2NDUwMFoXDTI1MDQyMjA2NDUwMFowdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxHTAbBgNVBAMTFEludGVybWVkaWFyeSBUZXN0IENBMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEWud6EjKXIUchRzhOlVIKtXHIdvWlbzZ31JoEVb5r6RLiRIG05/T8/tVTamj9ApqFCMSwj3ahuDWBudqBbmWXEqNjMGEwDgYDVR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFE2BWdeMp0qGgPEvC7OvuYagVV1KMB8GA1UdIwQYMBaAFHfzhicsmfD2EWWSaf3ZPRgs79+UMAoGCCqGSM49BAMCA0gAMEUCIASEKNQtJwb5Ht0C5iO1I44OgE/zRuyhnQrPaDbIZ8wjAiEA3N4bj03HX/FgC/bnHrkK16hUT79Aluzy+IteJIGtNHQ=</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Object><xades:QualifyingProperties xmlns:xades="http://uri.etsi.org/01903/v1.3.2#" Target="#id-6ed3ea267fa8a9f17b22813b23a2124a"><xades:SignedProperties Id="xades-id-6ed3ea267fa8a9f17b22813b23a2124a"><xades:SignedSignatureProperties><xades:SigningTime>2021-06-15T09:50:23Z</xades:SigningTime><xades:SigningCertificateV2><xades:Cert><xades:CertDigest><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha512"/><ds:DigestValue>5w8jXxhfZB4DdB+OzdMp4VEpjbOWEdiWSegTQrlmgY+juZ2HynKY2mg4Arg1JITlk5zv0TJGA+uBcUg1jF2sqw==</ds:DigestValue></xades:CertDigest><xades:IssuerSerialV2>MIGTMHukeTB3MQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxETAPBgNVBAoTCEN1c3RvbWVyMREwDwYDVQQLEwhDdXN0b21lcjEdMBsGA1UEAxMUSW50ZXJtZWRpYXJ5IFRlc3QgQ0ECFAuz+Yri9xdzsEwOILE9/EkDkGkX</xades:IssuerSerialV2></xades:Cert></xades:SigningCertificateV2><xades:SignaturePolicyIdentifier><xades:SignaturePolicyImplied/></xades:SignaturePolicyIdentifier></xades:SignedSignatureProperties><xades:SignedDataObjectProperties><xades:DataObjectFormat ObjectReference="#r-id-6ed3ea267fa8a9f17b22813b23a2124a-1"><xades:MimeType>text/xml</xades:MimeType></xades:DataObjectFormat></xades:SignedDataObjectProperties></xades:SignedProperties></xades:QualifyingProperties></ds:Object></ds:Signature>
<dcc:cryptographicIdentifier>dcc://952000000.230/A9H62PILZsYbRGwl/mlm-hMydv4--ggjqFDLO70k9pAU</dcc:cryptographicIdentifier>
</dcc:digitalCalibrationCertificate>`

const M_20L330_str = `<?xml version="1.0" encoding="UTF-8" standalone="no"?><dcc:digitalCalibrationCertificate xmlns:dcc="https://ptb.de/dcc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" schemaVersion="3.0.0-rc.2" xsi:schemaLocation="https://ptb.de/dcc https://ptb.de/dcc/v3.0.0-rc.2/dcc.xsd">

<!--
    Copyright (c) 2020 - Aalto-korkeakoulusäätiö sr., all rights reserved

    This software was developed at the Aalto University. Aalto University assumes no responsibility whatsoever for its use by other parties, and makes no guarantees, expressed or implied, about its quality, reliability, safety, suitability or any other characteristic.
    As far as legally permitted Aalto University refuses any liability for any direct, indirect or consequential damage arising in connection with the use of this software.
    
    This project (17IND02) has received funding from the EMPIR programme co-financed by the Participating States and from the European Union's Horizon 2020 research and innovation programme.
-->

<dcc:administrativeData>
  <dcc:dccSoftware>
    <dcc:software>
      <dcc:name>
        <dcc:content>Notepad++ (32-bit)</dcc:content>
      </dcc:name>
      <dcc:release>v7.9.1</dcc:release>
    </dcc:software>
  </dcc:dccSoftware>
  <dcc:coreData>
    <dcc:countryCodeISO3166_1>FI</dcc:countryCodeISO3166_1>
    <dcc:usedLangCodeISO639_1>en</dcc:usedLangCodeISO639_1>
    <dcc:mandatoryLangCodeISO639_1>en</dcc:mandatoryLangCodeISO639_1>
    <dcc:uniqueIdentifier>M-20L330</dcc:uniqueIdentifier>
    <dcc:beginPerformanceDate>2020-11-20</dcc:beginPerformanceDate>
    <dcc:endPerformanceDate>2020-11-20</dcc:endPerformanceDate>
  </dcc:coreData>
  <dcc:items>
    <dcc:item>
      <dcc:name>
        <dcc:content lang="en">Electronic distance measurement instrument</dcc:content>
      </dcc:name>
      <dcc:description>
        <dcc:content lang="en">Electronic distance measurement instrument</dcc:content>
      </dcc:description>
      <dcc:manufacturer>
        <dcc:name>
          <dcc:content>SICK OY</dcc:content>
        </dcc:name>
        <dcc:eMail>sick@sick.fi</dcc:eMail>
        <dcc:location>
          <dcc:city>Vantaa</dcc:city>
          <dcc:countryCode>FI</dcc:countryCode>
          <dcc:postCode>01620</dcc:postCode>
          <dcc:street>Myllykivenkuja</dcc:street>
          <dcc:streetNo>1</dcc:streetNo>
        </dcc:location>
      </dcc:manufacturer>
      <dcc:model>DL100-21AA2112</dcc:model>
      <dcc:identifications>
        <dcc:identification>
          <dcc:issuer>manufacturer</dcc:issuer>
          <dcc:value>20410182</dcc:value>
          <dcc:description>
            <dcc:content lang="en">Serial number</dcc:content>
          </dcc:description>
        </dcc:identification>
      </dcc:identifications>
    </dcc:item>
  </dcc:items>
  <dcc:calibrationLaboratory>
    <dcc:contact>
      <dcc:name>
        <dcc:content>National Metrology Institute VTT MIKES</dcc:content>
      </dcc:name>
      <dcc:eMail>firstname.lastname@vtt.fi</dcc:eMail>
      <dcc:location>
        <dcc:city>Espoo</dcc:city>
        <dcc:countryCode>FI</dcc:countryCode>
        <dcc:postCode>02150</dcc:postCode>
        <dcc:street>Tekniikantie</dcc:street>
        <dcc:streetNo>1</dcc:streetNo>
      </dcc:location>
    </dcc:contact>
  </dcc:calibrationLaboratory>
  <dcc:respPersons>
    <dcc:respPerson>
      <dcc:person>
        <dcc:name>
          <dcc:content>Antti Lassila</dcc:content>
        </dcc:name>
        <dcc:eMail>firstname.lastname@vtt.fi</dcc:eMail>
      </dcc:person>
      <dcc:role>Research team leader</dcc:role>
    </dcc:respPerson>
    <dcc:respPerson>
      <dcc:person>
        <dcc:name>
          <dcc:content>Jarkko Unkuri</dcc:content>
        </dcc:name>
        <dcc:eMail>firstname.lastname@vtt.fi</dcc:eMail>
      </dcc:person>
      <dcc:role>Research scientist</dcc:role>
    </dcc:respPerson>
  </dcc:respPersons>
  <dcc:customer>
    <dcc:name>
      <dcc:content>Aalto University</dcc:content>
    </dcc:name>
    <dcc:eMail>firstname.lastname@aalto.fi</dcc:eMail>
    <dcc:location>
      <dcc:city>Espoo</dcc:city>
      <dcc:countryCode>FI</dcc:countryCode>
      <dcc:postCode>02150</dcc:postCode>
      <dcc:street>Sähkömiehentie</dcc:street>
      <dcc:streetNo>4</dcc:streetNo>
      <dcc:further>
        <dcc:content lang="en">Engineering Design</dcc:content>
      </dcc:further>
    </dcc:location>
  </dcc:customer>
  <dcc:statements>
    <dcc:statement>
      <dcc:convention>CIPM-MRA</dcc:convention>
      <dcc:declaration>
        <dcc:content lang="en">This certificate is consistent with Calibration and Measurement Capabilities (CMCs) that are included in Appendix C of the Mutual Recognition Arrangement (CIPM MRA) drawn up by the International Committee for Weights and Measures (CIPM). Under the CIPM MRA, all participating institutes recognise the validity of each other’s calibration and measurement certificates for the quantities, ranges and measurement uncertainties specified in Appendix C (for details see http://www.bipm.org). VTT MIKES meets the requirements for calibration laboratories as defined in standard ISO/IEC 17025.</dcc:content>
      </dcc:declaration>
    </dcc:statement>
    <dcc:statement>
      <dcc:convention>Tracebility</dcc:convention>
      <dcc:declaration>
        <dcc:content lang="en">Calibration results are traceable to the International System of Units (SI) via Finnish national measurement standards.</dcc:content>
      </dcc:declaration>
    </dcc:statement>
    <dcc:statement>
      <dcc:convention>JCGM 100:2008</dcc:convention>
      <dcc:declaration>
        <dcc:content lang="en">Measurement uncertainty is estimated and stated according to GUM (Evaluation of measurement data – Guide to the expression of uncertainty in measurement, JCGM 100:2008) using the coverage factor k = 2, which for a normal distribution corresponds to a probability of approximately 95 %.</dcc:content>
      </dcc:declaration>
    </dcc:statement>
    <dcc:statement>
      <dcc:declaration>
        <dcc:content lang="en">VTT MIKES is the National Metrology Institute of Finland which designates the National Standards Laboratories and supervises their activities. The National Standards Laboratories are responsible for maintaining of national standards and their traceability to SI units. The Finnish national standards system is based on the Act No. 1156/1993 and the Decree No. 972/1994, including amendments. VTT MIKES is a part of VTT Technical Research Centre of Finland Ltd.</dcc:content>
      </dcc:declaration>
    </dcc:statement>
  </dcc:statements>
</dcc:administrativeData>
<dcc:measurementResults>
  <dcc:measurementResult>
    <dcc:name>
      <dcc:content lang="en">Length calibration results</dcc:content>
    </dcc:name>
    <dcc:usedMethods>
      <dcc:usedMethod>
        <dcc:name>
          <dcc:content lang="en">Calibration method</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content lang="en">The instrument was calibrated according to the internal procedure 2.1.2a of the National Standards Laboratory. Resolution, repeatability, angular errors and performance of the reference were factors in uncertainty estimate calculation.</dcc:content>
        </dcc:description>
      </dcc:usedMethod>
    </dcc:usedMethods>
    <dcc:measuringEquipments>
      <dcc:measuringEquipment>
        <dcc:name>
          <dcc:content lang="en">Laser interferometer</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content lang="en">Laser interferometer</dcc:content>
        </dcc:description>
        <dcc:manufacturer>
          <dcc:name>
            <dcc:content>Hewlett Packard</dcc:content>
          </dcc:name>
        </dcc:manufacturer>
        <dcc:model>HP 5519</dcc:model>
        <dcc:identifications>
          <dcc:identification>
            <dcc:issuer>manufacturer</dcc:issuer>
            <dcc:value>3627A00909</dcc:value>
            <dcc:description>
              <dcc:content lang="en">Serial number</dcc:content>
            </dcc:description>
          </dcc:identification>
          <dcc:identification>
            <dcc:issuer>calibrationLaboratory</dcc:issuer>
            <dcc:value>MIKES002101</dcc:value>
          </dcc:identification>
        </dcc:identifications>
      </dcc:measuringEquipment>
    </dcc:measuringEquipments>
    <dcc:influenceConditions>
      <dcc:influenceCondition>
        <dcc:name>
          <dcc:content lang="en">Ambient conditions</dcc:content>
        </dcc:name>
        <dcc:status>beforeAdjustment</dcc:status>
        <dcc:data>
          <dcc:list refType="/temperature">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>19.9</si:value>
                <si:unit>\degreecelsius</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.1</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/pressure">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>1002</si:value>
                <si:unit>\hecto\pascal</si:unit>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/humidity">
            <dcc:quantity refType="/measuredValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.45</si:value>
                <si:unit>\kilogram\tothe{1}\metre\tothe{-3}\kilogram\tothe{-1}\metre\tothe{3}</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.02</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
        </dcc:data>
      </dcc:influenceCondition>
    </dcc:influenceConditions>
    <dcc:results>
      <dcc:result>
        <dcc:name>
          <dcc:content lang="en">Length from 150 mm</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content lang="en">
            The calibration results and uncertainty correspond to the condition of the calibrated instrument during the calibration. They don´t include any possible longer term changes.
            Zero point was at 150 mm.
            EDM-readout - error = real length
          </dcc:content>
        </dcc:description>
        <dcc:data>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.00</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.0</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.05</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.3</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.10</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>1.7</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.15</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.4</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.20</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.3</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.25</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.3</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.30</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>1.9</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.35</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.2</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.40</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.2</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.45</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.7</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.50</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.9</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>0.75</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>4.5</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>1.00</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>4.1</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>1.25</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.9</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>1.50</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.0</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.0</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.4</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.5</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.1</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.0</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.2</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.5</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.8</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>4</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.7</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>5</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.5</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>6</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.5</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>7</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.2</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>8</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.1</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>9</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.5</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>10</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.5</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>11</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.3</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>12</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.7</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>13</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.3</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>14</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.4</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>15</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.2</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>16</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.6</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>17</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.1</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>18</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.9</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>19</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.1</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>20</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.8</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>21</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.9</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>22</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.0</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>23</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.2</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>24</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.9</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>25</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.3</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>26</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.9</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>27</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>2.5</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>28</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.1</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>29</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.0</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
          <dcc:list refType="/length">
            <dcc:quantity refType="/referenceValue">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>30</si:value>
                <si:unit>\metre</si:unit>
              </si:real>
            </dcc:quantity>
            <dcc:quantity refType="/deviation">
              <si:real xmlns:si="https://ptb.de/si">
                <si:value>3.1</si:value>
                <si:unit>\milli\metre</si:unit>
                <si:expandedUnc>
                  <si:uncertainty>0.6</si:uncertainty>
                  <si:coverageFactor>2</si:coverageFactor>
                  <si:coverageProbability>0.95</si:coverageProbability>
                  <si:distribution>normal</si:distribution>
                </si:expandedUnc>
              </si:real>
            </dcc:quantity>
          </dcc:list>
        </dcc:data>
      </dcc:result>
    </dcc:results>
  </dcc:measurementResult>
</dcc:measurementResults>
<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="id-fdcf3c77e7cbb0aa8a9b9609a14d922f"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256"/><ds:Reference Id="r-id-fdcf3c77e7cbb0aa8a9b9609a14d922f-1" URI=""><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/TR/1999/REC-xpath-19991116"><ds:XPath>not(ancestor-or-self::ds:Signature)</ds:XPath></ds:Transform><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>8O+gjLvY0UXMXDqTUTFijtb44aKnIp44fFIgTQ43LDY=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903#SignedProperties" URI="#xades-id-fdcf3c77e7cbb0aa8a9b9609a14d922f"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>NWN1dGBWh6firZ1d0RoD/M+ox7hdx/VE5YDKxW52uKo=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="value-id-fdcf3c77e7cbb0aa8a9b9609a14d922f">T80McG52XvtaYEHA5HDU/O31HPHo8Pt2MWqcYEtMhIWLIo/WX2Jne9Wl3k12mR93GdhxPtqE+SW+P/kNKofu2w==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICYTCCAgegAwIBAgIUC7P5iuL3F3OwTA4gsT38SQOQaRcwCgYIKoZIzj0EAwIwdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxHTAbBgNVBAMTFEludGVybWVkaWFyeSBUZXN0IENBMB4XDTIxMDQyMzA2NDUwMFoXDTI1MDQyMjA2NDUwMFowczELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxGTAXBgNVBAMTEFRlc3QgQ2VydGlmaWNhdGUwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAASKHHf6RHFTVkNiUphF4N4/yfmUJMZ203pi3hDysht5GAK/hljXMVzksAtnGW4vhNcheNTcl9HOSX1KdlbQK47mo3UwczAOBgNVHQ8BAf8EBAMCBaAwEwYDVR0lBAwwCgYIKwYBBQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQURaEgNaGTM4ByFCz6Sb6uEDarKkswHwYDVR0jBBgwFoAUTYFZ14ynSoaA8S8Ls6+5hqBVXUowCgYIKoZIzj0EAwIDSAAwRQIgIGETvYTeKizBqip9LMEjjMzInYumUeZiIgsuyZypKI4CIQDyj2j9fqRpbET/+MiuP4RTMDp+60YhlHT+7QrmDrXPOg==</ds:X509Certificate><ds:X509Certificate>MIICUzCCAfmgAwIBAgIUYFAqkIvPBDd0KXzOZmAOaHh1GhgwCgYIKoZIzj0EAwIwdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB1V1c2ltYWExETAPBgNVBAcTCEhlbHNpbmtpMQ4wDAYDVQQKEwVBYWx0bzEOMAwGA1UECxMFQWFsdG8xIzAhBgNVBAMTGkFhbHRvIEludGVybWVkaWFyeSBUZXN0IENBMB4XDTIxMDQyMzA2NDUwMFoXDTI1MDQyMjA2NDUwMFowdzELMAkGA1UEBhMCRkkxEDAOBgNVBAgTB0ZpbmxhbmQxETAPBgNVBAcTCEhlbHNpbmtpMREwDwYDVQQKEwhDdXN0b21lcjERMA8GA1UECxMIQ3VzdG9tZXIxHTAbBgNVBAMTFEludGVybWVkaWFyeSBUZXN0IENBMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEWud6EjKXIUchRzhOlVIKtXHIdvWlbzZ31JoEVb5r6RLiRIG05/T8/tVTamj9ApqFCMSwj3ahuDWBudqBbmWXEqNjMGEwDgYDVR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFE2BWdeMp0qGgPEvC7OvuYagVV1KMB8GA1UdIwQYMBaAFHfzhicsmfD2EWWSaf3ZPRgs79+UMAoGCCqGSM49BAMCA0gAMEUCIASEKNQtJwb5Ht0C5iO1I44OgE/zRuyhnQrPaDbIZ8wjAiEA3N4bj03HX/FgC/bnHrkK16hUT79Aluzy+IteJIGtNHQ=</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Object><xades:QualifyingProperties xmlns:xades="http://uri.etsi.org/01903/v1.3.2#" Target="#id-fdcf3c77e7cbb0aa8a9b9609a14d922f"><xades:SignedProperties Id="xades-id-fdcf3c77e7cbb0aa8a9b9609a14d922f"><xades:SignedSignatureProperties><xades:SigningTime>2021-06-15T09:49:57Z</xades:SigningTime><xades:SigningCertificateV2><xades:Cert><xades:CertDigest><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha512"/><ds:DigestValue>5w8jXxhfZB4DdB+OzdMp4VEpjbOWEdiWSegTQrlmgY+juZ2HynKY2mg4Arg1JITlk5zv0TJGA+uBcUg1jF2sqw==</ds:DigestValue></xades:CertDigest><xades:IssuerSerialV2>MIGTMHukeTB3MQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxETAPBgNVBAoTCEN1c3RvbWVyMREwDwYDVQQLEwhDdXN0b21lcjEdMBsGA1UEAxMUSW50ZXJtZWRpYXJ5IFRlc3QgQ0ECFAuz+Yri9xdzsEwOILE9/EkDkGkX</xades:IssuerSerialV2></xades:Cert></xades:SigningCertificateV2><xades:SignaturePolicyIdentifier><xades:SignaturePolicyImplied/></xades:SignaturePolicyIdentifier></xades:SignedSignatureProperties><xades:SignedDataObjectProperties><xades:DataObjectFormat ObjectReference="#r-id-fdcf3c77e7cbb0aa8a9b9609a14d922f-1"><xades:MimeType>text/xml</xades:MimeType></xades:DataObjectFormat></xades:SignedDataObjectProperties></xades:SignedProperties></xades:QualifyingProperties></ds:Object></ds:Signature>
<dcc:cryptographicIdentifier>dcc://952000000.230/9dMtb2_j7LYEVE0a/Iw3CZXjWrIXXT5OeU9J42ehYl_4</dcc:cryptographicIdentifier>
</dcc:digitalCalibrationCertificate>`

const pos_dcc = `<?xml version="1.0" encoding="UTF-8" standalone="no"?><dcc:digitalCalibrationCertificate xmlns:dcc="https://ptb.de/dcc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" schemaVersion="213&quot;0" xsi:schemaLocation="https://ptb.de/dcc schema.xsd">
  <dcc:administrativeData>
    <dcc:dccSoftware>
      <dcc:software>
        <dcc:name>
          <dcc:content>string</dcc:content>
        </dcc:name>
        <dcc:release>string</dcc:release>
      </dcc:software>
    </dcc:dccSoftware>
    <dcc:coreData>
      <dcc:countryCodeISO3166_1>VY</dcc:countryCodeISO3166_1>
      <dcc:usedLangCodeISO639_1>ar</dcc:usedLangCodeISO639_1>
      <dcc:mandatoryLangCodeISO639_1>ua</dcc:mandatoryLangCodeISO639_1>
      <dcc:uniqueIdentifier>string</dcc:uniqueIdentifier>
      <dcc:beginPerformanceDate>1984-09-20</dcc:beginPerformanceDate>
      <dcc:endPerformanceDate>1999-05-11</dcc:endPerformanceDate>
    </dcc:coreData>
    <dcc:items>
      <dcc:item>
        <dcc:name>
          <dcc:content>Position measurement device name</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content>string</dcc:content>
        </dcc:description>
        <dcc:manufacturer>
          <dcc:name>
            <dcc:content>Position measurement device manufacturer</dcc:content>
          </dcc:name>
        </dcc:manufacturer>
        <dcc:model>Position measurement device model</dcc:model>
        <dcc:identifications>
          <dcc:identification>
            <dcc:issuer>calibrationLaboratory</dcc:issuer>
            <dcc:value>string</dcc:value>
          </dcc:identification>
        </dcc:identifications>
      </dcc:item>
    </dcc:items>
    <dcc:calibrationLaboratory>
      <dcc:contact>
        <dcc:name>
          <dcc:content>string</dcc:content>
        </dcc:name>
        <dcc:eMail>string</dcc:eMail>
        <dcc:location>
          <dcc:street>string</dcc:street>
        </dcc:location>
      </dcc:contact>
    </dcc:calibrationLaboratory>
    <dcc:respPersons>
      <dcc:respPerson>
        <dcc:person>
          <dcc:name>
            <dcc:content>string</dcc:content>
          </dcc:name>
        </dcc:person>
      </dcc:respPerson>
    </dcc:respPersons>
    <dcc:customer>
      <dcc:name>
        <dcc:content>string</dcc:content>
      </dcc:name>
      <dcc:eMail>string</dcc:eMail>
      <dcc:location>
        <dcc:state>string</dcc:state>
      </dcc:location>
    </dcc:customer>
    <dcc:statements>
      <dcc:statement/>
    </dcc:statements>
  </dcc:administrativeData>
  <dcc:measurementResults>
    <dcc:measurementResult>
      <dcc:results>
        <dcc:result>
          <dcc:name>
            <dcc:content>string</dcc:content>
          </dcc:name>
          <dcc:data>
            <dcc:quantity>
              <dcc:noQuantity>
                <dcc:content>string</dcc:content>
              </dcc:noQuantity>
            </dcc:quantity>
          </dcc:data>
        </dcc:result>
      </dcc:results>
    </dcc:measurementResult>
  </dcc:measurementResults>
  <dcc:comment/>
  <dcc:document>
    <dcc:name>
      <dcc:content>string</dcc:content>
    </dcc:name>
    <dcc:description>
      <dcc:content>string</dcc:content>
    </dcc:description>
    <dcc:fileName>string</dcc:fileName>
    <dcc:mimeType>string</dcc:mimeType>
    <dcc:data>YTM0NZomIzI2OTsmIzM0NTueYQ==</dcc:data>
  </dcc:document>
<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="id-3e827992bcd2242293608ab4e0061f03"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256"/><ds:Reference Id="r-id-3e827992bcd2242293608ab4e0061f03-1" URI=""><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/TR/1999/REC-xpath-19991116"><ds:XPath>not(ancestor-or-self::ds:Signature)</ds:XPath></ds:Transform><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>Kh+P2upUqWeA1HS3kMhSAIsPit4GrlCRkU8KF+ork+g=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903#SignedProperties" URI="#xades-id-3e827992bcd2242293608ab4e0061f03"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>vSxuviUqQgeKbP0Svi2t56ROhQegudryWcGSCB/ZW2I=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="value-id-3e827992bcd2242293608ab4e0061f03">UMr6KMA2iGOD1hJ0MDiuzTzxptCE4U28M1CNYaWnVyiUFvjSja7EqUOSvm5AImrZAl0bGyysOWwJAKiiOg4HUQ==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICgzCCAimgAwIBAgIUEkIuN7HJhF0x3tDXsyCNZGXZ++4wCgYIKoZIzj0EAwIwgYAxCzAJBgNVBAYTAkZJMRAwDgYDVQQIEwdGaW5sYW5kMREwDwYDVQQHEwhIZWxzaW5raTETMBEGA1UEChMKQ2xpZW50IG9yZzEZMBcGA1UECxMQVGVzdC1jbGllbnQtdW5pdDEcMBoGA1UEAxMTVGVzdCBDdXN0b21lciBJbnRlcjAeFw0yMDExMTMyMjQ4MDBaFw0yNTExMTIyMjQ4MDBaMIGKMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxGDAWBgNVBAoTD1Rlc3QtY2xpZW50LW9yZzEaMBgGA1UECxMRVGVzdC1lbmR1c2VyLXVuaXQxIDAeBgNVBAMTF1Rlc3QgQ2xpZW50IENlcnRpZmljYXRlMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEIfm/Dv6VYmKMlsFd1zxmQE0kkDOl9AiLk2F9yyhr4yuC90EYZqlDohK+J17xfJY/B+2CuIS0YcsJNlpJVcmnmKN1MHMwDgYDVR0PAQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFDcjuvWte+Eaez3F7kIGpi5i3b92MB8GA1UdIwQYMBaAFPfpGRQmSq4Ew6pIIVoD2YrzKWopMAoGCCqGSM49BAMCA0gAMEUCIQDLynvEHwOKj9RQAGh81VrkA/fFVIHGWTSPWAVYbl7j8AIgb6f+az4Dokz+QdvD0OHQtD4eixPoJizhMdNI13kGwGc=</ds:X509Certificate><ds:X509Certificate>MIICaTCCAhCgAwIBAgIUcKvQe4T2l7XS3s52g/hDkKGRZyQwCgYIKoZIzj0EAwIwgYAxCzAJBgNVBAYTAkZJMRAwDgYDVQQIEwdGaW5sYW5kMREwDwYDVQQHEwhIZWxzaW5raTEZMBcGA1UEChMQSW50ZXJtZWRpYXJ5IG9yZzESMBAGA1UECxMJVGVzdC11bml0MR0wGwYDVQQDExRUZXN0LUludGVybWVkaWFyeS1DQTAeFw0yMDExMTMyMjQ4MDBaFw0yNTExMTIyMjQ4MDBaMIGAMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxEzARBgNVBAoTCkNsaWVudCBvcmcxGTAXBgNVBAsTEFRlc3QtY2xpZW50LXVuaXQxHDAaBgNVBAMTE1Rlc3QgQ3VzdG9tZXIgSW50ZXIwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAARAcIpB+jPU4l1Wzt6+Z/mecAZh6xehhyFVRZLquw0+ZG/lZnFOD2SEu0+R3IdVrMwaV7PTM8B2BTR1VV+X6gwHo2YwZDAOBgNVHQ8BAf8EBAMCAYYwEgYDVR0TAQH/BAgwBgEB/wIBATAdBgNVHQ4EFgQU9+kZFCZKrgTDqkghWgPZivMpaikwHwYDVR0jBBgwFoAUHuSf4W37nN5YMSaLDjFLfRCB7k4wCgYIKoZIzj0EAwIDRwAwRAIgXpBtD63MIPdSHRlCiHl3FmQaqY/joq4/tgZX73qZH58CIGycWmrTcJmvnUwTvZN5qFE3GrwufgWxZ88fpaCb4BeH</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Object><xades:QualifyingProperties xmlns:xades="http://uri.etsi.org/01903/v1.3.2#" Target="#id-3e827992bcd2242293608ab4e0061f03"><xades:SignedProperties Id="xades-id-3e827992bcd2242293608ab4e0061f03"><xades:SignedSignatureProperties><xades:SigningTime>2021-01-07T05:46:17Z</xades:SigningTime><xades:SigningCertificateV2><xades:Cert><xades:CertDigest><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha512"/><ds:DigestValue>WHpPnci0pAXd61uUxl6z2J/jJmIj9BcfKD1BlUgGekOF9REkOr/WOY0rvf/OG4OcJvx+lG6DpU7I6OKbREcZKw==</ds:DigestValue></xades:CertDigest><xades:IssuerSerialV2>MIGfMIGGpIGDMIGAMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxEzARBgNVBAoTCkNsaWVudCBvcmcxGTAXBgNVBAsTEFRlc3QtY2xpZW50LXVuaXQxHDAaBgNVBAMTE1Rlc3QgQ3VzdG9tZXIgSW50ZXICFBJCLjexyYRdMd7Q17MgjWRl2fvu</xades:IssuerSerialV2></xades:Cert></xades:SigningCertificateV2><xades:SignaturePolicyIdentifier><xades:SignaturePolicyImplied/></xades:SignaturePolicyIdentifier></xades:SignedSignatureProperties><xades:SignedDataObjectProperties><xades:DataObjectFormat ObjectReference="#r-id-3e827992bcd2242293608ab4e0061f03-1"><xades:MimeType>text/xml</xades:MimeType></xades:DataObjectFormat></xades:SignedDataObjectProperties></xades:SignedProperties></xades:QualifyingProperties></ds:Object></ds:Signature>
<dcc:cryptographicIdentifier>dcc://952000000.230/u7pgYmL9aU3cYjYK/NnLziAsSo5t-YaIw1dqTxTtKz5g</dcc:cryptographicIdentifier>
</dcc:digitalCalibrationCertificate>`

const pos_ref_dcc = `<?xml version="1.0" encoding="UTF-8" standalone="no"?><dcc:digitalCalibrationCertificate xmlns:dcc="https://ptb.de/dcc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" schemaVersion="213&quot;0" xsi:schemaLocation="https://ptb.de/dcc schema.xsd">
  <dcc:administrativeData>
    <dcc:dccSoftware>
      <dcc:software>
        <dcc:name>
          <dcc:content>string</dcc:content>
        </dcc:name>
        <dcc:release>string</dcc:release>
      </dcc:software>
    </dcc:dccSoftware>
    <dcc:coreData>
      <dcc:countryCodeISO3166_1>VY</dcc:countryCodeISO3166_1>
      <dcc:usedLangCodeISO639_1>ar</dcc:usedLangCodeISO639_1>
      <dcc:mandatoryLangCodeISO639_1>ua</dcc:mandatoryLangCodeISO639_1>
      <dcc:uniqueIdentifier>string</dcc:uniqueIdentifier>
      <dcc:beginPerformanceDate>1984-09-20</dcc:beginPerformanceDate>
      <dcc:endPerformanceDate>1999-05-11</dcc:endPerformanceDate>
    </dcc:coreData>
    <dcc:items>
      <dcc:item>
        <dcc:name>
          <dcc:content>Position measurement device calibrator name</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content>string</dcc:content>
        </dcc:description>
        <dcc:manufacturer>
          <dcc:name>
            <dcc:content>Position measurement device calibrator manufacturer</dcc:content>
          </dcc:name>
        </dcc:manufacturer>
        <dcc:model>Position measurement device calibrator model</dcc:model>
        <dcc:identifications>
          <dcc:identification>
            <dcc:issuer>calibrationLaboratory</dcc:issuer>
            <dcc:value>string</dcc:value>
          </dcc:identification>
        </dcc:identifications>
      </dcc:item>
    </dcc:items>
    <dcc:calibrationLaboratory>
      <dcc:contact>
        <dcc:name>
          <dcc:content>string</dcc:content>
        </dcc:name>
        <dcc:eMail>string</dcc:eMail>
        <dcc:location>
          <dcc:street>string</dcc:street>
        </dcc:location>
      </dcc:contact>
    </dcc:calibrationLaboratory>
    <dcc:respPersons>
      <dcc:respPerson>
        <dcc:person>
          <dcc:name>
            <dcc:content>string</dcc:content>
          </dcc:name>
        </dcc:person>
      </dcc:respPerson>
    </dcc:respPersons>
    <dcc:customer>
      <dcc:name>
        <dcc:content>string</dcc:content>
      </dcc:name>
      <dcc:eMail>string</dcc:eMail>
      <dcc:location>
        <dcc:state>string</dcc:state>
      </dcc:location>
    </dcc:customer>
    <dcc:statements>
      <dcc:statement/>
    </dcc:statements>
  </dcc:administrativeData>
  <dcc:measurementResults>
    <dcc:measurementResult>
      <dcc:results>
        <dcc:result>
          <dcc:name>
            <dcc:content>string</dcc:content>
          </dcc:name>
          <dcc:data>
            <dcc:quantity>
              <dcc:noQuantity>
                <dcc:content>string</dcc:content>
              </dcc:noQuantity>
            </dcc:quantity>
          </dcc:data>
        </dcc:result>
      </dcc:results>
    </dcc:measurementResult>
  </dcc:measurementResults>
  <dcc:comment/>
  <dcc:document>
    <dcc:name>
      <dcc:content>string</dcc:content>
    </dcc:name>
    <dcc:description>
      <dcc:content>string</dcc:content>
    </dcc:description>
    <dcc:fileName>string</dcc:fileName>
    <dcc:mimeType>string</dcc:mimeType>
    <dcc:data>YTM0NZomIzI2OTsmIzM0NTueYQ==</dcc:data>
  </dcc:document>
<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="id-450611a99d6d11dd18ad5af77b7e52d7"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256"/><ds:Reference Id="r-id-450611a99d6d11dd18ad5af77b7e52d7-1" URI=""><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/TR/1999/REC-xpath-19991116"><ds:XPath>not(ancestor-or-self::ds:Signature)</ds:XPath></ds:Transform><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>afTOh7vRssM9vxIf1vKaEHWh6AY5P6bKaetNMOV17SM=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903#SignedProperties" URI="#xades-id-450611a99d6d11dd18ad5af77b7e52d7"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>DKZ8A8mkqXawR/CSDrUJV4FLAUZOOq4DiKwqQV8DaCM=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="value-id-450611a99d6d11dd18ad5af77b7e52d7">zxx3FDvUNFS18DKfvhT9RxfWbB92b2FITKunfi23Ki91peoKk9camgTQQWQHLBKuef02DT+ix/GFrB9ccRdytw==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICgzCCAimgAwIBAgIUEkIuN7HJhF0x3tDXsyCNZGXZ++4wCgYIKoZIzj0EAwIwgYAxCzAJBgNVBAYTAkZJMRAwDgYDVQQIEwdGaW5sYW5kMREwDwYDVQQHEwhIZWxzaW5raTETMBEGA1UEChMKQ2xpZW50IG9yZzEZMBcGA1UECxMQVGVzdC1jbGllbnQtdW5pdDEcMBoGA1UEAxMTVGVzdCBDdXN0b21lciBJbnRlcjAeFw0yMDExMTMyMjQ4MDBaFw0yNTExMTIyMjQ4MDBaMIGKMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxGDAWBgNVBAoTD1Rlc3QtY2xpZW50LW9yZzEaMBgGA1UECxMRVGVzdC1lbmR1c2VyLXVuaXQxIDAeBgNVBAMTF1Rlc3QgQ2xpZW50IENlcnRpZmljYXRlMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEIfm/Dv6VYmKMlsFd1zxmQE0kkDOl9AiLk2F9yyhr4yuC90EYZqlDohK+J17xfJY/B+2CuIS0YcsJNlpJVcmnmKN1MHMwDgYDVR0PAQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFDcjuvWte+Eaez3F7kIGpi5i3b92MB8GA1UdIwQYMBaAFPfpGRQmSq4Ew6pIIVoD2YrzKWopMAoGCCqGSM49BAMCA0gAMEUCIQDLynvEHwOKj9RQAGh81VrkA/fFVIHGWTSPWAVYbl7j8AIgb6f+az4Dokz+QdvD0OHQtD4eixPoJizhMdNI13kGwGc=</ds:X509Certificate><ds:X509Certificate>MIICaTCCAhCgAwIBAgIUcKvQe4T2l7XS3s52g/hDkKGRZyQwCgYIKoZIzj0EAwIwgYAxCzAJBgNVBAYTAkZJMRAwDgYDVQQIEwdGaW5sYW5kMREwDwYDVQQHEwhIZWxzaW5raTEZMBcGA1UEChMQSW50ZXJtZWRpYXJ5IG9yZzESMBAGA1UECxMJVGVzdC11bml0MR0wGwYDVQQDExRUZXN0LUludGVybWVkaWFyeS1DQTAeFw0yMDExMTMyMjQ4MDBaFw0yNTExMTIyMjQ4MDBaMIGAMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxEzARBgNVBAoTCkNsaWVudCBvcmcxGTAXBgNVBAsTEFRlc3QtY2xpZW50LXVuaXQxHDAaBgNVBAMTE1Rlc3QgQ3VzdG9tZXIgSW50ZXIwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAARAcIpB+jPU4l1Wzt6+Z/mecAZh6xehhyFVRZLquw0+ZG/lZnFOD2SEu0+R3IdVrMwaV7PTM8B2BTR1VV+X6gwHo2YwZDAOBgNVHQ8BAf8EBAMCAYYwEgYDVR0TAQH/BAgwBgEB/wIBATAdBgNVHQ4EFgQU9+kZFCZKrgTDqkghWgPZivMpaikwHwYDVR0jBBgwFoAUHuSf4W37nN5YMSaLDjFLfRCB7k4wCgYIKoZIzj0EAwIDRwAwRAIgXpBtD63MIPdSHRlCiHl3FmQaqY/joq4/tgZX73qZH58CIGycWmrTcJmvnUwTvZN5qFE3GrwufgWxZ88fpaCb4BeH</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Object><xades:QualifyingProperties xmlns:xades="http://uri.etsi.org/01903/v1.3.2#" Target="#id-450611a99d6d11dd18ad5af77b7e52d7"><xades:SignedProperties Id="xades-id-450611a99d6d11dd18ad5af77b7e52d7"><xades:SignedSignatureProperties><xades:SigningTime>2021-01-07T05:47:24Z</xades:SigningTime><xades:SigningCertificateV2><xades:Cert><xades:CertDigest><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha512"/><ds:DigestValue>WHpPnci0pAXd61uUxl6z2J/jJmIj9BcfKD1BlUgGekOF9REkOr/WOY0rvf/OG4OcJvx+lG6DpU7I6OKbREcZKw==</ds:DigestValue></xades:CertDigest><xades:IssuerSerialV2>MIGfMIGGpIGDMIGAMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxEzARBgNVBAoTCkNsaWVudCBvcmcxGTAXBgNVBAsTEFRlc3QtY2xpZW50LXVuaXQxHDAaBgNVBAMTE1Rlc3QgQ3VzdG9tZXIgSW50ZXICFBJCLjexyYRdMd7Q17MgjWRl2fvu</xades:IssuerSerialV2></xades:Cert></xades:SigningCertificateV2><xades:SignaturePolicyIdentifier><xades:SignaturePolicyImplied/></xades:SignaturePolicyIdentifier></xades:SignedSignatureProperties><xades:SignedDataObjectProperties><xades:DataObjectFormat ObjectReference="#r-id-450611a99d6d11dd18ad5af77b7e52d7-1"><xades:MimeType>text/xml</xades:MimeType></xades:DataObjectFormat></xades:SignedDataObjectProperties></xades:SignedProperties></xades:QualifyingProperties></ds:Object></ds:Signature>
<dcc:cryptographicIdentifier>dcc://952000000.230/-kiZbw38WtG0k7So/to_NSuwnO3_VfJg7y8Tobq1xZEg</dcc:cryptographicIdentifier>
</dcc:digitalCalibrationCertificate>`

const weight_dcc = `<?xml version="1.0" encoding="UTF-8" standalone="no"?><dcc:digitalCalibrationCertificate xmlns:dcc="https://ptb.de/dcc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" schemaVersion="213&quot;0" xsi:schemaLocation="https://ptb.de/dcc schema.xsd">
  <dcc:administrativeData>
    <dcc:dccSoftware>
      <dcc:software>
        <dcc:name>
          <dcc:content>string</dcc:content>
        </dcc:name>
        <dcc:release>string</dcc:release>
      </dcc:software>
    </dcc:dccSoftware>
    <dcc:coreData>
      <dcc:countryCodeISO3166_1>VY</dcc:countryCodeISO3166_1>
      <dcc:usedLangCodeISO639_1>ar</dcc:usedLangCodeISO639_1>
      <dcc:mandatoryLangCodeISO639_1>ua</dcc:mandatoryLangCodeISO639_1>
      <dcc:uniqueIdentifier>string</dcc:uniqueIdentifier>
      <dcc:beginPerformanceDate>1984-09-20</dcc:beginPerformanceDate>
      <dcc:endPerformanceDate>1999-05-11</dcc:endPerformanceDate>
    </dcc:coreData>
    <dcc:items>
      <dcc:item>
        <dcc:name>
          <dcc:content>Weight measurement device name</dcc:content>
        </dcc:name>
        <dcc:description>
          <dcc:content>string</dcc:content>
        </dcc:description>
        <dcc:manufacturer>
          <dcc:name>
            <dcc:content>Weight measurement device manufacturer</dcc:content>
          </dcc:name>
        </dcc:manufacturer>
        <dcc:model>Weight measurement device model</dcc:model>
        <dcc:identifications>
          <dcc:identification>
            <dcc:issuer>calibrationLaboratory</dcc:issuer>
            <dcc:value>string</dcc:value>
          </dcc:identification>
        </dcc:identifications>
      </dcc:item>
    </dcc:items>
    <dcc:calibrationLaboratory>
      <dcc:contact>
        <dcc:name>
          <dcc:content>string</dcc:content>
        </dcc:name>
        <dcc:eMail>string</dcc:eMail>
        <dcc:location>
          <dcc:street>string</dcc:street>
        </dcc:location>
      </dcc:contact>
    </dcc:calibrationLaboratory>
    <dcc:respPersons>
      <dcc:respPerson>
        <dcc:person>
          <dcc:name>
            <dcc:content>string</dcc:content>
          </dcc:name>
        </dcc:person>
      </dcc:respPerson>
    </dcc:respPersons>
    <dcc:customer>
      <dcc:name>
        <dcc:content>string</dcc:content>
      </dcc:name>
      <dcc:eMail>string</dcc:eMail>
      <dcc:location>
        <dcc:state>string</dcc:state>
      </dcc:location>
    </dcc:customer>
    <dcc:statements>
      <dcc:statement/>
    </dcc:statements>
  </dcc:administrativeData>
  <dcc:measurementResults>
    <dcc:measurementResult>
      <dcc:results>
        <dcc:result>
          <dcc:name>
            <dcc:content>string</dcc:content>
          </dcc:name>
          <dcc:data>
            <dcc:quantity>
              <dcc:noQuantity>
                <dcc:content>string</dcc:content>
              </dcc:noQuantity>
            </dcc:quantity>
          </dcc:data>
        </dcc:result>
      </dcc:results>
    </dcc:measurementResult>
  </dcc:measurementResults>
  <dcc:comment/>
  <dcc:document>
    <dcc:name>
      <dcc:content>string</dcc:content>
    </dcc:name>
    <dcc:description>
      <dcc:content>string</dcc:content>
    </dcc:description>
    <dcc:fileName>string</dcc:fileName>
    <dcc:mimeType>string</dcc:mimeType>
    <dcc:data>YTM0NZomIzI2OTsmIzM0NTueYQ==</dcc:data>
  </dcc:document>
<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="id-175a35e10a6ff43af91926c78b2afdc1"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256"/><ds:Reference Id="r-id-175a35e10a6ff43af91926c78b2afdc1-1" URI=""><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/TR/1999/REC-xpath-19991116"><ds:XPath>not(ancestor-or-self::ds:Signature)</ds:XPath></ds:Transform><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>MNnPNT46SPmeT/0ACWooUJGIL/cj3L7T5NzEJZfsw8w=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903#SignedProperties" URI="#xades-id-175a35e10a6ff43af91926c78b2afdc1"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>s36cUnG1xmw6z54ttj+JbT0PgagDCoQCCKrdjpahAME=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="value-id-175a35e10a6ff43af91926c78b2afdc1">Ykskl+Mbd4FQv8+wW+7Olv8E2kYgfsksk3WA5JOp+4EXxL6b1f7GYxedpTNlX+FgzCwDv5+hKMSvoEzqC9XZJA==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICgzCCAimgAwIBAgIUEkIuN7HJhF0x3tDXsyCNZGXZ++4wCgYIKoZIzj0EAwIwgYAxCzAJBgNVBAYTAkZJMRAwDgYDVQQIEwdGaW5sYW5kMREwDwYDVQQHEwhIZWxzaW5raTETMBEGA1UEChMKQ2xpZW50IG9yZzEZMBcGA1UECxMQVGVzdC1jbGllbnQtdW5pdDEcMBoGA1UEAxMTVGVzdCBDdXN0b21lciBJbnRlcjAeFw0yMDExMTMyMjQ4MDBaFw0yNTExMTIyMjQ4MDBaMIGKMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxGDAWBgNVBAoTD1Rlc3QtY2xpZW50LW9yZzEaMBgGA1UECxMRVGVzdC1lbmR1c2VyLXVuaXQxIDAeBgNVBAMTF1Rlc3QgQ2xpZW50IENlcnRpZmljYXRlMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEIfm/Dv6VYmKMlsFd1zxmQE0kkDOl9AiLk2F9yyhr4yuC90EYZqlDohK+J17xfJY/B+2CuIS0YcsJNlpJVcmnmKN1MHMwDgYDVR0PAQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFDcjuvWte+Eaez3F7kIGpi5i3b92MB8GA1UdIwQYMBaAFPfpGRQmSq4Ew6pIIVoD2YrzKWopMAoGCCqGSM49BAMCA0gAMEUCIQDLynvEHwOKj9RQAGh81VrkA/fFVIHGWTSPWAVYbl7j8AIgb6f+az4Dokz+QdvD0OHQtD4eixPoJizhMdNI13kGwGc=</ds:X509Certificate><ds:X509Certificate>MIICaTCCAhCgAwIBAgIUcKvQe4T2l7XS3s52g/hDkKGRZyQwCgYIKoZIzj0EAwIwgYAxCzAJBgNVBAYTAkZJMRAwDgYDVQQIEwdGaW5sYW5kMREwDwYDVQQHEwhIZWxzaW5raTEZMBcGA1UEChMQSW50ZXJtZWRpYXJ5IG9yZzESMBAGA1UECxMJVGVzdC11bml0MR0wGwYDVQQDExRUZXN0LUludGVybWVkaWFyeS1DQTAeFw0yMDExMTMyMjQ4MDBaFw0yNTExMTIyMjQ4MDBaMIGAMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxEzARBgNVBAoTCkNsaWVudCBvcmcxGTAXBgNVBAsTEFRlc3QtY2xpZW50LXVuaXQxHDAaBgNVBAMTE1Rlc3QgQ3VzdG9tZXIgSW50ZXIwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAARAcIpB+jPU4l1Wzt6+Z/mecAZh6xehhyFVRZLquw0+ZG/lZnFOD2SEu0+R3IdVrMwaV7PTM8B2BTR1VV+X6gwHo2YwZDAOBgNVHQ8BAf8EBAMCAYYwEgYDVR0TAQH/BAgwBgEB/wIBATAdBgNVHQ4EFgQU9+kZFCZKrgTDqkghWgPZivMpaikwHwYDVR0jBBgwFoAUHuSf4W37nN5YMSaLDjFLfRCB7k4wCgYIKoZIzj0EAwIDRwAwRAIgXpBtD63MIPdSHRlCiHl3FmQaqY/joq4/tgZX73qZH58CIGycWmrTcJmvnUwTvZN5qFE3GrwufgWxZ88fpaCb4BeH</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Object><xades:QualifyingProperties xmlns:xades="http://uri.etsi.org/01903/v1.3.2#" Target="#id-175a35e10a6ff43af91926c78b2afdc1"><xades:SignedProperties Id="xades-id-175a35e10a6ff43af91926c78b2afdc1"><xades:SignedSignatureProperties><xades:SigningTime>2021-01-07T05:00:26Z</xades:SigningTime><xades:SigningCertificateV2><xades:Cert><xades:CertDigest><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha512"/><ds:DigestValue>WHpPnci0pAXd61uUxl6z2J/jJmIj9BcfKD1BlUgGekOF9REkOr/WOY0rvf/OG4OcJvx+lG6DpU7I6OKbREcZKw==</ds:DigestValue></xades:CertDigest><xades:IssuerSerialV2>MIGfMIGGpIGDMIGAMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxEzARBgNVBAoTCkNsaWVudCBvcmcxGTAXBgNVBAsTEFRlc3QtY2xpZW50LXVuaXQxHDAaBgNVBAMTE1Rlc3QgQ3VzdG9tZXIgSW50ZXICFBJCLjexyYRdMd7Q17MgjWRl2fvu</xades:IssuerSerialV2></xades:Cert></xades:SigningCertificateV2><xades:SignaturePolicyIdentifier><xades:SignaturePolicyImplied/></xades:SignaturePolicyIdentifier></xades:SignedSignatureProperties><xades:SignedDataObjectProperties><xades:DataObjectFormat ObjectReference="#r-id-175a35e10a6ff43af91926c78b2afdc1-1"><xades:MimeType>text/xml</xades:MimeType></xades:DataObjectFormat></xades:SignedDataObjectProperties></xades:SignedProperties></xades:QualifyingProperties></ds:Object></ds:Signature>
<dcc:cryptographicIdentifier>dcc://952000000.230/zZ9kkke2lvRLFSxj/fRvNeNmZQgNG3flEgKnLDWCYzy0</dcc:cryptographicIdentifier>
</dcc:digitalCalibrationCertificate>`

const weight_ref_dcc = `<?xml version="1.0" encoding="UTF-8" standalone="no"?><dcc:digitalCalibrationCertificate xmlns:dcc="https://ptb.de/dcc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" schemaVersion="213&quot;0" xsi:schemaLocation="https://ptb.de/dcc schema.xsd">
<dcc:administrativeData>
  <dcc:dccSoftware>
    <dcc:software>
      <dcc:name>
        <dcc:content>string</dcc:content>
      </dcc:name>
      <dcc:release>string</dcc:release>
    </dcc:software>
  </dcc:dccSoftware>
  <dcc:coreData>
    <dcc:countryCodeISO3166_1>VY</dcc:countryCodeISO3166_1>
    <dcc:usedLangCodeISO639_1>ar</dcc:usedLangCodeISO639_1>
    <dcc:mandatoryLangCodeISO639_1>ua</dcc:mandatoryLangCodeISO639_1>
    <dcc:uniqueIdentifier>string</dcc:uniqueIdentifier>
    <dcc:beginPerformanceDate>1984-09-20</dcc:beginPerformanceDate>
    <dcc:endPerformanceDate>1999-05-11</dcc:endPerformanceDate>
  </dcc:coreData>
  <dcc:items>
    <dcc:item>
      <dcc:name>
        <dcc:content>Weight measurement device calibrator name</dcc:content>
      </dcc:name>
      <dcc:description>
        <dcc:content>string</dcc:content>
      </dcc:description>
      <dcc:manufacturer>
        <dcc:name>
          <dcc:content>Weight measurement device calibrator manufacturer</dcc:content>
        </dcc:name>
      </dcc:manufacturer>
      <dcc:model>Weight measurement device calibrator model</dcc:model>
      <dcc:identifications>
        <dcc:identification>
          <dcc:issuer>calibrationLaboratory</dcc:issuer>
          <dcc:value>string</dcc:value>
        </dcc:identification>
      </dcc:identifications>
    </dcc:item>
  </dcc:items>
  <dcc:calibrationLaboratory>
    <dcc:contact>
      <dcc:name>
        <dcc:content>string</dcc:content>
      </dcc:name>
      <dcc:eMail>string</dcc:eMail>
      <dcc:location>
        <dcc:street>string</dcc:street>
      </dcc:location>
    </dcc:contact>
  </dcc:calibrationLaboratory>
  <dcc:respPersons>
    <dcc:respPerson>
      <dcc:person>
        <dcc:name>
          <dcc:content>string</dcc:content>
        </dcc:name>
      </dcc:person>
    </dcc:respPerson>
  </dcc:respPersons>
  <dcc:customer>
    <dcc:name>
      <dcc:content>string</dcc:content>
    </dcc:name>
    <dcc:eMail>string</dcc:eMail>
    <dcc:location>
      <dcc:state>string</dcc:state>
    </dcc:location>
  </dcc:customer>
  <dcc:statements>
    <dcc:statement/>
  </dcc:statements>
</dcc:administrativeData>
<dcc:measurementResults>
  <dcc:measurementResult>
    <dcc:results>
      <dcc:result>
        <dcc:name>
          <dcc:content>string</dcc:content>
        </dcc:name>
        <dcc:data>
          <dcc:quantity>
            <dcc:noQuantity>
              <dcc:content>string</dcc:content>
            </dcc:noQuantity>
          </dcc:quantity>
        </dcc:data>
      </dcc:result>
    </dcc:results>
  </dcc:measurementResult>
</dcc:measurementResults>
<dcc:comment/>
<dcc:document>
  <dcc:name>
    <dcc:content>string</dcc:content>
  </dcc:name>
  <dcc:description>
    <dcc:content>string</dcc:content>
  </dcc:description>
  <dcc:fileName>string</dcc:fileName>
  <dcc:mimeType>string</dcc:mimeType>
  <dcc:data>YTM0NZomIzI2OTsmIzM0NTueYQ==</dcc:data>
</dcc:document>
<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="id-8083b950c1c017777dc7c5a44326b78c"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256"/><ds:Reference Id="r-id-8083b950c1c017777dc7c5a44326b78c-1" URI=""><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/TR/1999/REC-xpath-19991116"><ds:XPath>not(ancestor-or-self::ds:Signature)</ds:XPath></ds:Transform><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>YZXIBtIQLUErPq5/BqhyFyoTsIuC/JkKuBseMzY2yHU=</ds:DigestValue></ds:Reference><ds:Reference Type="http://uri.etsi.org/01903#SignedProperties" URI="#xades-id-8083b950c1c017777dc7c5a44326b78c"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>6jgAxz2LX+oCzU0zTyJ7SIsvrZSuU+uy53yY8v9U+Tk=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id="value-id-8083b950c1c017777dc7c5a44326b78c">U5tFmoIV0PfWFwp0YGFKlDK5Opdumto6KCRHrcJzkDXFVKC+dWLqfDffe1DaCH9EM5vbnsPl//I6bV15PLfQRQ==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICgzCCAimgAwIBAgIUEkIuN7HJhF0x3tDXsyCNZGXZ++4wCgYIKoZIzj0EAwIwgYAxCzAJBgNVBAYTAkZJMRAwDgYDVQQIEwdGaW5sYW5kMREwDwYDVQQHEwhIZWxzaW5raTETMBEGA1UEChMKQ2xpZW50IG9yZzEZMBcGA1UECxMQVGVzdC1jbGllbnQtdW5pdDEcMBoGA1UEAxMTVGVzdCBDdXN0b21lciBJbnRlcjAeFw0yMDExMTMyMjQ4MDBaFw0yNTExMTIyMjQ4MDBaMIGKMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxGDAWBgNVBAoTD1Rlc3QtY2xpZW50LW9yZzEaMBgGA1UECxMRVGVzdC1lbmR1c2VyLXVuaXQxIDAeBgNVBAMTF1Rlc3QgQ2xpZW50IENlcnRpZmljYXRlMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEIfm/Dv6VYmKMlsFd1zxmQE0kkDOl9AiLk2F9yyhr4yuC90EYZqlDohK+J17xfJY/B+2CuIS0YcsJNlpJVcmnmKN1MHMwDgYDVR0PAQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFDcjuvWte+Eaez3F7kIGpi5i3b92MB8GA1UdIwQYMBaAFPfpGRQmSq4Ew6pIIVoD2YrzKWopMAoGCCqGSM49BAMCA0gAMEUCIQDLynvEHwOKj9RQAGh81VrkA/fFVIHGWTSPWAVYbl7j8AIgb6f+az4Dokz+QdvD0OHQtD4eixPoJizhMdNI13kGwGc=</ds:X509Certificate><ds:X509Certificate>MIICaTCCAhCgAwIBAgIUcKvQe4T2l7XS3s52g/hDkKGRZyQwCgYIKoZIzj0EAwIwgYAxCzAJBgNVBAYTAkZJMRAwDgYDVQQIEwdGaW5sYW5kMREwDwYDVQQHEwhIZWxzaW5raTEZMBcGA1UEChMQSW50ZXJtZWRpYXJ5IG9yZzESMBAGA1UECxMJVGVzdC11bml0MR0wGwYDVQQDExRUZXN0LUludGVybWVkaWFyeS1DQTAeFw0yMDExMTMyMjQ4MDBaFw0yNTExMTIyMjQ4MDBaMIGAMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxEzARBgNVBAoTCkNsaWVudCBvcmcxGTAXBgNVBAsTEFRlc3QtY2xpZW50LXVuaXQxHDAaBgNVBAMTE1Rlc3QgQ3VzdG9tZXIgSW50ZXIwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAARAcIpB+jPU4l1Wzt6+Z/mecAZh6xehhyFVRZLquw0+ZG/lZnFOD2SEu0+R3IdVrMwaV7PTM8B2BTR1VV+X6gwHo2YwZDAOBgNVHQ8BAf8EBAMCAYYwEgYDVR0TAQH/BAgwBgEB/wIBATAdBgNVHQ4EFgQU9+kZFCZKrgTDqkghWgPZivMpaikwHwYDVR0jBBgwFoAUHuSf4W37nN5YMSaLDjFLfRCB7k4wCgYIKoZIzj0EAwIDRwAwRAIgXpBtD63MIPdSHRlCiHl3FmQaqY/joq4/tgZX73qZH58CIGycWmrTcJmvnUwTvZN5qFE3GrwufgWxZ88fpaCb4BeH</ds:X509Certificate></ds:X509Data></ds:KeyInfo><ds:Object><xades:QualifyingProperties xmlns:xades="http://uri.etsi.org/01903/v1.3.2#" Target="#id-8083b950c1c017777dc7c5a44326b78c"><xades:SignedProperties Id="xades-id-8083b950c1c017777dc7c5a44326b78c"><xades:SignedSignatureProperties><xades:SigningTime>2021-01-05T11:58:49Z</xades:SigningTime><xades:SigningCertificateV2><xades:Cert><xades:CertDigest><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha512"/><ds:DigestValue>WHpPnci0pAXd61uUxl6z2J/jJmIj9BcfKD1BlUgGekOF9REkOr/WOY0rvf/OG4OcJvx+lG6DpU7I6OKbREcZKw==</ds:DigestValue></xades:CertDigest><xades:IssuerSerialV2>MIGfMIGGpIGDMIGAMQswCQYDVQQGEwJGSTEQMA4GA1UECBMHRmlubGFuZDERMA8GA1UEBxMISGVsc2lua2kxEzARBgNVBAoTCkNsaWVudCBvcmcxGTAXBgNVBAsTEFRlc3QtY2xpZW50LXVuaXQxHDAaBgNVBAMTE1Rlc3QgQ3VzdG9tZXIgSW50ZXICFBJCLjexyYRdMd7Q17MgjWRl2fvu</xades:IssuerSerialV2></xades:Cert></xades:SigningCertificateV2><xades:SignaturePolicyIdentifier><xades:SignaturePolicyImplied/></xades:SignaturePolicyIdentifier></xades:SignedSignatureProperties><xades:SignedDataObjectProperties><xades:DataObjectFormat ObjectReference="#r-id-8083b950c1c017777dc7c5a44326b78c-1"><xades:MimeType>text/xml</xades:MimeType></xades:DataObjectFormat></xades:SignedDataObjectProperties></xades:SignedProperties></xades:QualifyingProperties></ds:Object></ds:Signature>
<dcc:cryptographicIdentifier>dcc://952000000.230/776Yv_yXYRcrSd-c/1P88HT1oGiE3Llw4REZiGJVOhvg</dcc:cryptographicIdentifier>
</dcc:digitalCalibrationCertificate>`

export { pos_dcc, pos_ref_dcc, weight_dcc, weight_ref_dcc, AIIC_K16052_str, AIIC_M91300_str, M_17L288_str, M_20L330_str}