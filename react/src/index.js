import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import { createMuiTheme } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#dddddd'
    }
  },
  props: {
    MuiButton: {
      variant: 'contained',
      style: {
        margin: '10px'
      }
    },
    MuiTextField: {
      variant: 'outlined',
      fullWidth: true
    },
    MuiAccordionSummary: {
      style: {
        padding: '10px',
      }
    },
    MuiAccordionDetails: {
      style: {
        display: 'flex',
        justifyContent: 'space-evenly',
        margin: 'auto',
        alignItems: 'center',
        padding: '0px',
        backgroundColor: '#525252'
      }
    }
  },
  overrides: {
    MuiTimelineItem: {
      missingOppositeContent: {
        "&:before": {
          display: "none"
        }
      }
    }
  }
})

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
