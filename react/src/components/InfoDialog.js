import React from 'react'
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const InfoDialog = ({ open, onClose }) => {

  const InfoText = () => (
    <div className={classes.infoText}>
      <p>This demonstrator was developed in EMPIR 17IND02 SmartCom project. For more information visit <br/><a href='https://www.ptb.de/empir2018/smartcom/project/'>https://www.ptb.de/empir2018/smartcom/project/</a></p>
      <p>This project has received funding from the EMPIR programme co-financed by the Participating States and from the European Union's Horizon 2020 research and innovation programme.</p>
      <p>Link to project GitLab repository: <br/><a href='https://gitlab.com/aalto-smartcom/ceracrane'>https://gitlab.com/aalto-smartcom/ceracrane</a></p>
    </div>
  )

  const classes = useStyles()
  return (
    <Dialog open={open} onClose={onClose} fullWidth={true} maxWidth='lg'>
      <DialogTitle className={classes.title}>
        <Typography variant='h3'>Project information</Typography>
      </DialogTitle>
      <DialogContent className={classes.content}>
        <InfoText />
        <img src='smartcom-logo.png' width='80%' alt='smartcom logo' />
      </DialogContent>
    </Dialog>
  )
}

const useStyles = makeStyles((theme) => ({
  title: {
    margin: '20px'
  },
  content: {
    display: 'flex',
    flexDirection: 'row',
    margin: '20px'
  },
  infoText: {
    fontSize: '24px',
    fontWeight: '600',
    padding: '20px'
  }
}))

export { InfoDialog }