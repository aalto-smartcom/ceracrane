import React, { useState } from 'react'
import { createDCCBlobURL } from '../utils'
//import { useSelector } from 'react-redux'
//import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import { Button } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  modal: {
    flex: 0,
    display: 'flex',
    paddingTop: '20px',
    justifyContent: 'center',
    alignContent : 'center',
    paddingBottom: '20px',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    overflowY: 'auto',
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    width: '90%',
    maxWidth: '1000px'
  },
  iframe: {
    width: '100%'
  }
}))

const DCCxslModal = ({ href, disabled = false }) => {
  const [dccUrl, setDccUrl] = useState('')
  const [open, setOpen] = useState(false)
  const classes = useStyles()
  //const client = useSelector(state => state.client)
  const xsladdr = 'demo_xslt.xsl'

  const handleOpen = async () => {
    try {
      const xsl = await (await fetch(xsladdr)).text()
      const xml = await (await fetch(href)).text()
      //const xsl = (await axios.get(xsladdr)).data
      //const xml = await client.getFileContents(href, { format: 'text' })
      console.log("Loaded xml and xslt")
      const dcc = await createDCCBlobURL(xml, xsl)
      console.log("Created blob url")
      setDccUrl(dcc)
    } catch (error) {
      alert('An error occurred loading DCC: ', error.message)
    }
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  return (
    <div>
      <Button
        onClick={() => handleOpen()}
        variant="outline-info"
        disabled={disabled}
      >
        Show DCC
      </Button>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <iframe className={classes.iframe} src={dccUrl} />
          </div>
        </Fade>
      </Modal>
    </div>
  )
}

export default DCCxslModal
