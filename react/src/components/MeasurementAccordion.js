import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { 
  Accordion, AccordionDetails, AccordionSummary,
  Typography,
  Button,
} from '@material-ui/core'
import { 
  CheckCircle, 
  Warning, 
  ExpandMore
} from '@material-ui/icons'
import { printDate, printTime, printMeasurement } from '../utils'
import './MeasurementAccordion.css'
import { DCCDialog } from './DCCDialog'

export const MeasurementAccordion = ({ result, db_address }) => {
  const [ dialogOpen, setDialogOpen ] = useState(false)

  const xmlUrl = `${db_address}/api/xml?tx=${result.iota_tx_hash}`
  
  const weightCheckText = (!result.weightTampered) ?
      <Typography style={{color: 'forestgreen'}}>Measurement chain is consistent</Typography> :
      <Typography style={{color: '#ee0000'}}>Cumulative error on measurement chain!</Typography>
  
  const iotaCheckText = (result.iota_confirmed) ? 
      <Typography style={{color: 'forestgreen'}}>IOTA confirmed!</Typography> :
      <Typography style={{color: '#ee0000'}}>IOTA not confirmed!</Typography>
  
  const integrityIcon = (!result.weightTampered && result.iota_confirmed) ?
      <CheckCircle style={{color: 'forestgreen', flexBasis: '35%'}} /> :
      <Warning style={{color: '#ee0000', flexBasis: '35%'}} />

  const classes = useStyles()
  return (
    <Accordion className='single-result-accordion'>
      <AccordionSummary expandIcon={<ExpandMore />}>
          <Typography className={classes.heading}>{result.crane_name}</Typography>
          <Typography className={classes.secondaryHeading}>{`${printDate(result.date_time)}, ${printTime(result.date_time)}`}</Typography>
          {integrityIcon}
      </AccordionSummary>
      <AccordionDetails>
          <div className='accordion-details'>
              <div className='measurement-content'>
                  <div className='crane-name'>
                      <Typography variant='h5' gutterBottom={true}>Container ID: {result.container_id}</Typography>
                  </div>
                  <div className='crane-position'>
                      <Typography variant='h6'>Crane position</Typography>
                      <Typography>Bridge position: {printMeasurement(result.bridge_pos, 'm')}</Typography>
                      <Typography>Hoist position: {printMeasurement(result.hoist_pos, 'm')}</Typography>
                      <Typography>Trolley position: {printMeasurement(result.trolley_pos, 'm')}</Typography>
                  </div>
                  <div className='container-weight'>
                      <Typography variant='h6' gutterBottom={true}>Container weight</Typography>
                      <Typography>Weight: {printMeasurement(result.load_gross_t, 't')}</Typography>
                      <Typography gutterBottom={true}>Tared weight: {printMeasurement(result.load_tared_t, 't')}</Typography>
                  </div>
              </div>
              <div className='integrity-and-buttons'>
                  <div className='integrity-checks'>
                      <Typography variant='h6' gutterBottom={true}>Integrity checks</Typography>
                      {weightCheckText}
                      {iotaCheckText}
                  </div>
                  <div className='result-measurement-buttons'>
                      <Button onClick={() => setDialogOpen(true)}>Device details</Button>
                      <Button href={xmlUrl} disabled={true}>Show measurement XML</Button>
                      <DCCDialog open={dialogOpen} onClose={() => setDialogOpen(false)} />
                  </div>
              </div>
          </div>
      </AccordionDetails>
    </Accordion>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
      margin: 'auto',
      display: 'grid',
      padding: '5px'
  },
  heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '40%',
      flexShrink: 0,
  },
  secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
  },
  iconButton: {
      padding: 10,
  }
}))