import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { 
  Timeline, 
  TimelineDot,
  TimelineItem, 
  TimelineContent,
  TimelineSeparator, 
  TimelineConnector
} from '@material-ui/lab'
import { Button, Typography, Tooltip } from '@material-ui/core'
import { Check, Clear } from '@material-ui/icons'
import { green, red, yellow } from '@material-ui/core/colors'
import axios from 'axios'
const convert = require('xml-js')

const getValidationReport = async (xml) => {
  const validateUrl = 'http://localhost:3000/v1/validate'
  const headers = { 'Content-Type': 'application/xml' }

  try {
    return (await axios.post(validateUrl, xml, { headers })).data

  } catch (error) {
    return { Errors: [
        { 'value': `Failed to fetch validation report: ${error.message}` }
    ]}
  }
}

const emptyReport = {
  Indication: '',
  SubIndication: '',
  Errors: [],
  Warnings: [],
  Infos: []
}

const Certificate = ({ xml, report = emptyReport, last = false }) => {

  const iconColor = (
      !report.Indication ?                yellow[800]
      : report.Indication === 'PASSED' ?  green[800]
      :                                   red[800]
  )

  const validityIcon = (
    report.Indication && report.Indication === 'PASSED' ? <Check /> 
    :                                                     <Clear />
  )

  const json = JSON.parse(convert.xml2json(xml, { compact: true }))
  let dccName, dccId
  try {
    dccName = json['dcc:digitalCalibrationCertificate']['dcc:administrativeData']['dcc:items']['dcc:item']['dcc:name']['dcc:content']['_text']
  } catch {
    dccName = ''
  }
  try {
    dccId = json['dcc:digitalCalibrationCertificate']['dcc:cryptographicIdentifier']['_text']
  } catch {
    dccId = ''
  }

  const tooltipHTML = (
    <table>
      <thead>
        <tr>
          <th colSpan='2'><h2><b>Signature validity: {report.Indication || 'Unvalidated'}</b></h2></th>
        </tr>
      </thead>
      <tbody>
        {
          report.SubIndication && (
            <tr>
              <td valign='top'><h3>Cause</h3></td>
              <td valign='top'>{report.SubIndication}</td>
            </tr>
          )
        }
        <tr>
          <td valign='top'><h3>Errors ({report.Errors.length})</h3></td>
          <td valign='top'>{(report.Errors).map((error, i) => <span key={i}>{error.value}<br /></span>)}</td>
        </tr>
        <tr>
          <td valign='top'><h3>Warnings ({report.Warnings.length})</h3></td>
          <td>{(report.Warnings).map((warning, i) => <span key={i}>{warning.value}<br /></span>)}</td>
        </tr>
        <tr>
          <td valign='top'><h3>Infos ({report.Infos.length})</h3></td>
          <td>{(report.Infos).map((info, i) => <span key={i}>{info.value}<br /></span>)}</td>
        </tr>
      </tbody>
    </table>
  )

  return (
    <TimelineItem>
      <TimelineSeparator>
        <Tooltip title={
          <React.Fragment>
            {tooltipHTML}
          </React.Fragment>
        }>
          <TimelineDot style={{backgroundColor: iconColor}}>
            {validityIcon}
          </TimelineDot>
        </Tooltip>
        {!last && <TimelineConnector />}
      </TimelineSeparator>
      <Tooltip title={dccId}>
        <TimelineContent>
          <Typography>{dccName}</Typography>
          <Typography><a href={dccId}>{dccId}</a></Typography>
        </TimelineContent>
      </Tooltip>
    </TimelineItem>
  )
}

const CertificateChain = ({ chain = [] }) => {
  const [ reports, setReports ] = useState([])
  const [ buttonDisabled, setButtonDisabled ] = useState(false)
  
  const certificates = chain.map((cert, i) => (
    <Certificate key={i} xml={cert} report={reports[i] || emptyReport} last={i + 1 >= chain.length} />
  ))

  const getReports = async () => {
    setButtonDisabled(true)
    setReports(await Promise.all(chain.map(getValidationReport)))
    setButtonDisabled(false)
  }

  const classes = useStyles()
  return (
    <div className={classes.root}>
      <div className={classes.heading}>
        <h2 style={{paddingRight: '200px'}}>DCC chain</h2>
        <Button disabled={buttonDisabled} style={{flexBasis: '15%'}} onClick={getReports}>Validate</Button>
      </div>
      <Timeline className='certificate'>
        {certificates}
      </Timeline>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    width: '90%'
  },
  heading: {
    display: 'flex',
    alignItems: 'center',
  }
}))

export default CertificateChain