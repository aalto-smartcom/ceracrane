import { React } from 'react'
import { PeculiarCertificateViewer } from '@peculiar/certificates-viewer-react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Table, TableBody, TableCell, TableContainer, TableRow,
  Accordion, AccordionDetails, AccordionSummary,
  Dialog, DialogContent,
  Button,
  Paper
} from '@material-ui/core'
import { ExpandMore } from '@material-ui/icons'
import { /*pos_dcc, pos_ref_dcc, weight_dcc, weight_ref_dcc,*/ AIIC_K16052_str, AIIC_M91300_str, M_17L288_str, M_20L330_str} from '../static/dccs.js'
//import CertificateChain from './CertificateChain'
import DCCxslModal from './DCCxslModal.js'
const convert = require('xml-js')

const DCCDialog = ({ open, onClose }) => {
  /*const positionDCC = pos_dcc
  const positionREF = pos_ref_dcc
  const weightDCC = weight_dcc
  const weightREF = weight_ref_dcc*/
  
  const AIIC_K16052 = 'AIIC-K16052.xml'
  const AIIC_M91300 = 'AIIC-M91300.xml'
  const M_17L288 = 'M-17L288.xml'
  const M_20L330 = 'M-20L330.xml'
  
  return (
    <Dialog open={open} onClose={onClose} fullWidth={true} maxWidth='lg' >
      <DialogContent>
        <h1>Measurement devices</h1>
        <DCCAccordion xmlhref={M_20L330} dccRefhref={M_17L288} xml={M_20L330_str} dccRef={M_17L288_str}/>
        <DCCAccordion xmlhref={AIIC_K16052} dccRefhref={AIIC_M91300} xml={AIIC_K16052_str} dccRef={AIIC_M91300_str}/>
        {/*
        <DCCAccordion xml={positionDCC} dccRef={positionREF} />
        <DCCAccordion xml={weightDCC} dccRef={weightREF} />
        */}
      </DialogContent>
  </Dialog>
  )
}


// Unnecessary as XLST has signature validation
/*const SignatureAccordion = ({ certificate }) => {
  const classes = useStyles()
  return (
    <Accordion className={classes.inner}>
      <AccordionSummary expandIcon={<ExpandMore />}>
        Signature Info
      </AccordionSummary>
      <AccordionDetails>
        <PeculiarCertificateViewer certificate={certificate} />
      </AccordionDetails>
    </Accordion>
  )
}*/

const DCCAccordion = ({ xmlhref = '', dccRefhref = '', xml = '', dccRef = '', depth = 0 }) => {

  /*
  const xml = xmlhref ? await (await fetch(xmlhref)).text() : ''
  const dccRef = dccRefhref ? await (await fetch(dccRefhref)).text() : ''

  console.log(xml)
  console.log(dccRef)
  */

  //const chain = [ xml, dccRef ]
  const dcc = JSON.parse(convert.xml2json(xml, { compact: true }))
  //const dccUrl = 'AIIC-K16052.xml'//'/M-20L330.pdf'
  let deviceName, beginPerformance, endPerformance, deviceModel, deviceManufacturer, certificate, dccId
  
  try {
    deviceName = dcc['dcc:digitalCalibrationCertificate']['dcc:administrativeData']['dcc:items']['dcc:item']['dcc:name']['dcc:content']['_text']
  } catch (error) {}
  
  try {
    beginPerformance = dcc['dcc:digitalCalibrationCertificate']['dcc:administrativeData']['dcc:coreData']['dcc:beginPerformanceDate']['_text']
  } catch (error) {}

  try {
    endPerformance = dcc['dcc:digitalCalibrationCertificate']['dcc:administrativeData']['dcc:coreData']['dcc:endPerformanceDate']['_text']
  } catch (error) {}
  
  try {
    deviceManufacturer = dcc['dcc:digitalCalibrationCertificate']['dcc:administrativeData']['dcc:items']['dcc:item']['dcc:manufacturer']['dcc:name']['dcc:content']['_text']
  } catch (error) {}

  try {
    deviceModel = dcc['dcc:digitalCalibrationCertificate']['dcc:administrativeData']['dcc:items']['dcc:item']['dcc:model']['_text']
  } catch (error) {}

  try {
    dccId = dcc['dcc:digitalCalibrationCertificate']['dcc:cryptographicIdentifier']['_text']
  } catch (error) {}
  
  // Might be an array or single entry depending on certificate chain!
  try {
    certificate = dcc['dcc:digitalCalibrationCertificate']['ds:Signature']['ds:KeyInfo']['ds:X509Data']['ds:X509Certificate'][0]['_text']
  } catch (error) {}
  

  const classes = useStyles()
  return (
    <Accordion className={depth ? classes.inner : classes.root}>
      <AccordionSummary expandIcon={<ExpandMore />}>  
        {(depth ? 'Reference: ' : '').concat(deviceName)}
      </AccordionSummary>
      <AccordionDetails className={classes.dccDetails}>
        <TableContainer component={Paper} className={classes.table}>
          <Table aria-label="simple table">
            <TableBody>
              <TableRow>
                <TableCell>Measurement device</TableCell>
                <TableCell>{deviceName}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Manufacturer</TableCell>
                <TableCell>{deviceManufacturer}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Model</TableCell>
                <TableCell>{deviceModel}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>DCC ID</TableCell>
                <TableCell><a href={dccId}>{dccId}</a></TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Valid through</TableCell>
                <TableCell>{beginPerformance} - {endPerformance}</TableCell>
              </TableRow>
                <TableRow>
                  <TableCell><b>Human readable DCC</b></TableCell>
                  <TableCell><DCCxslModal href={xmlhref} /></TableCell>
                </TableRow> 
            </TableBody>
          </Table>
        </TableContainer>
        {/*!depth && <CertificateChain chain={chain} />*/}
        {/*<SignatureAccordion certificate={certificate} /> Signature is visible in XSLT view*/}
        {dccRef && <DCCAccordion depth={depth + 1} xml={dccRef} xmlhref={dccRefhref} />}
      </AccordionDetails>
    </Accordion>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  inner: {
    width: '95%',
    margin: '30px',
    marginTop: '0px',
    marginBottom: '20px'
  },
  table: {
    width: '95%',
    margin: '30px'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  dccDetails: {
    flexDirection: 'column'
  }
}))

export { DCCDialog }