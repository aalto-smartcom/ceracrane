import './OperatorView.css'
import axios from 'axios'
import { React, useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
    Table, TableBody, TableCell, TableContainer, TableRow, TableHead,
    IconButton, Button,
    Card, CardContent,
    Typography,
    InputBase,
    Paper,
    Tooltip,
    TextField,
    Link,
    Menu,
    MenuItem,
} from '@material-ui/core'
import { ArrowForward, Add, Remove } from '@material-ui/icons'
import { printDate, printTime, printMeasurement } from './utils'
import { DCCDialog } from './components/DCCDialog'

const CRANE_UPDATE_INTERVAL = 1000
const JOB_UPDATE_INTERVAL = 333

const crane = {
    name: "Ilmatar",
    url: "http://localhost:9000"
}

const opcuaUrl = 'http://localhost:9005'

const IncreaseButton = ({ action }) => (
    <Tooltip title="Increase by one">
        <IconButton size="small" onClick={action}>
            <Add />
        </IconButton>
    </Tooltip>
)

const DecreaseButton = ({ action }) => (
    <Tooltip title="Decrease by one">
        <IconButton size="small" onClick={action} >
            <Remove />
        </IconButton>
    </Tooltip>
)

const NewValueMenu = ({ anchorEl, setAnchorEl, status, field }) => (
    <Menu 
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        keepMounted
        onClose={() => setAnchorEl(null)}
    >
        <MenuItem>
            <NewValueTextField 
                onClose={() => setAnchorEl(null)} 
                status={status}
                field={field}
            />
        </MenuItem>
    </Menu>
)

const NewValueTextField = ({ onClose, status, field }) => {
    const [ value, setValue ] = useState('')
    const [ error, setError ] = useState(false)

    const onSubmit = (event) => {
        event.preventDefault()
        if (!isNaN(value)) {
            setError(false)
            setValue('')
            postValues(status, field, Number(value))
            onClose()
        } else {
            setError(true)
        }
    }

    return (
        <form onSubmit={onSubmit}>
            <TextField 
                value={value} 
                error={error}
                helperText={error ? 'Invalid value' : ''}
                onChange={(event) => setValue(event.target.value)}  
                size="small" 
                label="Set a new value"
                placeholder="E.g. 1.10"
            />
        </form>
    )
}

const postValues = (values, field, newValue) => {
    const newValues = { ...values }
    newValues[field] = newValue
    axios.post(`${opcuaUrl}/setValues`, newValues)
}

const randomize = () => {
    axios.post(`${opcuaUrl}/setRandomState`, { "random_state": true })
}

const OperatorView = () => {
    return (
        <div className='operator-view-cards'>
            <CraneStatus />
            <CreateMeasurement />
        </div>
    )
}

const MeasurementField = ({ field, unit, status }) => {
    const [ menuAnchorEl, setMenuAnchorEl ] = useState(null)

    return (<>
        <DecreaseButton 
            action={() => postValues(status, field, status[field] - 1)} 
        />

        <Tooltip title="Click to set a new value">
            <Link 
                component="button" 
                underline="always" 
                onClick={(event) => { setMenuAnchorEl(event.target) }}>
                {printMeasurement(status[field], unit)}
            </Link>
        </Tooltip>

        <NewValueMenu 
            anchorEl={menuAnchorEl}
            setAnchorEl={setMenuAnchorEl}
            status={status}
            field={field}
        />

        <IncreaseButton 
            action={() => postValues(status, field, status[field] + 1)} 
        />
    </>)
}

const CraneStatus = () => {
    const [ connectionStatus, setConnectionStatus ] = useState("Connecting...")
    const emptyResponse = { datetime: 0, weight: 0, tared: 0, bridge_pos: 0, hoist_pos: 0, trolley_pos: 0 }
    const [ craneStatus, setCraneStatus ] = useState(emptyResponse)
    const [ dialogOpen, setDialogOpen ] = useState(false)
    let intervalID = 0

    /** Update crane status every ${CRANE_UPDATE_INTERVAL} milliseconds */
    /* eslint-disable */
    useEffect(() => {
        intervalID = setInterval(() => {
            axios.get(`${crane.url}/cranestatus`)
                .then(response => {
                    setConnectionStatus("ONLINE")
                    setCraneStatus(response.data)
                })
                .catch((error) => {
                    setConnectionStatus(`OFFLINE`)
                    setCraneStatus(emptyResponse)
                })
        }, CRANE_UPDATE_INTERVAL)

        return function cleanup() {
            clearInterval(intervalID)
        }
    }, [])
    /* eslint-enable */

    return (
        <Card>
            <CardContent className='card-content'>
                <TableContainer component={Paper}>
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell><Typography variant='h5'>Crane status</Typography></TableCell>
                                <TableCell>
                                    <Tooltip title="Randomize measurement values">
                                        <Button onClick={randomize}>
                                            Randomize
                                        </Button>
                                    </Tooltip>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell>Crane name</TableCell>
                                <TableCell>{crane.name}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Connection status</TableCell>
                                <TableCell>{connectionStatus}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Current date</TableCell>
                                <TableCell>{printDate(craneStatus.datetime)}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Current time</TableCell>
                                <TableCell>{printTime(craneStatus.datetime)}</TableCell>
                            </TableRow>
                        </TableBody>
                        <TableHead>
                            <TableRow>
                                <TableCell><Typography variant='h6'>Position</Typography></TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell>Bridge position</TableCell>
                                <TableCell><MeasurementField field='bridge_pos' unit='m' status={craneStatus} /></TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Trolley position</TableCell>
                                <TableCell><MeasurementField field='trolley_pos' unit='m' status={craneStatus} /></TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Hoist position</TableCell>
                                <TableCell><MeasurementField field='hoist_pos' unit='m' status={craneStatus} /></TableCell>
                            </TableRow>
                        </TableBody>
                        <TableHead>
                            <TableRow>
                                <TableCell><Typography variant='h6'>Weight</Typography></TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell>Weight gross</TableCell>
                                <TableCell><MeasurementField field='weight' unit='t' status={craneStatus} /></TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Weight tared</TableCell>
                                <TableCell><MeasurementField field='tared' unit='t' status={craneStatus} /></TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>
                <Button onClick={() => setDialogOpen(true)}>Device details</Button>
                <DCCDialog open={dialogOpen} onClose={() => setDialogOpen(false)} />
            </CardContent>
        </Card>
    )
}

const CreateMeasurement = () => {
    const [ currentJob, setCurrentJob ] = useState({})
    let intervalID = 0

    const inProgress = () => currentJob.status !== "Job is complete" && currentJob.jobid

    const createNewJob = (id) => {
        if (id) {
            axios.get(`${crane.url}/createjob?id=${id}`)
                .then(response => setCurrentJob(response.data))
                .catch(error => console.log(error.message))
        }
    }

    /** If a job is running, update its status every ${JOB_UPDATE_INTERVAL} milliseconds */
    /* eslint-disable */
    useEffect(() => {
        intervalID = setInterval(() => {
            if (inProgress()) {
                axios.get(`${crane.url}/status?id=${currentJob.jobid}`)
                    .then(response => setCurrentJob(response.data))
                    .catch(error => console.log(error.message))
            } else {
                clearInterval(intervalID.current)
            }
        }, JOB_UPDATE_INTERVAL)

        return function cleanup() {
            clearInterval(intervalID)
        }
    }, [ currentJob ])
    /* eslint-enable */
    
    return (
        <div className='create-measurement-cards'>
            <Card className='card-root'>
                <CardContent>
                    <CreateMeasurementInput 
                        currentJob={currentJob}
                        newJob={createNewJob}
                        inProgress={inProgress} />
                </CardContent>
            </Card>
            <Card className='card-root'>
                <CardContent className='card-content'>
                    <CurrentMeasurement currentJob={currentJob} inProgress={inProgress} />
                </CardContent>
            </Card>
        </div>
        
    )
}

const CreateMeasurementInput = (props) => {
    const [id, setId] = useState("")
    
    const createJob = (event) => {
        event.preventDefault()
        if (id && !props.inProgress()) {
            console.log('Creating job with id: ', id)
            props.newJob(id)
        }
    }
    
    const inputChangeHandler = (event) => {
        setId(
            event.target.value.replace(/ /g, '')
        )
    }
    
    const disabled = true

    const classes = useStyles()
    return (
        <div className="create-measurement">
            <div className='create-measurement-header'>
                <Typography component='h4' variant='h4'>Create a new measurement</Typography>
                <p className="create-measurement-error">IOTA network changes prevent creation of new measurements!</p>
            </div>
            <div className='create-measurement-form'>
                <Paper component='form' className={classes.form} onSubmit={createJob}>
                <InputBase className={classes.input} 
                    placeholder='Container ID' 
                    onChange={inputChangeHandler}
                    value={id} />
                <IconButton type='submit' disabled={disabled} className={classes.iconButton}>
                    <ArrowForward />
                </IconButton>
            </Paper>
            </div>
        </div>
    )
}

const CurrentMeasurement = ({ currentJob, inProgress }) => {
    const description = !currentJob.jobid ? 
        'No measurements running.': inProgress() ?
        'Creating measurement...' :
        'Created measurement!'

    return (
        <div>
            <Typography component='h5' variant='h5' gutterBottom={true}>{description}</Typography>
            <Waterfall currentJob={currentJob} inProgress={inProgress} />
        </div>
    )
}

const Waterfall = ({ currentJob }) => {
    const ret = []
    const statuses = [
        "Job created",
        "Data fetched from the crane",
        "XML created and signed",
        "XML signature was valid",
        "Waiting for data to be written to IOTA and database",
        "Job is complete"
    ]

    /** Bold current stage */
    statuses.forEach((i, idx) => {
        if (i === currentJob.status) {
            ret.push(<Typography variant='body1' key={idx}><b>{i}</b></Typography>)
        } else {
            ret.push(<Typography variant='body1' color='textSecondary' key={idx}>{i}</Typography>)
        }
    })

    return <div className="waterfall">{ret}</div>
}

const useStyles = makeStyles((theme) => ({
    form: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        backgroundColor: '#323232'
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10
    },
}))

export default OperatorView