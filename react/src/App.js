import './App.css'
import React, { useState } from 'react'
import SearchView from './SearchView'
import OperatorView from './OperatorView'
import { Button } from '@material-ui/core'
import { InfoDialog } from './components/InfoDialog'

const pages = {
  0: <SearchView />,
  1: <OperatorView />
}

const App = () => {
  const [ page, setPage ] = useState(0)
  const [ infoDialogOpen, setInfoDialogOpen ] = useState(false)

  const pageSwitchButtonText = () => (
    page === 1 ? 'Switch to search view' : 'Switch to operator view'
  )

  return (
    <div className="App">
      <header className="App-header">
        <div className='ceracrane-logo'>
          <a href='/'>
            <img src='/ceracrane-logo.png' width='40%'  alt='Ceracrane logo' />
          </a>
        </div>
        <img className='empir-logo' src='/EMPIR-logo.jpg' width='18%' alt='EMPIR logo'/>
        <div className='page-content'>
          {pages[page]} 
        </div>
        <div className='page-switch-button'>
          <Button onClick={() => setPage((page + 1) % 2)}>
            {pageSwitchButtonText()}
          </Button>
          <Button onClick={() => setInfoDialogOpen(true)}>Info</Button>
          <InfoDialog open={infoDialogOpen} onClose={() => setInfoDialogOpen(false)} />
        </div>
      </header>
    </div>
  )
}

export default App
