import './SearchView.css'
import axios from 'axios'
import { React, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { 
    Paper,
    InputBase,
    IconButton
} from '@material-ui/core'
import { Search } from '@material-ui/icons'
import { MeasurementAccordion } from './components/MeasurementAccordion'


const DB_API_ADDRESS = "http://localhost:9010"
const WEIGHT_DELTA = 0.2 //In tonne

const SearchView = () => {
    const [results, setResults] = useState([])
    const [ searchMade, setSearchMade ] = useState(false)
    
    const getResults = (query) => {
        if (query) {
            axios.get(`${DB_API_ADDRESS}/api/read?containerid=${query}`)
                .then(response => {
                    setResults(response.data)
                    setSearchMade(true)
                })
                .catch(error => console.log(error.message))
        }
    }
    
    return (
        <div className="Search-view">
            <h3>Search containers</h3>
            <SearchBar getResults={getResults}/>
            <Results data={results} searchMade={searchMade} />
        </div>
    )
}

const SearchBar = ({getResults}) => {
    const [id, setId] = useState("")
    
    const makeSearch = (event) => {
        event.preventDefault()
        getResults(id)
    }
    
    const inputChangeHandler = (event) => {
        setId(
            event.target.value.replace(/ /g, '')
        )
    }

    const classes = useStyles()
    return (
        <div className="Search-bar">
            <Paper component='form' className={classes.form} onSubmit={makeSearch}>
                <InputBase className={classes.input} 
                    placeholder='Container ID' 
                    onChange={inputChangeHandler}
                    value={id} />
                <IconButton type='submit' className={classes.iconButton}>
                    <Search />
                </IconButton>
            </Paper>
        </div>
    )
}

const Results = ({ data, searchMade }) => {
    const sortedData = data.sort((a, b) => new Date(a.date_time) < new Date(b.date_time))
    
    const checkWeights = (list) => {
        if (list.length === 0) return
        list[0].weightTampered = false
        if (list.length === 1) return

        const minWeight = list[0].load_gross_t - WEIGHT_DELTA
        const maxWeight = list[0].load_gross_t + WEIGHT_DELTA

        for(let i = 1; i < sortedData.length; i++) {
            if (sortedData[i].load_gross_t < minWeight || sortedData[i].load_gross_t > maxWeight) {
                sortedData[i].weightTampered = true
            } else {
                sortedData[i].weightTampered = false
            }
        }
    }
    
    const parseResult = (result, key) => <MeasurementAccordion result={result} db_address={DB_API_ADDRESS} key={key} />

    checkWeights(sortedData)
    const ret = sortedData.map(parseResult)
    const emptyQueryText = 'No results were found for your search query.'

    return (
        <div className="Result-list">
            {!searchMade || ret.length ? ret : emptyQueryText}
        </div>
    )
}

const useStyles = makeStyles((theme) => ({
    form: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 600,
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    }
}))

export default SearchView