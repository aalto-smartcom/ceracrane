/**
 * Generates an documentFragment from provided XML and XSLT template.
 * @param {xml} a 
 * @param {xsl} b 
 * @returns 
 */
 export const createDCCBlobURL = async (a, b) => {
  const parser = new DOMParser()
  const xml = parser.parseFromString(a, 'application/xml')
  console.log(xml.documentElement.tagName)
  const xsl = parser.parseFromString(b, 'application/xml')
  if (!(xml.documentElement.tagName === 'dcc:digitalCalibrationCertificate')) 
    throw Error('Invalid document format')
  const xsltProcessor = new XSLTProcessor()
  xsltProcessor.importStylesheet(xsl)
  xsltProcessor.setParameter(null, 'b64', btoa(a)) // Pass original XML to XSLT processor
  const newDocument = xsltProcessor.transformToDocument(xml, document)
  // let rootElement = document.createElement('div')
  // rootElement.appendChild(newDocument)
  let htmlDocStr = new XMLSerializer().serializeToString(newDocument)

  const getBlobURL = (code, type) => {
    const blob = new Blob([code], { type })
    return URL.createObjectURL(blob)
  }

  const url = getBlobURL(htmlDocStr, 'text/html')
  return url
}

export const printDate = (datestr) => {
  const date = new Date(datestr)
  return `${date.toLocaleDateString('en-GB')}`
}

export const printTime = (datestr) => {
  const date = new Date(datestr)
  return `${date.toLocaleTimeString('en-GB')} (+0 UTC)`
}

export const printMeasurement = (str, unit) => `${parseFloat(str).toFixed(3)} ${unit}`

