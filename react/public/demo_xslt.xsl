<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dcc="https://ptb.de/dcc" xmlns:si="https://ptb.de/si" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" version="3.0">
  <xsl:param name="b64"/>
  <xsl:template match="dcc:digitalCalibrationCertificate">
    <html>
      <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://xsltstorageacc.blob.core.windows.net/xsltpub/styles.css" />
        <script type="module" src="https://unpkg.com/@peculiar/certificates-viewer@latest/dist/peculiar/peculiar.esm.js"></script>
        <script nomodule="true" src="https://unpkg.com/@peculiar/certificates-viewer@latest/dist/peculiar/peculiar.js"></script>
        <link rel="stylesheet" href="https://unpkg.com/@peculiar/certificates-viewer@1.13.4/dist/peculiar/peculiar.css" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons+Two+Tone"/>
        <title>Minimal example</title>
      </head>
      <body>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous" />
        <div class="container" style="margin-top: 10px;">
          <div class="card">
            <xsl:apply-templates select="dcc:administrativeData" />
            <xsl:apply-templates select="dcc:measurementResults" />
            <xsl:apply-templates select="ds:Signature" />
            <xsl:apply-templates select="dcc:comment" />
          </div>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:variable name="selectedLang">en</xsl:variable>

  <xsl:template name="lang-content">
    <xsl:param name="node" />
    
    <xsl:choose>
      <xsl:when test="$node/dcc:content[@lang='en']">
        <xsl:copy-of select="($node/dcc:content[@lang='en'])[1]"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="$node/dcc:content[1]"/>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <!-- 
    General template for all contact elements
    Needs the element node and index as parameters!
   -->
  <xsl:template name="contact">
    <xsl:param name="node" />
    <xsl:param name="index" />
    <!-- Unique ID for the accordion -->
    <xsl:variable name="id" select="generate-id(.)" />
    <!-- Element IDs -->
    <xsl:variable name="accordion-id" select="concat('accordion-', $id, $index)" />
    <xsl:variable name="heading-id" select="concat('heading-', $id, $index)" />
    <xsl:variable name="collapse-id" select="concat('collapse-', $id, $index)" />
    <div class="accordion" id="{$accordion-id}">
      <div class="accordion-item">
        <h2 class="accordion-header" id="{$heading-id}">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#{$collapse-id}" aria-expanded="false" aria-controls="{$collapse-id}">
            <xsl:choose>
              <xsl:when test="$node/../dcc:mainSigner">
                <b><xsl:value-of select="concat($node/dcc:name/dcc:content, ' (Main signer)')" /></b>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$node/dcc:name/dcc:content" />
              </xsl:otherwise>
            </xsl:choose>
          </button>
        </h2>
        <div id="{$collapse-id}" class="accordion-collapse collapse" aria-labelledby="{$heading-id}" data-bs-parent="#{$accordion-id}">
          <table class="table table-borderless" style="table-layout: fixed;">
            <tr>
              <th>Email:</th>
              <td><xsl:value-of select="$node/dcc:eMail" /></td>
            </tr>
            <tr>
              <th>Phone:</th>
              <td><xsl:value-of select="$node/dcc:phone" /></td>
            </tr>
            <tr>
              <th>Fax:</th>
              <td><xsl:value-of select="$node/dcc:fax" /></td>
            </tr>
            <tr>
              <th>Street:</th>
              <td><xsl:value-of select="$node/dcc:location/dcc:street"/></td>
            </tr>
            <tr>
              <th>Street number:</th>
              <td><xsl:value-of select="$node/dcc:location/dcc:streetNo"/></td>
            </tr>
            <tr>
              <th>City:</th>
              <td><xsl:value-of select="$node/dcc:location/dcc:city" /></td>
            </tr>
            <tr>
              <th>Postal code:</th>
              <td><xsl:value-of select="$node/dcc:location/dcc:postCode" /></td>
            </tr>
            <tr>
              <th>PO Box:</th>
              <td><xsl:value-of select="$node/dcc:location/dcc:postOfficeBox" /></td>
            </tr>
            <tr>
              <th>Country code:</th>
              <td><xsl:value-of select="$node/dcc:location/dcc:countryCode" /></td>
            </tr>
            <tr>
              <th>State:</th>
              <td><xsl:value-of select="$node/dcc:location/dcc:state" /></td>
            </tr>
            <tr>
              <th>Additional location info:</th>
              <td>
                <xsl:call-template name="lang-content">
                    <xsl:with-param name="node" select="$node/dcc:location/dcc:further" />
                </xsl:call-template>
               <!-- <xsl:value-of select="$node/dcc:location/dcc:further" /> -->
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="dcc:administrativeData">
    <div class="card-header"><h2>Administrative Data</h2></div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item"><xsl:apply-templates select="dcc:dccSoftware" /></li>
      <li class="list-group-item"><xsl:apply-templates select="dcc:coreData" /></li>
      <li class="list-group-item"><xsl:apply-templates select="dcc:items" /></li>
      <li class="list-group-item">
        <xsl:apply-templates select="dcc:calibrationLaboratory" />
        <xsl:apply-templates select="dcc:respPersons" />
        <xsl:apply-templates select="dcc:customer" />
      </li>
      <li class="list-group-item"><xsl:apply-templates select="dcc:statements" /></li>
    </ul>
  </xsl:template>

  <xsl:template match="dcc:dccSoftware">
    <div class="administrative-data-subheaders">
      <h3>Used software</h3>
    </div>
    <table class="table table-borderless">
      <tr>
        <th style="width: 25%">Name</th>
        <th style="width: 25%">Release</th>
        <th style="width: 50%">Description</th>
      </tr>
        <xsl:for-each select="dcc:software">
          <tr>
            <td><xsl:value-of select="dcc:name/dcc:content" /></td>
            <td><xsl:value-of select="dcc:release" /></td>
            <td><xsl:value-of select="dcc:description" /></td>
          </tr>
        </xsl:for-each>
    </table>
  </xsl:template>
 
  <xsl:template match="dcc:coreData">
    <div class="administrative-data-subheaders">
      <h3>Core Data</h3>
    </div>
    <table class="table table-borderless" style="table-layout: fixed;">
      <tr>
        <th style="width: 25%"></th>
      </tr>
      <tr>
        <th>Country code (ISO3166_1):</th>
        <td><xsl:value-of select="dcc:countryCodeISO3166_1" /></td>
      </tr>
      <tr>
        <th>Used Languages (ISO639_1):</th>
        <td>
          <xsl:for-each select="dcc:usedLangCodeISO639_1">
            <xsl:value-of select="."/>                                    
            <xsl:choose>
              <xsl:when test="position() != last()">, </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </td>
      </tr>
      <tr>
        <th>Mandatory Languages (ISO639_1):</th>
        <td>
          <xsl:for-each select="dcc:mandatoryLangCodeISO639_1">
            <xsl:value-of select="."/>                                    
            <xsl:choose>
              <xsl:when test="position() != last()">, </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </td>
      </tr>
      <tr>
        <th>Unique Identifier:</th>
        <td><xsl:value-of select="dcc:uniqueIdentifier" /></td>
      </tr>
      <tr>
        <th>Receipt date:</th>
        <td><xsl:value-of select="dcc:receiptDate" /></td>
      </tr>
      <tr>
        <th>Begin perf date:</th>
        <td><xsl:value-of select="dcc:beginPerformanceDate" /></td>
      </tr>
      <tr>
        <th>End perf date:</th>
        <td><xsl:value-of select="dcc:endPerformanceDate" /></td>
      </tr>
    </table>
    <div class="core-data-identifications-header">
    <h4>Identifications</h4>
    </div>
    <xsl:apply-templates select="dcc:identifications" />
  </xsl:template>

  <xsl:template match="dcc:items">
    <div class="administrative-data-subheaders">
      <h3>Items</h3>
    </div>
    <!-- <table class="table table-borderless" style="table-layout: fixed;">
      <tr>
        <th style="width: 25%"></th>
      </tr>
      <tr>
        <th>Name:</th>
        <td><xsl:value-of select="dcc:name/dcc:content" /></td>
      </tr>
      <tr>
        <th>Equipment Class:</th>
        <td>
          Reference: <xsl:value-of select="dcc:equipmentClass/dcc:reference" />
          <br />
          Class ID: <xsl:value-of select="dcc:equipmentClass/dcc:classID" />
        </td>
      </tr>
      <tr>
        <th>Description:</th>
        <td><xsl:apply-templates select="dcc:description/dcc:content" /></td>
      </tr>
      <tr>
        <th>Owner:</th>
        <td>
          <xsl:for-each select="dcc:owner">
            <xsl:call-template name="contact">
              <xsl:with-param name="node" select="." />
              <xsl:with-param name="index" select="position()" />
            </xsl:call-template>
          </xsl:for-each>
        </td>
      </tr>
    </table> -->
    <table class="table table-bordered align-middle">
      <thead>
        <tr>
          <th style="width: 15%">Item name</th>
          <th style="width: 35%">Manufacturer</th>
          <th style="width: 50%">Identifications</th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="dcc:item">
          <tr>
            <td>
              <xsl:value-of select="dcc:name/dcc:content" />
            </td>
            <td>
              <xsl:call-template name="contact">
                <xsl:with-param name="node" select="dcc:manufacturer" />
                <xsl:with-param name="index" select="position()" />
              </xsl:call-template>
            </td>
            <td><xsl:apply-templates select="dcc:identifications" /></td>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>
  </xsl:template>

  <xsl:template match="dcc:identifications">
    <table class="table table-bordered align-middle">
      <thead>
        <tr>
          <th style="width: 25%">Issuer</th>
          <th style="width: 25%">Value</th>
          <th style="width: 50%">Description</th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="dcc:identification">
          <tr>
            <td>
              <xsl:value-of select="dcc:issuer" />
            </td>
            <td>
              <xsl:value-of select="dcc:value" />
            </td>
            <td>
              <xsl:call-template name="lang-content">
                <xsl:with-param name="node" select="dcc:description" />
              </xsl:call-template>
              <!-- <xsl:value-of select="dcc:description/dcc:content" /> -->
            </td>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>
  </xsl:template>

  <xsl:template match="dcc:calibrationLaboratory">
  <div class="administrative-data-subheaders">
    <h4>Calibration laboratory</h4>
  </div>
    <p>Calibration laboratory code: <xsl:value-of select="dcc:calibrationLaboratoryCode" /></p>
    <xsl:for-each select="dcc:contact">
      <xsl:call-template name="contact">
        <xsl:with-param name="node" select="." />
        <xsl:with-param name="index" select="position()" />
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="dcc:respPersons">
    <div class="responsible-persons-header">
      <h4>Responsible persons</h4>
    </div>
    <table class="table table-bordered align-middle" style="table-layout: fixed">
      <thead>
        <tr>
          <th style="width: 15%">Role</th>
          <th style="width: 35%">Person</th>
          <th style="width: 50%">Description</th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="dcc:respPerson">
          <tr>
            <td><xsl:value-of select="dcc:role" /></td>
            <td>
              <xsl:call-template name="contact">
                <xsl:with-param name="node" select="dcc:person" />
                <xsl:with-param name="index" select="position()" />
              </xsl:call-template>
            </td>
            <td>
              <xsl:call-template name="lang-content">
                <xsl:with-param name="node" select="dcc:description" />
              </xsl:call-template>
              <!-- <xsl:value-of select="dcc:description/dcc:content" /> -->
              </td>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>
  </xsl:template>

  <xsl:template match="dcc:customer">
  <div class="customer-info">
    <h4>Customer Info</h4>
    <xsl:call-template name="contact">
      <xsl:with-param name="node" select="." />
      <xsl:with-param name="index" select="position()" />
    </xsl:call-template>
  </div>
  </xsl:template>


  <xsl:template match="dcc:statements">
    <div class="statements">
      <div class="administrative-data-subheaders">
        <h4>Statements</h4>
      </div>
      <xsl:for-each select="dcc:statement">
        <xsl:apply-templates select=".">
          <xsl:with-param name="index" select="position()" />
        </xsl:apply-templates>
      </xsl:for-each>
    </div>
  </xsl:template>


  <xsl:template match="dcc:statement | dcc:metaData">
    <xsl:param name="index" />
    <!-- Unique ID for the accordion -->
    <xsl:variable name="id" select="generate-id(.)" />
    <!-- Element IDs -->
    <xsl:variable name="accordion-id" select="concat('accordion-', $id, $index)" />
    <xsl:variable name="heading-id" select="concat('heading-', $id, $index)" />
    <xsl:variable name="collapse-id" select="concat('collapse-', $id, $index)" />
    <div class="accordion" id="{$accordion-id}">
      <div class="accordion-item">
        <h2 class="accordion-header" id="{$heading-id}">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#{$collapse-id}" aria-expanded="false" aria-controls="{$collapse-id}">
            <xsl:value-of select="concat('Statement ', $index)" />
          </button>
        </h2>
        <div id="{$collapse-id}" class="accordion-collapse collapse" aria-labelledby="{$heading-id}" data-bs-parent="#{$accordion-id}">
          <table class="table table-borderless">
            <tr>
              <th>Country codes (ISO3166_1):</th>
              <td>
                <xsl:for-each select="dcc:countryCodeISO3166_1">
                  <xsl:value-of select="."/>                                    
                  <xsl:choose>
                    <xsl:when test="position() != last()">, </xsl:when>
                  </xsl:choose>
                </xsl:for-each>
              </td>
            </tr>
            <tr>
              <th>Convention:</th>
              <td><xsl:value-of select="dcc:convention" /></td>
            </tr>
            <tr>
              <th>Traceable:</th>
              <td><xsl:value-of select="dcc:traceable" /></td>
            </tr>
            <tr>
              <th>Norm:</th>
              <td><xsl:value-of select="dcc:norm"/></td>
            </tr>
            <tr>
              <th>Reference:</th>
              <td><xsl:value-of select="dcc:reference"/></td>
            </tr>
            <tr>
              <th>Declaration:</th>
              <td>
                <xsl:call-template name="lang-content">
                    <xsl:with-param name="node" select="dcc:declaration" />
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <th>Valid:</th>
              <td><xsl:value-of select="dcc:valid" /></td>
            </tr>
            <tr>
              <th>Date:</th>
              <td><xsl:value-of select="dcc:date" /></td>
            </tr>
            <tr>
              <th>Period:</th>
              <td><xsl:value-of select="dcc:period" /></td>
            </tr>
            <tr>
              <th>Responsible Authority:</th>
              <td>
                <xsl:for-each select="dcc:responsibleAuthority">
                  <xsl:call-template name="contact">
                    <xsl:with-param name="node" select="." />
                    <xsl:with-param name="index" select="concat('nested-', position())" />
                  </xsl:call-template>
                </xsl:for-each>
              </td>
            </tr>
            <tr>
              <th>Conformity:</th>
              <td><xsl:value-of select="dcc:conformity" /></td>
            </tr>
            <tr>
              <th>Data:</th>
              <td><xsl:value-of select="dcc:data" /></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </xsl:template>


  <!-- MEASUREMENT RESULTS-->

  <xsl:template match="dcc:measurementResults">
    <xsl:for-each select="dcc:measurementResult">
      <div class="card-header">
        <h2>Measurement result <xsl:value-of select="position()"/>
        </h2>
      </div>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">
          <div class="measurement-header">
            <h3>
              <xsl:call-template name="lang-content">
                <xsl:with-param name="node" select="dcc:name" />
              </xsl:call-template>
              <!-- <xsl:value-of select="dcc:name/dcc:content" />  -->
            </h3>
          </div>
          <p>
            <xsl:call-template name="lang-content">
              <xsl:with-param name="node" select="dcc:description" />
            </xsl:call-template>
            <!-- <xsl:value-of select="dcc:description/dcc:content" /> -->
          </p>
          <xsl:apply-templates select="dcc:usedMethods" />
          <xsl:apply-templates select="dcc:usedSoftware" />
          <xsl:apply-templates select="dcc:measuringEquipments" />
          <xsl:apply-templates select="dcc:influenceConditions" />
          <xsl:apply-templates select="dcc:results" />
          <xsl:apply-templates select="dcc:measurementMetaData" />
        </li>
      </ul>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="dcc:results">
    <xsl:for-each select="dcc:result">
      <div class="measurement-name">
      <h4>
        <xsl:call-template name="lang-content">
          <xsl:with-param name="node" select="dcc:name" />
        </xsl:call-template>
        <!-- <xsl:value-of select="dcc:name" /> -->
      </h4>
      </div>
      <xsl:apply-templates select="dcc:data" />
    </xsl:for-each>
  </xsl:template>
  
  
  <xsl:template match="dcc:data">
    <xsl:variable name="tablename" select="generate-id(.)"/>
    <xsl:variable name="chartname" select="concat($tablename, 'chart')"/>
    <xsl:variable name="theader" select="concat($tablename, '-header')"/>
    <xsl:variable name="measPointCount" select="count(dcc:list)"/>
    <canvas id="{$chartname}"></canvas>
    <div class="measurement-result-table">
    <table id="{$tablename}" class="table table-bordered">
      <thead>
        <tr id="{$theader}">
        </tr>
      </thead>
    </table>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.2.1/dist/chart.min.js" />
    <script type="text/javascript">
      <!-- Gather measurement point data and initialize table with headers -->
      var data = {}
      <xsl:for-each select="dcc:list">
        <xsl:variable name="j" select="position()"/>
        <xsl:for-each select="dcc:quantity">
          <xsl:variable name="i" select="position()"/>
          <xsl:variable name="label" select="concat(./@refType, ' (', si:real/si:unit, ')')"/>
            <!-- var label = 
              '<xsl:value-of select="dcc:name/dcc:content"/> (<xsl:value-of select="si:real/si:unit"/>)'
                .replace(/\s+/g, ' ').trim() -->
            if (data['<xsl:value-of select="$label"/>']) {
              data['<xsl:value-of select="$label"/>'][<xsl:value-of select="$j"/>-1] = {
                value: '<xsl:value-of select="si:real/si:value"/>',
                unit: '<xsl:value-of select="si:real/si:unit"/>'
              }
            } else {
              data['<xsl:value-of select="$label"/>'] = []
              let tableHeader = document.createElement('th')
              tableHeader.setAttribute('id', '<xsl:value-of select="$theader"/>-' + Object.keys(data).length)
              tableHeader.innerHTML = '<xsl:value-of select="$label"/>'
              document.getElementById('<xsl:value-of select="$theader"/>').appendChild(tableHeader)
              data['<xsl:value-of select="$label"/>'][<xsl:value-of select="$j"/>-1] = {
                value: '<xsl:value-of select="si:real/si:value"/>',
                unit: '<xsl:value-of select="si:real/si:unit"/>'
              }
            }
        </xsl:for-each>
      </xsl:for-each>

      <!-- Construct table data -->
      var tableBody = document.createElement('tbody')
      for (const j of [ ...Array(<xsl:value-of select="$measPointCount"/>).keys() ]) {
        let tableRow = document.createElement('tr')
        for (const i of [ ...Array(Object.keys(data).length).keys() ]) {
          let tableCell = document.createElement('td')
          let label = document.getElementById('<xsl:value-of select="$theader"/>-'+(i+1)).innerHTML
          tableCell.setAttribute('id', '<xsl:value-of select="$tablename"/>-row' + (j+1) + '-col' + (i+1))
          tableCell.innerHTML = (
            data[label][j] !== undefined ? data[label][j].value : ''
          )
          tableRow.appendChild(tableCell)
        }
        tableBody.appendChild(tableRow)
      }
      document.getElementById('<xsl:value-of select="$tablename"/>').appendChild(tableBody)

      var ctx = document.getElementById('<xsl:value-of select="$chartname"/>').getContext('2d')

      var bgColors = [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 206, 86, 0.2)'
      ]

      var borderColors = [
        'rgba(255, 99, 132, 0.5)',
        'rgba(54, 162, 235, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(153, 102, 255, 0.5)',
        'rgba(255, 159, 64, 0.5)',
        'rgba(255, 206, 86, 0.5)'
      ]
      
      function dataMap (x) { return Number(x.value) }
      function labelMap (k, i) { return 'Point ' + (i+1) }
      function datasetsMap ([label, data]) { return { ...data, label } }

      var datasets = {}
      for (const [label, _data] of Object.entries(data)) {
        let colorIndex = Object.keys(datasets).length % bgColors.length
        datasets[label] = {
          data: _data.map(dataMap),
          backgroundColor: bgColors[colorIndex],
          borderColor: borderColors[colorIndex]
        }
      }

      var labels = [ ...Array(<xsl:value-of select="$measPointCount"/>).keys() ].map(labelMap)

      var chart = new Chart(ctx, {
        type: 'line',
        data: {
          labels,
          datasets: Object.entries(datasets).map(datasetsMap)
        },
        options: {
          pointRadius: 4
        }
    })
   </script>
  </xsl:template>

  <xsl:template match="dcc:usedMethods">
    <div class="calibration-methods-header">
      <h5>Used methods</h5>
    </div>
    <xsl:for-each select="dcc:usedMethod">
      <xsl:apply-templates select=".">
        <xsl:with-param name="index" select="position()" />
      </xsl:apply-templates>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="dcc:usedMethod">
    <xsl:param name="index" />
    <!-- Unique ID for the accordion -->
    <xsl:variable name="id" select="generate-id(.)" />
    <!-- Element IDs -->
    <xsl:variable name="accordion-id" select="concat('accordion-', $id, $index)" />
    <xsl:variable name="heading-id" select="concat('heading-', $id, $index)" />
    <xsl:variable name="collapse-id" select="concat('collapse-', $id, $index)" />
    <div class="accordion" id="{$accordion-id}">
      <div class="accordion-item">
        <h2 class="accordion-header" id="{$heading-id}">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#{$collapse-id}" aria-expanded="false" aria-controls="{$collapse-id}">
            <xsl:call-template name="lang-content">
              <xsl:with-param name="node" select="dcc:name" />
            </xsl:call-template>
          </button>
        </h2>
        <div id="{$collapse-id}" class="accordion-collapse collapse" aria-labelledby="{$heading-id}" data-bs-parent="#{$accordion-id}">
          <table class="table table-borderless">
            <tr>
              <th style="width: 20%"></th>
              <th style="width: 80%"></th>
            </tr>
            <tr>
              <th>Description:</th>
              <td>
                <xsl:call-template name="lang-content">
                    <xsl:with-param name="node" select="dcc:description" />
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <th>Norm:</th>
              <td><xsl:value-of select="dcc:norm" /></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="dcc:usedSoftware">
    <div class="measurement-used-softare-head">
      <h5>Used software</h5>
    </div>
    <table class="table table-borderless" style="table-layout: fixed;">
      <tr>
        <th>Software</th>
        <th>release</th>
        <th>description</th>
      </tr>
      <xsl:for-each select="dcc:software">
        <tr>
          <td>
            <xsl:call-template name="lang-content">
              <xsl:with-param name="node" select="dcc:name" />
            </xsl:call-template>
          </td>
          <td>
            <xsl:value-of select="dcc:release" />
          </td>
          <td>
            <xsl:call-template name="lang-content">
              <xsl:with-param name="node" select="dcc:description" />
            </xsl:call-template>
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>

  <xsl:template match="dcc:measuringEquipments">
  <div class="measuring-equipments">
    <h5>Measuring equipment and instruments</h5>
  </div>  
    <xsl:for-each select="dcc:measuringEquipment">
      <xsl:apply-templates select=".">
        <xsl:with-param name="index" select="position()" />
      </xsl:apply-templates>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="dcc:measuringEquipment">
    <xsl:param name="index" />
    <!-- Unique ID for the accordion -->
    <xsl:variable name="id" select="generate-id(.)" />
    <!-- Element IDs -->
    <xsl:variable name="accordion-id" select="concat('accordion-', $id, $index)" />
    <xsl:variable name="heading-id" select="concat('heading-', $id, $index)" />
    <xsl:variable name="collapse-id" select="concat('collapse-', $id, $index)" />
    <div class="accordion" id="{$accordion-id}">
      <div class="accordion-item">
        <h2 class="accordion-header" id="{$heading-id}">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#{$collapse-id}" aria-expanded="false" aria-controls="{$collapse-id}">
            <xsl:value-of select="dcc:name/dcc:content" />
          </button>
        </h2>
        <div id="{$collapse-id}" class="accordion-collapse collapse" aria-labelledby="{$heading-id}" data-bs-parent="#{$accordion-id}">
          <table class="table table-borderless">
            <tr>
              <th style="width: 20%"></th>
              <th style="width: 80%"></th>
            </tr>
            <tr>
                <th>Equipment class</th>
                <td>
                  <xsl:value-of select="dcc:equipmentClass/dcc:reference" />
                </td>
            </tr>
            <tr>
                <th>Description</th>
                <td>
                  <xsl:call-template name="lang-content">
                    <xsl:with-param name="node" select="dcc:description" />
                  </xsl:call-template>
                </td>
            </tr>
            <tr>
                <th>Model</th>
                <td>
                  <xsl:call-template name="lang-content">
                    <xsl:with-param name="node" select="dcc:name" />
                  </xsl:call-template>
                </td>
            </tr>
            <tr>
                <th>Description data</th>
                <td>
                  <xsl:apply-templates select="dcc:descriptionData" /> 
                </td>
            </tr>
            <tr>
              <th>Certificate</th>
              <td>
                <xsl:value-of select="dcc:certificate/dcc:referral/dcc:content" />
              </td>
            </tr>
            <tr>
              <th>Manufacturer</th>
              <td>
                <xsl:for-each select="dcc:manufacturer">
                  <xsl:call-template name="contact">
                    <xsl:with-param name="node" select="." />
                    <xsl:with-param name="index" select="concat('nested-', position())" />
                  </xsl:call-template>
                
                </xsl:for-each>
                
              </td>
            </tr>
            <tr>
              <th>Identifications</th>
              <td><xsl:apply-templates select="dcc:identifications" /></td>
            </tr>
        </table>
        </div>
      </div>
    </div>
  </xsl:template>

  <!-- Probably not needed! -->
  <xsl:template match="dcc:descriptionData">
    <table class="table table-bordered">
      <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Filename</th>
        <th>mimeType</th>
        <th>dataBase64</th>
      </tr>
      <tr>
        <td>
          <xsl:value-of select="dcc:name/dcc:content" />
        </td>
        <td>
          <xsl:call-template name="lang-content">
            <xsl:with-param name="node" select="dcc:description" />
          </xsl:call-template>
          <!-- <xsl:value-of select="dcc:description/dcc:content" /> -->
        </td>
        <td>
          <xsl:value-of select="dcc:fileName" />
        </td>
        <td>
          <xsl:value-of select="dcc:mimeType" />
        </td>
        <td>
          <xsl:value-of select="dcc:dataBase64" />
        </td>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="dcc:influenceConditions">
    <div class="influence-header">
      <h5>Influence Conditions</h5>
    </div>
    <xsl:for-each select="dcc:influenceCondition">
      <xsl:apply-templates select=".">
        <xsl:with-param name="index" select="position()" />
      </xsl:apply-templates>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="dcc:influenceCondition">
    <xsl:param name="index" />
    <!-- Unique ID for the accordion -->
    <xsl:variable name="id" select="generate-id(.)" />
    <!-- Element IDs -->
    <xsl:variable name="accordion-id" select="concat('accordion-', $id, $index)" />
    <xsl:variable name="heading-id" select="concat('heading-', $id, $index)" />
    <xsl:variable name="collapse-id" select="concat('collapse-', $id, $index)" />
    <div class="accordion" id="{$accordion-id}">
      <div class="accordion-item">
        <h2 class="accordion-header" id="{$heading-id}">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#{$collapse-id}" aria-expanded="false" aria-controls="{$collapse-id}">
            <xsl:call-template name="lang-content">
              <xsl:with-param name="node" select="dcc:name" />
            </xsl:call-template>
          </button>
        </h2>
        <div id="{$collapse-id}" class="accordion-collapse collapse" aria-labelledby="{$heading-id}" data-bs-parent="#{$accordion-id}">
          <table class="table table-borderless">
            <tr>
              <th style="width: 20%"></th>
              <th style="width: 80%"></th>
            </tr>
            <tr>
              <th>Description:</th>
              <td>
                <xsl:call-template name="lang-content">
                  <xsl:with-param name="node" select="dcc:description" />
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <th>Status:</th>
              <td><xsl:value-of select="dcc:status" /></td>
            </tr>
            <tr>
              <th>Data:</th>
                <td>
                  <xsl:for-each select="dcc:data/dcc:quantity">
                  <table class="table table-bordered">
                    <tr>
                      <th style="width: 20%">Name</th>
                      <th style="width: 50%">Description</th>
                      <th style="width: 15%">Value</th>
                      <th style="width: 15%">Unit</th>
                    </tr>
                    <tr>
                      <td>
                        <xsl:call-template name="lang-content">
                          <xsl:with-param name="node" select="dcc:name" />
                        </xsl:call-template>
                      </td>
                      <td>
                        <xsl:call-template name="lang-content">
                          <xsl:with-param name="node" select="dcc:description" />
                        </xsl:call-template>
                      </td>
                      <td>
                        <xsl:value-of select="si:real/si:value" />
                      </td>
                      <td>
                        <xsl:value-of select="si:real/si:unit" />
                      </td>
                    </tr>
                  </table>
                  </xsl:for-each>
                </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="dcc:measurementMetaData">
    <xsl:if test="count(dcc:metaData) &gt; 0">
    <div class="measurement-metadata">
      <h5>Measurement metadata</h5>
      <xsl:for-each select="dcc:metaData">
        <xsl:apply-templates select=".">
          <xsl:with-param name="index" select="position()" />
        </xsl:apply-templates>
      </xsl:for-each>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="ds:Signature">
    <ul class="list-group list-group-flush">
      <div class="card-header"><h2>Digital signature</h2></div>
      <li class="list-group-item">
      <div class="validate-signature-header">
        <h3>Validate signature</h3>
      </div>
        <div id="validation" class="d-flex">
          <button id="validateButton" class="btn btn-primary" style="width: 100px; height: 50px; margin-top: 5px;">Validate</button>
          <i id="validationIcon" class="material-icons-two-tone" style="font-size:60px">gpp_maybe</i>
          <ul id="validationErrors"></ul>
        </div>
        <div class="certificate-chain-header">
            <h3>Certificate chain</h3>
        </div>
        <peculiar-certificates-viewer id="certificate-list" />
      </li>
    </ul>

    <script type="text/javascript">
      <!-- Parse certificate chain and add them to a list -->
      var certificates = []
      <xsl:for-each select="ds:KeyInfo/ds:X509Data/ds:X509Certificate">
        certificates.push({value: '<xsl:value-of select="."/>'})
      </xsl:for-each>

      document.getElementById('certificate-list').certificates = certificates

      async function validate() {

        document.getElementById("validationIcon").innerHTML = "gpp_maybe"
        document.getElementById("validationErrors").innerHTML = "Validating signature..."

        let validateRep
        try {
          validateRep = (await fetch("https://signing.contiot.io/v1/validate", {
            method: 'POST',
            cache: 'no-cache',
            headers: {
              'Content-Type': 'application/xml',
            },
            redirect: 'follow', 
            referrerPolicy: 'no-referrer',
            body: atob('<xsl:value-of select="$b64"/>')
          }))
        } catch (error) {
          document.getElementById("validationErrors").innerHTML = 
            "Failed to connect to the validation service"
        }
        const validateres = (await (validateRep.json()))
        console.log("validaterp", validateres)

        if (validateres.Indication === 'PASSED') {
            document.getElementById("validationErrors").innerHTML = "This signature is valid."
            document.getElementById("validationIcon").innerHTML = "verified_user"
        }
        else if (validateres.Indication) {
          if (validateres.Indication === 'FAILED') {
            document.getElementById("validationIcon").innerHTML = "gpp_bad"
          }
          else {
            document.getElementById("validationIcon").innerHTML = "gpp_maybe" 
          }
          document.getElementById("validationErrors").innerHTML = ""
          var validationErrors = document.getElementById("validationErrors")
          var list = document.createElement("ul")
          validationErrors.appendChild(document.createTextNode(validateres.SubIndication))
          for (const error of validateres.Errors) {
            var node = document.createElement('li')
            node.innerHTML = error.value
            list.appendChild(node)
          }
          validationErrors.appendChild(list)
          
        }
        else {
          var validationErrors = document.getElementById("validationErrors")
          var node = document.createElement("li")
          var text = document.createTextNode(validateres.error)
          node.appendChild(text)
          validationErrors.appendChild(node)
        }
      }

      document.getElementById("validateButton").addEventListener("click", validate)
      

    </script>

    
  </xsl:template>

  <xsl:template match="dcc:comment">
    <div class="card-header"><h2>Comments</h2></div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item"><xsl:value-of select="."/></li>
    </ul>
  </xsl:template>
  
</xsl:stylesheet>