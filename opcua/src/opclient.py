import base64
import requests
from lxml import etree
import sys
import json
import datetime
import os
from dotenv import load_dotenv
from opcua import Client, ua
import datetime
from validation import signatureIsValid
sys.path.insert(0, "..")
sys.path.append('../')
from templates import xmlTemplate

class opclient:
    """
    @param address: complete tcp.ip path for the opcua server
    """
    def __init__(self, address="opc.tcp://opcua-mockup:4840/freeopcua/server/"):
        load_dotenv()
        self.address = address
        #self.address = os.getenv("ADDRESS", "opc.tcp://opcua-mockup:4840/freeopcua/server/")
        # <xs:schema version="1.0" xmls:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="https://ptb.de/si" xmlns:si="https://ptb.de/si" elementFormDefault="qualified">
        # <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
        self.xmlTemplate = xmlTemplate
        
    """
    Accessing crane with given ip. Considerated private function.
    """
    def _connect_to_crane(self):
        url = self.address
        crane = Crane(url)
        return crane

    """
    Fetching wanted values from opcua.
    @return: dict
    """
    def fetchData(self, containerId="ID_ERR"):
        crane = self._connect_to_crane()
        data = {}
        try:
            bridge_pos, trolley_pos, hoist_pos = crane.get_coordinates_absolute()
            data['crane_name'] = "Ilmatar"
            data['load_gross'] = crane.get_load()
            data['load_tared'] = crane.get_load_tared()
            data['bridge_pos'] = bridge_pos
            data['trolley_pos'] = trolley_pos
            data['hoist_pos'] = hoist_pos
            data['datetime'] = crane.get_datetime()
            data['containerid'] = containerId
        except Exception as e:
            print(e)
            crane.disconnect()
            return None
        crane.disconnect()
        return data

    """
    Construct XML from dictionary.
    @return: etree
    """
    def constructXML(self, data):
        if data is None:
            return None
        xmlstring = (self.xmlTemplate.strip() % data).encode('utf-8')
        xml = etree.XML(xmlstring)
        return xml

    """
    Signing xml againts EIDAS dss. Payload structure in templates.py file.
    If no keystore/password is specified it will use the mockup version from templates.py
    @return: base64 decoded signed xml
    """
    def signData(self, xmlToSign, url="http://signing-api:3000/v1/seal"):
        #print(xmlToSign, flush=True)
        #payload = str(base64.b64encode(xmlToSign.encode("utf-8")), encoding='utf-8')
        #print(payload, flush=True)
        headers = {
            'Content-Type': 'application/xml; charset=utf-8',
            'disable-schema' : 'true',
            'signeralias': 'testcert'
        }
        try:
            response = requests.request("POST", url, headers=headers, data=xmlToSign)
            #signedXML = etree.XML(response.text.encode('utf8'))
            signedXML = response.text.encode('utf8')
            print("Sign successful", flush=True)
        except ConnectionError as e:
            print("Failed with signing:", e)
            signedXML = None
        # Decoding from base64 if the REST API changes.
        #response_data = json.loads(response.text.encode('utf8'))
        #decoded_xml_string = base64.b64decode(response_data['bytes'])
        #signedXML = etree.XML(decoded_xml_string)
        return signedXML

class Crane(object):
    """Definition of Ilmatar Crane interface through OPC UA."""

    def __init__(self, clientaddress):
        """Initialization of the Ilmatar Crane."""
        self.x = 1
        self.client = Client(clientaddress)
        self.client.connect()

        NS = "ns=" + str(self.client.get_namespace_index("SYM:"))

        # Accescode
        self._node_access_code = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Controls.AccessCode")
        
        # Loads
        self._node_hoist_current_load = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Status.Hoist.Load.Load_t")
        self._node_hoist_current_tared_load = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Status.Hoist.Load.TaredLoad_t")

        # Datetime
        self._node_datime_year = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Year")
        self._node_datime_month = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Month")
        self._node_datime_day = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Day")
        self._node_datime_hour = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Hour")
        self._node_datime_minute = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Minute")
        self._node_datime_second = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Second")
        self._node_datime_millisecond = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Millisecond")

        # Position
        self._node_hoist_position_m = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Status.Hoist.Position.Position_m")
        self._node_trolley_position_m = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Status.Trolley.Position.Position_m")
        self._node_bridge_position_m = self.client.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Status.Bridge.Position.Position_m")

    """Functions related to connecting to opcua server"""

    def connect(self):
        """Connect to the OPC UA Server."""
        self.client.connect()


    def disconnect(self):
        """Disconnect from OPC UA Server."""
        self.client.disconnect()


    """Functions related to accesscode"""

    def get_accesscode(self):
        """Get AccessCode value."""
        return self._node_access_code.get_value()


    def set_accesscode(self, accesscode):
        """Set AccessCode to number x."""
        self._node_access_code.set_value(ua.DataValue(ua.Variant(accesscode, ua.VariantType.Int32)))


    """Functions to get weight from the crane"""

    def get_load(self):
        """Get current load from crane."""
        return self._node_hoist_current_load.get_value()

    def get_load_tared(self):
        """Get current tared load from crane."""
        return self._node_hoist_current_tared_load.get_value()
    
    def get_datetime(self):
        """Get Python datetime object from the crane."""
        year = self._node_datime_year.get_value()
        month = self._node_datime_month.get_value()
        day = self._node_datime_day.get_value()
        hour = self._node_datime_hour.get_value()
        minute = self._node_datime_minute.get_value()
        second = self._node_datime_second.get_value()
        millisecond = self._node_datime_millisecond.get_value()
        return datetime.datetime(year,month,day,hour,minute,second,millisecond*1000).isoformat()

    """Get absolute position for Bridge,Trolley and Hoist in mm (laser)."""
    
    def get_coordinates_absolute(self):
        return(self._node_bridge_position_m.get_value(),
               self._node_trolley_position_m.get_value(),
               self._node_hoist_position_m.get_value())

if __name__ == '__main__':
    address = "opc.tcp://0.0.0.0:4840/freeopcua/server/"
    opc = opclient(address)
    
    xml = opc.constructXML()
    xml_original = etree.tostring(xml).decode()
    print(xml_original)
    tmp = opc.signData(xml_original)
    xml_signed = etree.tostring(tmp).decode()
    print(signatureIsValid(xml_signed))
    #validation_report = validate(xml_original, xml_signed)
    #print(json.dumps(validation_report, indent=2, sort_keys=True))
    
