import sys
sys.path.append('../')
import unittest
from opclient import opclient
from lxml import etree
from validation import signatureIsValid
import sys
import os
from pathlib import Path
import time
import json
import time

class TestClient(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        address = "opc.tcp://opcua-mockup:4840/freeopcua/server/"
        self.client = opclient(address)
        fn = str(Path(__file__).resolve().parent) + '/xml/measurementEvent.xsd'
        self.xmlschema = etree.XMLSchema(etree.parse(fn))

    @classmethod
    def tearDownClass(self):
        pass

    def test_connection(self):
        crane = self.client._connect_to_crane()
        self.assertEqual(crane is not None, True, "Connection to the server failed")
        crane.disconnect()

    def test_XML(self):
        data = self.client.fetchData()
        xml = self.client.constructXML(data)
        self.assertEqual(self.xmlschema.validate(xml), True, "XML format was not correct")
    
    def test_data(self):
        data_1 = self.client.fetchData()
        self.assertEqual(data_1 is not None, True, "Fetching data packet 1 failed.")
        time.sleep(4)
        data_2 = self.client.fetchData()
        self.assertEqual(data_2 is not None, True, "Fetching data packet 2 failed.")
        self.assertEqual(data_1['load_gross'] == data_2['load_gross'], False, "Load_gross values are the same. Have you hardcoded something?")
        self.assertEqual(data_1['load_tared'] == data_2['load_tared'], False, "Load_gross_tared values are the same. Have you hardcoded something?")
        self.assertEqual(data_1['datetime'].split(":")[2].split("+")[0] == data_2['datetime'].split(":")[2].split("+")[0], False, "Time hasn't elapsed between measurements. Have you hardcoded something?")

if __name__ == '__main__':
    unittest.main()
