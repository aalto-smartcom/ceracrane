import sys
sys.path.insert(0, "..")
import time
import random
import datetime
import requests
from flask import Flask, json, request
from flask_cors import CORS, cross_origin
from opcua import ua, Server
sys.path.insert(0, "..")
from pathlib import Path


class Mockup:
    randomState = True
    def __init__(self):
        # setup our server
        self.server = Server()
        self.server.set_endpoint("opc.tcp://0.0.0.0:4840/freeopcua/server/")
        uri = "SYM:"
        idx = self.server.register_namespace(uri)
        self.server.import_xml(str(Path(__file__).resolve().parent) + '/xml/custom_nodes.xml')
        self.date = datetime.datetime.now()

    def updateValues(self):
        values = ""
        try:
            values = requests.get('http://opcua-valueapi:9005/getValues').json()
        except:
            print("OPCUA Server failed to get data from serverapi", flush="True")
            values = {
                "hoist_load" : 0,
                "tared_load" : 0,
                "hoist_position" : 0,
                "trolley_position" : 0,
                "bridge_position" : 0
            }
        
        self.date = datetime.datetime.now()
        NS = "ns=" + str(self.server.get_namespace_index("SYM:"))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Year").set_value((self.date.year))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Month").set_value((self.date.month))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Day").set_value((self.date.day))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Hour").set_value((self.date.hour))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Minute").set_value((self.date.minute))
        self.server.get_node(
            NS + ";s=SCF.PLC.DX_Custom_V.Datetime.Second").set_value((self.date.second))
        
        # Load
        self.server.get_node(NS + ";s=SCF.PLC.DX_Custom_V.Status.Hoist.Load.Load_t").set_value(values["hoist_load"])
        self.server.get_node(NS + ";s=SCF.PLC.DX_Custom_V.Status.Hoist.Load.TaredLoad_t").set_value(values["tared_load"])
        
        # Position
        self.server.get_node(NS + ";s=SCF.PLC.DX_Custom_V.Status.Hoist.Position.Position_m").set_value(values["hoist_position"])
        self.server.get_node(NS + ";s=SCF.PLC.DX_Custom_V.Status.Trolley.Position.Position_m").set_value(values["trolley_position"])
        self.server.get_node(NS + ";s=SCF.PLC.DX_Custom_V.Status.Bridge.Position.Position_m").set_value(values["bridge_position"])


if __name__ == "__main__":
    try:
        mockup = Mockup()
        mockup.server.start()
        while True:
            time.sleep(1)
            mockup.updateValues()
    finally:
        mockup.server.stop()
