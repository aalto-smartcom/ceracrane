xmlTemplate = """
<?xml version="1.0" ?>

<ev:measurementEvent xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://NamespaceTest.com/measurementEvent"
  xmlns:si="https://ptb.de/si"
  xmlns:ev="http://NamespaceTest.com/measurementEvent"
  xmlns:m="http://NamespaceTest.com/measurementMetadata">

<ev:metadata>
  <m:craneName>%(crane_name)s</m:craneName>
  <m:dateTime>%(datetime)s</m:dateTime>
  <m:containerId>%(containerid)s</m:containerId>
</ev:metadata>

<ev:measurement>
  <si:real>
    <si:label>load_tared</si:label>   
    <si:value>%(load_tared)s</si:value>
    <si:unit>\kilogram</si:unit>
    <si:expandedUnc>
      <si:uncertainty>0.50</si:uncertainty>
      <si:coverageFactor>2</si:coverageFactor>
      <si:coverageProbability>0.95</si:coverageProbability>
      <si:distribution>normal</si:distribution>
    </si:expandedUnc>
  </si:real>

  <si:real>
    <si:label>load_gross</si:label>   
    <si:value>%(load_gross)s</si:value>
    <si:unit>\kilogram</si:unit>
    <si:expandedUnc>
      <si:uncertainty>0.50</si:uncertainty>
      <si:coverageFactor>2</si:coverageFactor>
      <si:coverageProbability>0.95</si:coverageProbability>
      <si:distribution>normal</si:distribution>
    </si:expandedUnc>
  </si:real>

  <si:real>
    <si:label>hoist_position</si:label>   
    <si:value>%(hoist_pos)s</si:value>
    <si:unit>\metre</si:unit>
    <si:expandedUnc>
      <si:uncertainty>0.10</si:uncertainty>
      <si:coverageFactor>2</si:coverageFactor>
      <si:coverageProbability>0.95</si:coverageProbability>
      <si:distribution>normal</si:distribution>
    </si:expandedUnc>
  </si:real>

  <si:real>
    <si:label>bridge_position</si:label>   
    <si:value>%(bridge_pos)s</si:value>
    <si:unit>\metre</si:unit>
    <si:expandedUnc>
      <si:uncertainty>0.20</si:uncertainty>
      <si:coverageFactor>2</si:coverageFactor>
      <si:coverageProbability>0.95</si:coverageProbability>
      <si:distribution>normal</si:distribution>
    </si:expandedUnc>
  </si:real>

  <si:real>
    <si:label>trolley_position</si:label>   
    <si:value>%(trolley_pos)s</si:value>
    <si:unit>\metre</si:unit>
    <si:expandedUnc>
      <si:uncertainty>0.20</si:uncertainty>
      <si:coverageFactor>2</si:coverageFactor>
      <si:coverageProbability>0.95</si:coverageProbability>
      <si:distribution>normal</si:distribution>
    </si:expandedUnc>
  </si:real>

</ev:measurement>
</ev:measurementEvent>  
    """
