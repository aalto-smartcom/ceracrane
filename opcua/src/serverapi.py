import time
import random
import datetime
from flask import Flask, json, request
from flask_cors import CORS, cross_origin

api = Flask(__name__)
cors = CORS(api)
api.config['CORS_HEADERS'] = 'Content-Type'
print("initial set", flush="True")
state = {}
values = {
    "hoist_load" : 0,
    "tared_load" : 0,
    "hoist_position" : 0,
    "trolley_position" : 0,
    "bridge_position" : 0
}

def generateRandomValues():
    ret = {
        "hoist_load" : float(5 + random.random() - 0.5),
        "tared_load" : float(5 + random.random() - 0.5),
        "hoist_position" : float(1.0 + random.random()),
        "trolley_position" : float(4.0 + random.random()),
        "bridge_position" : float(2.0 + random.random())
    }
    return ret

@api.route('/setValues', methods=['POST'])
@cross_origin()
def apiValues():
    payload = request.get_json(force=True)
    values["hoist_load"] = payload["weight"]
    values["tared_load"] = payload["tared"]
    values["hoist_position"] = payload["hoist_pos"]
    values["trolley_position"] = payload["trolley_pos"]
    values["bridge_position"] = payload["bridge_pos"]
    state["randomState"] = False
    return str("Success (like always).")

@api.route('/setRandomState', methods=['POST'])
@cross_origin()
def setRandom():
    payload = request.get_json(force=True)
    state["randomState"] = payload["random_state"]
    print(state["randomState"], flush="True")
    return str(state["randomState"])

@api.route('/getValues', methods=['GET'])
@cross_origin()
def getValues():
    if state["randomState"]:
        return json.dumps(generateRandomValues())
    else:
        return json.dumps(values)



if __name__ == '__main__':
    state["randomState"] = True
    api.run(host='0.0.0.0', port='9005', debug=True)