import requests
import base64
import json

"""
Verifies the integrity and signature of signedXML againts originalXML
@return: validation report in JSON
"""
def simpleValidate(signedXML, url="http://signing-api:3000/v1/validate"):

    headers = {
    'Content-Type': 'application/xml; charset=utf-8'
    }
    response = requests.request("POST", url, headers=headers, data = signedXML)
    response_data = json.loads(response.text.encode('utf8'))
    return response_data

def signatureIsValid(signedXML, url="http://signing-api:3000/v1/validate"):
    response = simpleValidate(signedXML, url)
    signatureReport = response["DiagnosticData"]["Signature"][0]
    #Structural validation
    structure = signatureReport["StructuralValidation"]["Valid"]
    #Signature validation
    signature = signatureReport["BasicSignature"]["SignatureIntact"] and signatureReport["BasicSignature"]["SignatureValid"]

    return structure and signature